# Docker Images

## Build Image
This image contains all the dependencies for building switcher in a docker container.

    cd docker
    docker build -t metalab/switcher:build .
    
