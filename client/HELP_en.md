| Key        		| Description                      |
| ----------------- | ---------------------------------| 
| (1-9)             | Switch between tabs              |
| i                 | Information on selected ressource|
| s                 | Selected ressource settingss     |
| c                 | SIP Connection/Contacts          |
| f                 | Make window fullscreen           | 
| a                 | Add a ressource	               | 
| ctrl+s            | Save file        	               |
| ctrl+shift+s      | Save as...      	               | 
| ctrl+o            | Open a file      	               |
| ctrl+alt+n        | Create a new file	    	       |
