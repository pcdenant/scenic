# Scenic Frontend
The application's front-end is built using primarily [Backbone](http://backbonejs.org/) and [Marionette](http://marionettejs.com/).
It uses [RequireJS](http://requirejs.org/) to manage module dependencies/loading and building with ```r.js```.

[Bower](http://bower.io/) is used to manage browser packages and paths to bower components need to be configured for
installed packages in ```src/main.js```.

## Directory Structure

### client/assets
Static files shared publicly via http.

### client/css
Stylesheet built from the scss files in *client/scss*.

### client/src
Javascript source files.

#### client/src/lib
Miscelanious libs used in the fron-end

#### client/src/model
Backbone collections and models

#### client/src/view
Marionette views

### templates
EJS HTML templates used by the views.

### test
Test files
