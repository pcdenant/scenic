"use strict";

import _ from 'underscore';
import Backbone from 'backbone';
import async from 'async';
import io from 'socket.io-client';
import SaveFiles from './SaveFiles';
import ClassDescriptions from './ClassDescriptions';
import Quiddities from './Quiddities';
import SIPConnection from './SIPConnection';
import SaveProperties from './SaveProperties';
import SaveContacts from './SaveContacts';
import Scenes from './scene/Scenes';
import Chats from './Chats';
/**
 * Session
 *
 * @constructor
 * @extends module:Backbone.Model
 */
const Session = Backbone.Model.extend( {

    defaults: function () {
        return {
            id:         null,
            name:       null,
            host:       null,
            lang:       localStorage.getItem( 'lang' ) ? localStorage.getItem( 'lang' ) : 'en',
            default:    false,
            connecting: false,
            connected:  false,
            active:     false,
            fullscreen: false,
            shutdown:   false
        }
    },

    /**
     * Initialize
     */
    initialize: function ( attributes, options ) {
        var self = this;

        // Session models
        this.config       = null;
        this.saveFiles    = null;
        this.saveContacts = null;
        this.classes      = null;
        this.quiddities   = null;
        this.sip          = null;
        this.chats        = null;

        // Establish missing values
        if ( !this.get( 'id' ) ) {
            this.set( 'id', this.get( 'host' ) ? this.get( 'host' ) : 'default' );
        }
        if ( !this.get( 'name' ) ) {
            this.set( 'name', this.get( 'id' ) );
        }

        // Session Config
        this.sessionConfig = this.readSessionConfig();

        // Global Scenic Channel
        this.scenicChannel = Backbone.Wreqr.radio.channel( 'scenic' );

        // Session Channel
        this.sessionChannel = Backbone.Wreqr.radio.channel( this.id );

        // Socket (before the collections, they need this socket)
        this.socket = io.connect( this.get( 'host' ), {
            reconnectionDelay:    250,
            reconnectionDelayMax: 2 * 60 * 1000,
            //timeout: 1000
        } );
        this.set( 'connecting', true );

        // Collections (they need the socket)
        this.saveFiles      = new SaveFiles( null, { scenic: this } );
        this.classes        = new ClassDescriptions( null, { scenic: this } );
        this.quiddities     = new Quiddities( null, { scenic: this } );

        // Session Socket.io
        this.socket.on( 'connect', function () {
            // Announce ourselves and recover config information from the server
            self.socket.emit( 'config', self.get( 'lang' ), self.sessionConfig.socketId ? self.sessionConfig.socketId : null, function ( config ) {
                self.sessionConfig.socketId = self.socket.id;
                self.flushScenicConfig();
                self.start( config );
            } );

        } );

        // When the server is closed or crashes shutdown the app
        this.socket.on( 'shutdown', _.bind( this._onShutdown, this ) );
        this.socket.on( 'disconnect', _.bind( this._onDisconnect, this ) );
    },

    start: function ( config ) {

        // Configuration
        this.config = config;

        var self = this;

        async.series( [

            // Get class descriptions
            function ( callback ) {
                self.saveFiles.fetch( { success: _.partial( callback, null ), error: callback } );
            },

            // Get class descriptions
            function ( callback ) {
                self.classes.fetch( { success: _.partial( callback, null ), error: callback } );
            },

            // Get Quiddities
            function ( callback ) {
                self.quiddities.fetch( { success: _.partial( callback, null ), error: callback } );
            },

            // SIP and saveproperties init
            function ( callback ) {
                self.scenes         = new Scenes( null,  { scenic: self });
                self.saveProperties = new SaveProperties( null, { scenic: self });
                self.saveContacts   = new SaveContacts( null, { scenic: self });
                self.sip            = new SIPConnection( null, { scenic: self } );
                if( !self.config.disableChat ) {
                    self.chats = new Chats(null, {scenic: self});
                }
                //self.sip.autoconnect( {success: _.partial( callback, null ), error: callback} );
                callback();
            }

        ], function ( error ) {
            if ( error ) {
                //TODO: Initialization errors
                alert( error.toString() );
                log.error( error );
                if ( callback ) {
                    callback( error );
                }
                return;
            }

            self.set( 'connecting', false );
            self.set( 'connected', true );
            self.set( 'shutdown', false );

            if ( self.get( 'default' ) ) {
                self.activate();
            }
        } );
    },

    readScenicConfig: function () {
        // Local config
        var scenicConfig = JSON.parse( localStorage.getItem( 'scenic' ) ) || {};
        if ( !scenicConfig.sessions ) {
            scenicConfig.sessions = {};
        }
        return scenicConfig;
    },

    readSessionConfig: function () {
        var scenicConfig = this.readScenicConfig();
        if ( !scenicConfig.sessions[this.id] ) {
            scenicConfig.sessions[this.id] = {};
        }
        return scenicConfig.sessions[this.id];
    },

    flushScenicConfig: function () {
        var scenicConfig               = this.readScenicConfig();
        scenicConfig.sessions[this.id] = this.sessionConfig;
        localStorage.setItem( 'scenic', JSON.stringify( scenicConfig ) );
    },

    reset: function () {
        this.socket.emit( 'reset' );
    },

    /**
     * Activate a session
     */
    activate: function () {
        this.collection.setCurrentSession( this );
    },

    refreshScenes: function () {
        // this.scenes.destroy();
        this.scenes = new Scenes( null,  { scenic: this });
    },

    _onShutdown: function () {
        this.set( 'shutdown', true );
        this._onDisconnect();
    },

    _onDisconnect: function () {
        if ( this.get( 'connected' ) ) {
            this.set( 'connected', false );
            this.set( 'connecting', true );
            this.sessionChannel.reset();
            this.saveFiles.reset();
            this.classes.reset();
            this.quiddities.reset();
            this.sip.destroy();
        }
    }
} );

export default Session;