"use strict";

import _ from 'underscore';
import ScenicCollection from './base/ScenicCollection';
import Quiddity from './Quiddity';

/**
 * Quiddities Collection
 *
 * @constructor
 * @extends ScenicCollection
 */
const Quiddities = ScenicCollection.extend( {
    model:      Quiddity,
    comparator: function ( quiddity ) {
        const classDescription = quiddity.get( 'classDescription' );
        if ( classDescription ) {
            return classDescription.get( 'category' ) + '.' + quiddity.get( 'class' ) + '.' + quiddity.id;
        } else {
            console.warn( 'no description' );
            return quiddity.get( 'class' ) + '.' + quiddity.id;
        }
    },

    methodMap: {
        'create': null,
        'update': null,
        'patch':  null,
        'delete': null,
        'read':   'quiddity.list'
    },

    /**
     * Initialize
     */
    initialize: function ( models, options ) {
        ScenicCollection.prototype.initialize.apply( this, arguments );

        // Handlers
        this.onSocket( 'quiddity.created', _.bind( this._onCreate, this ) );
    },

    create: function ( info ) {
        var quiddity = new Quiddity( null, { scenic: this.scenic } );
        quiddity.save( { 'class': info.type, 'initName': info.name, 'initProperties': info.properties}, {
            success:  ( quiddity ) => {
                if ( info.device ) {
                    //TODO: What is this I don't even
                    alert( 'What is this I don\'t even' );
                    quiddity.setProperty( 'device', info.device );
                }
            }
        } );
    },

    reorder: function ( id, view, from, to ) {
        if ( from == to ) {
            return;
        }

        // Reorder the moved quiddity
        var movedUserData = this.get( id ).get( "userData" );
        if ( !movedUserData.order ) {
            movedUserData.order = {};
        }
        movedUserData.order[view] = to;

        this.forEach( function ( quiddity ) {
            var userData = quiddity.get( "userData" );
            if ( !userData.order || "undefined" == typeof userData.order[view] ) {
                return;
            }
            var order = userData.order[view];

            /*if ( order == from ) {
             order = to;
             } else*/
            if ( from < to && order > from && order < to ) {
                order--;
            } else if ( from > to && order > to && order < from ) {
                order++;
            }
            userData.order[view] = order;
        } );
    },

    /**
     * Create Handler
     * Listens to quiddity creations and add/merge new quiddities to the collection
     *
     * @private
     * @param quiddityData
     * @param socketId
     */
    _onCreate: function ( quiddityData, socketId ) {
        var quiddity = this.add( quiddityData, { merge: true, parse: true } );
        this.scenic.sessionChannel.vent.trigger( 'quiddity:added', quiddity );
        // If we created it, start editing it
        if ( this.scenic.socket.id == socketId ) {
            quiddity.edit();
        }
        
    }
} );

export default Quiddities;