"use strict";

import Backbone     from "backbone";
import UserDataWatcherModel  from '../base/UserDataWatcherModel';

/**
 * SceneProperties
 *
 * @constructor
 * @extends module:Backbone.Model
 */

var SceneProperties = UserDataWatcherModel.extend( {

    defaults: function () {
        return {
            name:   null,
            active: false,
            select: false,
            toggle: false
        }
    },

    methodMap: {
        'create': function () {
            return ['quiddity.userData.set.branch', this.quiddityId, this.userDataPath, JSON.stringify(this) ]
        },
        'update': function () {
            return ['quiddity.userData.set.branch', this.quiddityId, this.userDataPath, JSON.stringify(this) ]
        },
        'delete': function () {
            return ['quiddity.userData.remove', this.quiddityId, this.userDataPath ]
        },
        'read': function () {
            return ['quiddity.userData.get', this.quiddity.id, this.get('path') + '.' + this.id]
        },
    },

    /**
     * Initialize
     */
    initialize: function ( attributes, options ) {
        this.quiddityId     = this.scenic.config.userTree.quiddName;
        this.userDataPath   = options.userDataPath + ".properties";
        this.scenic         = options.scenic;

        UserDataWatcherModel.prototype.initialize.apply( this, arguments );

        this.set({
            "name":     options.name,
            "active":   options.active,
            "select":   options.select,
            "toggle":   options.toggle

        });
    },

    rename: function ( name ) {
        this.save({name});
    },

    select: function () {
        var select = true;
        this.save({select});
    },

    deselect: function ( ) {
        var select = false;
        this.save({select});
    },
    
    toggle: function () {
        var toggle = true;
        this.save({toggle});
    },

    untoggle: function ( ) {
        var toggle = false;
        this.save({toggle});
    },
    
    activate: function ( isSelect ) {
        var active = true;
        this.save({active});
    },

    deactivate: function ( isSelect ) {
        var active = false;
        this.save({active});
    },

    userDataChanged: function( properties ) {
        if ( properties ) {
            if( this.get('name') != properties.name ) {
                this.set( 'name', properties.name );
            }
            if( this.get('select') != properties.select ) {
                this.set( 'select', properties.select );
            }
            if( this.get('active') != properties.active ) {
                this.set( 'active', properties.active );
            }
            if( this.get('toggle') != properties.toggle ) {
                this.set( 'toggle', properties.toggle );
            }
        }
    }

} );

export default SceneProperties;