"use strict";

import Backbone from "backbone";
import UserDataWatcherCollection from '../base/UserDataWatcherCollection';
import SceneConnection from './SceneConnection';

/**
 * Connections collection
 * Self-updates when quiddities and/or shmdatas are updated
 */
var SceneConnections = UserDataWatcherCollection.extend( {
    model:        SceneConnection,

    methodMap: {
        'create': function () {
            return ['quiddity.userData.set.branch', this.quiddityId, this.userDataPath, JSON.stringify(this) ]
        },
        'update': function () {
            return ['quiddity.userData.set.branch', this.quiddityId, this.userDataPath, JSON.stringify(this) ]
        },
        'patch':  null,
        'delete': function () {
            return ['quiddity.userData.remove', this.quiddityId, this.userDataPath ]
        },
        'read': function () {
            return ['quiddity.userData.get', this.quiddity.id, this.userDataPath]
        },
    },

    /**
     * Initialize
     */
    initialize: function ( models, options ) {
        this.quiddityId     = this.scenic.config.userTree.quiddName;
        this.userDataPath   = options.userDataPath + ".connections";
        this.scenic         = options.scenic;

        UserDataWatcherCollection.prototype.initialize.apply(this,arguments);
    },

    toJSON:  function(options) {
        var json = Backbone.Collection.prototype.toJSON.call(this, options);
        var result = {};

        _.each(json, value => {
            // quick fix
            var id = value.id;
            if (  value.id && value.id.indexOf('.') > 0 ) {
                id = id.substring(0, value.id.indexOf('.'));
            }
            result[id] = value;
        });
        return result;
    },

    _add: function ( connection ) {
        Backbone.Collection.prototype.add.call(this, connection);
    },

    _remove: function ( connection ) {
        Backbone.Collection.prototype.remove.call(this, connection);
    },

    _sync: function () {
        this.sync( 'update', this, { 'success': result => { return null }, 'error': error => { return null }});
    },

    _addConnection: function ( connection ) {
        var connection = new SceneConnection(null, {
            id: connection.id,
            source: connection.source,
            destination: connection.destination,
            shmdata: connection.shmdata,
            type: connection.type,
            scenic: this.scenic,
            userDataPath: this.userDataPath
        });
        this._add( connection );
    },

    getConnection: function ( source, destination ) {
        var connectionFound = null;
        this.each( connection  => {
            if ( connection.get('source') == source && connection.get('destination') == destination ) {
                connectionFound = connection;
            }
        });
        return connectionFound;
    },

    getConnectionsFromSource: function ( source ) {
        return this.where( connection => { return connection.get('source') == source.id });
    },

    getConnectionsFromDestination: function ( destination ) {
        return this.where( connection => { return connection.get('destination') == destination.id });
    },

    addConnection: function ( connectionToAdd ) {
        var connection = new SceneConnection(null, {
            id: connectionToAdd.id,
            source: connectionToAdd.get('source'),
            destination: connectionToAdd.get('destination'),
            shmdata: connectionToAdd.get('shmdata'),
            type: connectionToAdd.get('type'),
            scenic: this.scenic,
            userDataPath: this.userDataPath
        });
        this._add( connection );
        this._sync();
    },

    addSourceDestination: function ( source, destination, type ) {
        var sourceId = source.collection ? source.collection.quiddity.id : source.id;
        var connection = new SceneConnection(null, {
            id: sourceId + destination.id,
            source: sourceId,
            shmdata: source.id,
            type: type,
            destination: destination.id,
            scenic: this.scenic,
            userDataPath: this.userDataPath
        });
        this._add( connection );
        this._sync();
    },

    removeConnection: function ( source, destination ) {
        var connection = this.getConnection( source, destination );
        if ( connection ) {
            this._remove( connection );
            this._sync();
        }
    },

    userDataChanged: function( sceneConnections ) {
        if ( sceneConnections ) {
            _.each( sceneConnections, connection => {
                if ( !this.getConnection( connection.source, connection.destination ) ) {
                    var connection = new SceneConnection(null, {
                        id: connection.id,
                        source: connection.source,
                        destination: connection.destination,
                        shmdata: connection.shmdata,
                        type: connection.type,
                        scenic: this.scenic,
                        userDataPath: this.userDataPath
                    });
                    this._add(connection);
                }
            })
            // Synchronize the local connection of the scene with the userTree connection
            if ( this.length != sceneConnections.length ) {
                this.each( connection => {
                    if (connection) {
                        var connectionPresent = _.find( sceneConnections, sceneConnection => {
                            return sceneConnection.id == connection.id
                        });
                        if ( !connectionPresent ) {
                            this._remove(connection);
                        }
                    }
                });
            }
        }
    }
} );

export default SceneConnections;
