"use strict";

import Backbone     from "backbone";
import _            from 'underscore';
import UserDataWatcherModel  from '../base/UserDataWatcherModel';
import SceneConnections  from './SceneConnections';
import SceneProperties  from './SceneProperties';

/**
 * Scene
 *
 * @constructor
 * @extends module:Backbone.Model
 */

var Scene = UserDataWatcherModel.extend( {

    defaults: function () {
        return {
            id:                 null,
            properties:         null
        }
    },

    methodMap: {
        'create': function () {
            return ['quiddity.userData.set.branch', this.quiddityId, this.userDataPath, JSON.stringify(this) ]
        },
        'update': function () {
            return ['quiddity.userData.set.branch', this.quiddityId, this.userDataPath, JSON.stringify(this) ]
        },
        'delete': function () {
            return ['quiddity.userData.remove', this.quiddityId, this.userDataPath ]
        },
        'read': function () {
            return ['quiddity.userData.get', this.quiddity.id, this.get('path') + '.' + this.id]
        },
    },

    /**
     * Initialize
     */
    initialize: function ( attributes, options ) {
        this.quiddityId     = this.scenic.config.userTree.quiddName;
        this.userDataPath   = 'scenes.'+ options.id;
        this.scenic         = options.scenic;

        var sceneProperties = new SceneProperties( null, {
            name: options.properties.name,
            select: options.properties.select,
            active: options.properties.active,
            userDataPath: this.userDataPath,
            scenic: this.scenic  });


        var connections = options.connections ? options.connections : new Backbone.Collection();


        var sceneConnections = new SceneConnections(null, {
            quiddityId: this.quiddityId,
            userDataPath: this.userDataPath,
            scenic: this.scenic,
            collection: connections
        });

        if ( options.connections ) {
            options.connections.each( connection => {
                sceneConnections.addConnection ( connection )
            })
        }

        this.set({
            "id": options.id,
            "properties": sceneProperties,
            "connections": sceneConnections
        });

        this.listenTo( this.get('properties'), 'change:select', this.triggerSelect );
        this.listenTo( this.get('properties'), 'change:toggle', this.triggerToggle );
        this.listenTo( this.get('properties'), 'change:active', this.triggerActivate );
        this.listenTo( this.get('connections'), 'add remove', this.triggerConnections );

        UserDataWatcherModel.prototype.initialize.apply( this, arguments );
    },

    triggerSelect: function () {
        this.trigger('change:select')
    },

    triggerToggle: function () {
        this.trigger('change:toggle')
    },

    triggerActivate: function () {
        this.trigger('change:active')
    },

    triggerConnections: function () {
        this.trigger('change:connections')
    },

    isEmpty: function () {
        return this.get('connections') && this.get('connections').length == 0 ? true : false;
    },

    rename: function ( name ) {
        this.get('properties').rename( name );
    },

    select: function () {
        this.get('properties').select();
    },

    deselect: function ( ) {
        this.get('properties').deselect();
    },

    activate: function ( select ) {
        this.get('properties').activate( select );
    },

    deactivate: function ( select ) {
        this.get('properties').deactivate( select );
    },

    toggle: function ( ) {
        this.get('properties').toggle();
    },

    untoggle: function () {
        this.get('properties').untoggle();
    },

    setProperties: function ( properties ) {
        this.get('properties').set({
            name: properties.name,
            select: properties.select,
            active: properties.active
        });
    },

    getConnection: function ( source, destination ) {
        return this.get('connections') ? this.get('connections').getConnection( source, destination ) : null;
    },

    getConnectionsFromSource: function ( source ) {
        return this.get('connections').getConnectionsFromSource( source );
    },

    getConnectionsFromDestination: function ( destination ) {
        return this.get('connections').getConnectionsFromDestination( destination );
    },

    setConnections: function ( connections ) {
        var sceneConnection = this.get('connections');
        _.each( connections, connection => {
            if ( !sceneConnection.get( connection.id ) ) {
                sceneConnection._addConnection( connection );
            }
        });
        this.set('connections', sceneConnection);
    },

    addConnection: function ( connection ) {
        this.get('connections').addConnection( connection );
    },

    addSourceDestination: function ( source, destination, type ) {
        this.get('connections').addSourceDestination( source, destination, type );
    },

    removeSourceDestination: function ( source, destination ) {
        this.get('connections').removeConnection( source, destination );
    },

    removeConnection: function ( source, destination ) {
        if ( this.get('connections') && this.get('connections').getConnection( source, destination )) {
            this.get('connections').removeItem( source, destination );
        }
    },

    userDataChanged: function( scene ) {
        if ( scene ) {
            if (scene.connections) {
                this.setConnections(scene.connections);
            }
            if (scene.properties) {
                this.setProperties(scene.properties);
            }
        }
    }

} );

export default Scene;
