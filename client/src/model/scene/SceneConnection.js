"use strict";

import Backbone     from "backbone";
import UserDataWatcherModel  from '../base/UserDataWatcherModel';

/**
 * Group
 *
 * @constructor
 * @extends module:Backbone.Model
 */

var SceneConnection = UserDataWatcherModel.extend( {

    defaults: function () {
        return {
            'source': null,
            'destination': null
        }
    },

    methodMap: {
        'create': function () {
            return ['quiddity.userData.set.branch', this.quiddityId, this.userDataPath, JSON.stringify(this) ]
        },
        'update': function () {
            return ['quiddity.userData.set.branch', this.quiddityId, this.userDataPath, JSON.stringify(this) ]
        },
        'delete': function () {
            return ['quiddity.userData.remove', this.quiddityId, this.userDataPath ]
        },
        'read': function () {
            return ['quiddity.userData.get', this.quiddity.id, this.get('path') + '.' + this.id]
        },
    },

    /**
     * Initialize
     */
    initialize: function ( attributes, options ) {
        this.quiddityId     = this.scenic.config.userTree.quiddName;
        this.userDataPath   = options.userDataPath + '.'+  options.id;

        UserDataWatcherModel.prototype.initialize.apply( this, arguments );

        this.set({
            'id': options.id,
            'source': options.source,
            'destination': options.destination,
            'shmdata': options.shmdata,
            'type': options.type
        });
    },

    userDataChanged: function( connection ) {
    }

} );

export default SceneConnection;
