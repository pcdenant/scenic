"use strict";

import Backbone from "backbone";
import UserDataWatcherCollection from '../base/UserDataWatcherCollection';
import Scene from './Scene';
import i18n  from 'i18n';
import SceneConnections from './SceneConnections';

/**
 * scenes collection
 * Self-updates when quiddities and/or shmdatas are updated
 */

var Scenes = UserDataWatcherCollection.extend( {
    model:        Scene,

    methodMap: {
        'create': function () {
            return ['quiddity.userData.set.branch', this.quiddityId, this.userDataPath, JSON.stringify(this) ]
        },
        'update': function () {
            return ['quiddity.userData.set.branch', this.quiddityId, this.userDataPath, JSON.stringify(this) ]
        },
        'patch':  null,
        'delete': function () {
            return ['quiddity.userData.remove', this.quiddityId, this.userDataPath ]
        },
        'read': function () {
            return ['quiddity.userData.get', this.quiddity.id, this.userDataPath]
        },
    },

    // comparator: function( scene ){
    //     return scene.get('properties').get( 'order' );
    // },

    toJSON:  function( options ) {
        var json = Backbone.Collection.prototype.toJSON.call( this, options );
        var result = {};

        _.each(json, value => {
            result[value.id] = value;
        });

        return result;
    },

    /**
     * Initialize
     */
    initialize: function ( models, options ) {
        this.scenic         = options.scenic;
        this.quiddityId     = this.scenic.config.userTree.quiddName;
        this.userDataPath   = 'scenes';

        // Add default group
        var defaultScene = new Scene( null, {
            id: 'defaultScene',
            properties: {
                name: i18n.t('Default scene'),
                select: false,
                active: false },
            scenic: this.scenic  });

        this._add( defaultScene );
        this.previousActivateScene = null;
        UserDataWatcherCollection.prototype.initialize.apply( this, arguments );
    },


    add: function ( ) {
        var defaultNumber = 1;
        // get the last order and set the default number for the name of the new scene
        this.each( scene  => {
            var sceneNumber = scene.id.substring( 5, scene.id.length );

            if ( scene.id.indexOf('scene') == 0 && !isNaN( sceneNumber ) ) {
                defaultNumber = parseInt( sceneNumber ) + 1;
            }
        });
        var connections = this.getSelectedScene() ?this.getSelectedScene().get('connections') : null;

        this.deselectAllScene();

        var scene = new Scene ( null, {
            id: 'scene'+ defaultNumber,
            properties: {
                name: 'scene '+ defaultNumber,
                select: true,
                active: false },
            connections: connections,
            scenic: this.scenic });



        this._add( scene );
        this._sync();
    },

    _add: function ( scene ) {
        Backbone.Collection.prototype.add.call( this, scene );
    },

    _remove: function ( scene ) {
        Backbone.Collection.prototype.remove.call( this, scene );
    },

    _sync: function () {
        this.sync( 'update', this, { 'success': result => { return null }, 'error': error => { return null }});
    },

    remove: function ( idScene ) {
        if ( this.get( idScene ).get('properties').get('select') == true ) {
            var index = this.indexOf( idScene );
            var indexSelect = index < this.length ? index + 1 : index - 1;
            var previousScene = this.at( indexSelect );
            if (previousScene ) {
                this.selectScene(previousScene.id);
            }
        }

        this._remove( idScene );
        this._sync();
    },


    getActiveScene: function() {
       return  this.find( scene => { return scene.get('properties').get('active') == true } );
    },

    getSelectedScene: function() {
        return  this.find( scene => { return scene.get('properties').get('select') == true } );
    },

    getToggleScene: function() {
        return  this.find( scene => { return scene.get('properties').get('toggle') == true } );
    },

    selectScene: function ( idScene ) {
        if ( this.get( idScene ) ) {
            this.each( scene => {
                if ( scene.id == idScene ) {
                    scene.select();
                } else if ( scene.get('properties').get('select') == true ){
                    scene.deselect();
                }
            });

        }
    },

    /**
     * Get the scene which is sctivated
     *
     * @returns {*}
     */
    isActiveScene: function () {
        if( this.getActiveScene() ) {
            var connectionActiveScene = this.getActiveScene();
            var connectionSelectedScene = this.getSelectedScene();
            return connectionActiveScene && ( connectionActiveScene === connectionSelectedScene );
        }
        return false;
    },

    /**
     * Select a source and a destination for the selected scene
     * @param {Object} source
     * @param {Object} destination
     * @param {String} type of destination
     * @returns {*}
     */
    selectConnection: function ( source, destination, type ) {
        if ( !this.getSelectedScene().getConnection( source.id, destination.id ) ) {
            this.getSelectedScene().addSourceDestination( source, destination, type );
        }
    },

    /**
     * Deselect a source and a destination for the selected scene
     * @param {Object} source
     * @param {Object} destination
     * @returns {*}
     */
    deselectConnection: function ( source, destination ) {
        if ( this.getSelectedScene().getConnection( source, destination ) ) {
            this.getSelectedScene().removeSourceDestination( source, destination );
        }
    },

    activateScene: function ( idScene ) {
        if ( this.get( idScene ) ) {
            this.each( scene => {
                if ( scene.get('properties').get('active') == true ){
                    this.previousActivateScene = scene;
                    scene.deactivate( false );
                }
            });
            this.each( scene => {
                if ( scene.id == idScene ) {
                    scene.activate( false );
                    this.switchConnectionsScene( scene );
                }
            });
        }
    },

    deactivateScene: function ( idScene ) {
        if ( this.get( idScene ) ) {
             if ( this.get( idScene ).get('properties').get('active') == true ){
                 this.previousActivateScene = this.get( idScene );
                 this.get( idScene ).deactivate( false );
                 this.deactivateConnectionsScene( this.get( idScene ) );
             }
        }
    },

    toggleScene: function ( idScene ) {
        if ( this.get( idScene ) ) {
            this.each( scene => {
                if ( scene.get('properties').get('toggle') == true ){
                    scene.untoggle();
                }
            });
            this.get( idScene ).toggle();
        }
    },

    untoggleScene: function ( idScene ) {
        if ( this.get( idScene ) ) {
            if ( this.get( idScene ).get('properties').get('toggle') == true ){
                this.get( idScene ).untoggle();
            }
        }
    },

    getPreviousScene: function () {
        return this.previousActivateScene;
    },

    switchScene: function ( idScene ) {
        if ( this.get( idScene ) ) {
            this.each( scene => {
                if ( scene.get('properties').get('active') == true ){
                    this.previousActivateScene = scene;
                    scene.deactivate( true );
                }
            });
            this.each( scene => {
                if ( scene.id == idScene ) {
                    scene.activate( true );
                    this.switchConnectionsScene( scene );
                }
            });
        }
    },

    /**
     * Deactivate the previous scene and activate the next one
     *
     * @param {Object} scene
     * @returns {*}
     */

    switchConnectionsScene: function( scene ){
        var connections = scene.get('connections');

        // disconnect only the quiddities that are not present on the new scne
        if ( this.getPreviousScene() ) {
            this.getPreviousScene().get('connections').each( connection => {
                var isPresent = false;
                connections.each( connect => {
                    if ( connect.id == connection.id ) {
                        isPresent = true;
                    }
                });
                if ( !isPresent ) {
                    var source = this.scenic.quiddities.find(quiddity => {
                        return quiddity.id == connection.get('source')
                    });
                    if (source) {
                        var sourceShmdata = source.shmdatas.find(shmdata => {
                            return shmdata.id == connection.get('shmdata')
                        });
                        if ( sourceShmdata && connection.get('type') == 'sink') {
                            this._disconnectSinkByDestinationId(sourceShmdata, connection.get('destination'));
                        }
                    }
                }
            })

        }
        this.activeConnectionsScene( scene );
    },


    /**
     * Connect the shmdatas to their destinations from a scene
     *
     * @param {Object} scene
     * @returns {*}
     */
    activeConnectionsScene: function( scene ){
        var connections = scene.get('connections');

        // for each connection present is the scene connect them
        connections.each( ( connection, index ) => {
            var source = this.scenic.quiddities.find( quiddity => {return quiddity.id == connection.get('source')});
            if ( source ) {
                var sourceShmdata = source.shmdatas.find(shmdata => {
                    return shmdata.id == connection.get('shmdata')
                });

                if (sourceShmdata && connection.get('type') == 'sink') {
                    this._connectSinkByDestinationId( sourceShmdata, connection.get('destination'));
                }
            }
        });
    },

    /**
     * Disconnect the shmdatas to their destinations from a scene
     *
     * @param {Object} scene
     * @returns {*}
     */
    deactivateConnectionsScene: function( scene ){
        var connections = scene.get('connections');

        // for each connection present is the scene connect them
        connections.each( connection => {
            var source = this.scenic.quiddities.find( quiddity => {return quiddity.id == connection.get('source')});
            if ( source ) {
                var sourceShmdata = source.shmdatas.find(shmdata => {
                    return shmdata.id == connection.get('shmdata')
                });
                if (sourceShmdata && connection.get('type') == 'sink') {
                    this._disconnectSinkByDestinationId( sourceShmdata, connection.get('destination'));
                }
            }
        })
    },

    /**
     * Connect a source and a destination
     * @param {Shmdata} source
     * @param {String} id of the destination
     * @returns {*}
     */
    _connectSinkByDestinationId: function ( sourceShmdata, destination ) {
        var destQuiddity = this.scenic.quiddities.find(quiddity => {
            return quiddity.id == destination;
        });

        if ( destQuiddity && !destQuiddity.isConnected( sourceShmdata )) {
            destQuiddity.connect( sourceShmdata );
        }
    },

    /**
     * Disconnect a source and a destination
     * @param {Shmdata} source
     * @param {String} id of the destination
     * @returns {*}
     */
    _disconnectSinkByDestinationId: function ( sourceShmdata, destination ) {
        var destQuiddity = this.scenic.quiddities.find(quiddity => {
            return quiddity.id == destination;
        });
        if ( destQuiddity && sourceShmdata && destQuiddity.isConnected( sourceShmdata )) {
            destQuiddity.disconnect( sourceShmdata );
        }
    },

    /**
     * Select a source and a sink destination for the selected scene
     * @param {Object} source
     * @param {Object} destination
     * @param {String} type of destination
     * @returns {*}
     */
    selectSinkConnection: function ( source, destination ) {
        if ( !this.scenic.scenes.getSelectedScene().getConnection( source.collection.quiddity.id, destination.id ) ) {

            var maxReaders              = destination.get( 'maxReaders' );
            var existingConnectionCount = destination.shmdatas.where( { type: 'reader' } ).length;

            if ( maxReaders == 1 ) {
                // deselect if the group is connected and if there is already the same destination selected
                var sourceConnected = this.scenic.scenes.getSelectedScene().getConnectionsFromDestination( destination );

                if ( sourceConnected.length > 0 ) {
                    this.deselectConnection( sourceConnected[0].get('source'), destination.id );
                }
            } else if ( maxReaders < existingConnectionCount ) {
                self.scenic.sessionChannel.vent.trigger('error', i18n.t('You have reached the maximum number of selection. The limit is __limit__', {limit: maxReaders}));
                return;
            }

            this.scenic.scenes.getSelectedScene().addSourceDestination( source, destination, 'sink' );
        }
    },

    deselectAllScene: function ( ) {
        this.each( scene => {
            if ( scene.get('properties').get('select') == true ){
                scene.deselect();
            }
        });
    },

    removeConnection: function ( source, destination ) {
        this.each( scene  => {
            scene.removeConnection( source, destination );
        });
    },

    getConnection: function ( source, destination ) {
        var result;
        this.each( scene => {
            if ( scene.getConnection( source, destination ) != undefined ) {
                result = scene.getConnection( source, destination );
            }
        });
        return result;
    },

    userDataChanged: function( scenesData ) {
        if ( scenesData ) {
            _.each( scenesData, scene => {
                // if default scene was saved in the userTree retrieve data
                if ( scene.id == 'defaultScene' ) {
                    this.get('defaultScene').setConnections( scene.connections );
                    this.get('defaultScene').setProperties( scene.properties );
                } else if ( !this.get( scene.id ) ) {
                    var sceneAdded = new Scene( null, {
                        id: scene.id,
                        properties: scene.properties,
                        scenic: this.scenic
                    });
                    this._add( sceneAdded );
                }
            });
            // Synchronize the local scenes with the userTree scenes
            if ( this.length != scenesData.length ) {
                this.each( scene => {
                    if ( scene ) {
                        var scenePresent = _.find(scenesData, sceneData => {
                            return sceneData.id == scene.id
                        });
                        if ( !scenePresent ) {
                            this._remove( scene );
                        }
                    }
                });
            }
            this.trigger( 'change:scenes' );
        }
    }
} );

export default Scenes;
