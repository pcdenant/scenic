"use strict";

define( [
    'underscore',
    'backbone',
    'i18n',
    'model/base/ScenicModel'
], function ( _, Backbone, i18n, ScenicModel ) {

    /**
     * Property
     *
     * @constructor
     * @extends ScenicModel
     */

    var Property = ScenicModel.extend( {

        defaults: {
            'type':        null,
            'writable':    false,
            'label':       null,
            'description': null,
            'value':       null,
            'parent':      null,
            'order':       0,
            'enabled':     true
        },

        methodMap: {
            'create': null,
            'update': null,
            'patch':  null,
            'delete': null,
            'read':   function () {
                return ['quiddity.property.get', this.collection.quiddity.id, this.id]
            }
        },

        mutators: {
            transient:        true,
            disabled: function () {
                var enabled = this.get( 'enabled' );
                if ( !enabled ) {
                    return true;
                }

                var parentId = this.get('parent');
                if (parentId) {
                    var parent = this.collection.get( parentId );
                    return ( parent && !parent.get('enabled') );
                }

                return false;
            },
            whyDisabled: function () {
                var whyDisabled = this.get( 'why_disabled' );

                if ( !whyDisabled ) {
                    var parentId = this.get('parent');
                    if (parentId) {
                        whyDisabled = this.collection.get( parentId ).get('why_disabled') ;
                    }
                }

                return whyDisabled;
            }
        },

        /**
         * Initialize
         */
        initialize: function () {
            ScenicModel.prototype.initialize.apply( this, arguments );

            // Save initial value as backup
            this.lastSyncedValue = this.get( 'value' );
            // When syncing, save value as a backup
            this.on( 'sync', function ( model, response, options ) {
                this.lastSyncedValue = this.get( 'value' );
            } );

            // Only bind to socket if we aren't new
            // We don't want temporary models staying referenced by socket.io
            if ( !this.isNew() ) {
                this.onSocket( "propertyChanged", _.bind( this._onPropertyChanged, this ) );
                this.onSocket( 'quiddity.property.removed', _.bind( this._onPropertyRemoved, this ) );
                this.onSocket( 'quiddity.property.attribute.grafted', _.bind( this._onPropertyAttributeGrafted, this ) );
                this.onSocket( 'quiddity.property.attribute.pruned', _.bind( this._onPropertyAttributePruned, this ) );
            }
        },

        /**
         * Property Removed Handler
         *
         * @param {string} quiddityId The name of the quiddity
         * @param {string} name The name of the property or method
         */
        _onPropertyRemoved: function ( quiddityId, name ) {
            if ( !this.isNew() && this.collection.quiddity.id == quiddityId && this.id == name ) {
                this.trigger( 'destroy', this, this.collection );
            }
        },

        _onPropertyAttributeGrafted: function ( quiddityId, property, attribute, value ) {
            if ( !this.isNew() && this.collection.quiddity.id == quiddityId && this.id == property ) {
                this.set( attribute, value );
                if ( attribute == 'enabled' && value == false ) {
                    this.fetch();
                }
                
            }
        },

        _onPropertyAttributePruned: function ( quiddityId, property, attribute, value ) {
            if ( !this.isNew() && this.collection.quiddity.id == quiddityId && this.id == property ) {
                this.unset( attribute, value );
            }
        },

        /**
         * Signals Property Value Handler
         * Listens to property values changes concerning our parent quiddity and update this property if it matches
         *
         * @param {string} quiddityId The name of the quiddity
         * @param {string} property The name of the property or method
         * @param {string} value The value of the property
         */
        _onPropertyChanged: function ( quiddityId, property, value ) {
            if ( !this.isNew() && this.collection.quiddity.id == quiddityId && this.id == property ) {
                this.set( 'value', value );
                // Keep last synced value as a backup in case setting a value fails
                this.lastSyncedValue = value;
            }
        },

        /**
         * Update the value with the server
         *
         * @param value
         */
        updateValue: function ( value ) {
            var self = this;

            this.set( 'value', value, { internal: true } );
            this.scenic.socket.emit( "quiddity.property.set", this.collection.quiddity.id, this.id, value, function ( error ) {
                if ( error ) {
                    // There was an error setting the value, go back to the last synced value
                    if ( 'undefined' != typeof self.lastSyncedValue ) {
                        self.set( 'value', self.lastSyncedValue );
                    }
                    return self.scenic.sessionChannel.vent.trigger( 'error', error );
                }
            } );
        },

        unlock: function ( ) {
            var self = this;
            this.scenic.socket.emit( "quiddity.property.set", this.collection.quiddity.id, 'started', false, function ( error ) {
                if ( error ) {
                    return self.scenic.sessionChannel.vent.trigger( 'error', error );
                }
            } );
        }
    } );

    return Property;
} );