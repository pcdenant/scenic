"use strict";

import _ from 'underscore';
import Backbone from 'backbone';
import Chat from './Chat';
import ScenicCollection from "./base/ScenicCollection";
/**
 *  @constructor
 *  @augments module:Backbone.Collection
 */
const Chats = ScenicCollection.extend( {
    model: Chat,

    initialize: function ( models, options ) {
        this.scenic = options.scenic;

        if ( !this.scenic.config.disableChat ) {
            this.scenic.sessionChannel.commands.setHandler('open:chat', _.bind(this.addChat, this));
            this.scenic.sessionChannel.commands.setHandler('open:group:chat', _.bind(this.addGroupChat, this));
        }
        this.onSocket( 'irc.message', _.bind( this._onMessage, this ) );
        this.onSocket( 'irc.channel.joined', _.bind( this._onJoin, this ) );
        this.onSocket( 'irc.connected', _.bind( this._onConnected, this ) );
        this.onSocket( 'irc.channel.names', _.bind( this._onNames, this ) );
        this.listenTo( this.scenic.sip, 'change:connected', this.updateConnection );

        this.updateConnection();

        this.defaultGroupName = '#scenic_group_';

        ScenicCollection.prototype.initialize.apply( this, arguments );
    },

    login: function ( uri ) {
        this.scenic.socket.emit('irc.create', this.createNickname( uri ));
    },

    createNickname: function ( uri ) {
        return uri.replace(/[^a-zA-Z0-9]/g, '_').substring(0, 15);
    },

    cleanUri: function ( uri ) {
        return uri.substring(0, uri.indexOf('@'));
    },

    cleanNickname: function ( nick ) {
        return nick.substring(0, nick.indexOf('_'));
    },

    logout: function (  ) {
        this.scenic.socket.emit( 'irc.delete');
        this.reset();
    },

    updateConnection: function (  ) {
        if( this.scenic.sip.get('connected') == true ){
            this.login( this.scenic.sip.get('uri') )
        } else {
            this.logout();
        }
    },

    addChat: function ( contact, target, message ) {
        if(  !this.get( contact.id ) ) {
            var nick = contact.id ? this.cleanUri( contact.id ) : this.cleanNickname( contact );
            var nicknames = [];
            nicknames.push (nick );

            var chat = new Chat(null, {
                id: contact.id ? this.createNickname( contact.id ) : contact,
                nicknames : nicknames,
                scenic: this.scenic
            });
            this.add( chat );
            if ( target ) {
                chat.receiveMessage( this.cleanNickname( contact ), message );
            }
        }
    },

    addGroupChat: function ( contacts ) {
        if ( contacts ) {
            var nicknames = [];
            var channelName = this.defaultGroupName + guid();

            _.each(contacts, contact => {
                nicknames.push(this.createNickname(contact.get('uri')));
            })

            this.scenic.socket.emit('irc.create.channel', nicknames, channelName);
        }
    },

    getUri: function ( username ) {
        var contactUri = null;
        this.scenic.sip.contacts.each( contact => {
            var cutUsername = username.substring(0, username.indexOf('_'));
            if ( contact.get('uri') ) {
                var cutContact = this.cleanUri( contact.get('uri') );

                if (cutContact == cutUsername) {
                    contactUri = contact.get('uri');
                }
            }
        } );

        return contactUri;
    },


    getNickname: function ( uri ) {
        return uri.substring(0, uri.indexOf('@'));
    },

    _onConnected: function () {
        this.scenic.sessionChannel.vent.trigger('chat:connected', this.id);
    },

    _onNames: function ( channel, nicks, message ) {
        var chat = this.findWhere( chat => {
            return chat.id == channel;
        });
        if ( chat ) {
            var nicknames = [];

            for (var k in nicks) nicknames.push( this.cleanNickname( k ));
            chat.setNicknames( nicknames );
        }
    },


    _onJoin: function ( channel, nick, message ) {
        var chat = this.findWhere( chat => {
            return chat.id == channel;
        });

        if( chat == undefined ) {
            var chatGroup = new Chat(null, {
                id: channel,
                nicknames: [ this.getNickname(this.getUri(nick)) ],
                scenic: this.scenic
            });

            this.add( chatGroup );
        } else {
            var nicknames = chat.get('nicknames');
            var uri = this.cleanUri( this.getUri( nick ) );
            if ( this.getUri(nick) && this.getUri(nick) != this.scenic.sip.get('uri') && !_.contains(nicknames, uri ) ) {
                nicknames.push( uri );
                chat.setNicknames( nicknames );
            }
        }
    },

    _onMessage: function ( username, target, message ) {
        if ( message.args[1].indexOf('JOIN_CHANNEL #scenic_group_') != -1 ) {
            var lengthChannel = message.args[1].length;
            var channelName = message.args[1].substring( 12, lengthChannel );
            this.scenic.socket.emit('irc.join.channel', channelName);
        } else {
            //if the target is a group, directly check the id of the chat model
            var chat = this.findWhere( chat => {
                return chat.id == target;
            });

            if( chat ){
                chat.receiveMessage( this.cleanNickname( username ), message.args[1]);
            } else {
                if ( target.indexOf('#scenic_group_') != -1 ) {
                    var chatGroup = new Chat(null, {
                        id: target,
                        nicknames: [],
                        scenic: this.scenic
                    });

                    this.add( chatGroup );
                    this.scenic.socket.emit('irc.channel.names', target);
                    chatGroup.receiveMessage( this.cleanNickname( username ), message.args[1]);

                } else {
                    var contactUri = this.getUri(username);

                    if (contactUri) {

                        var nickname = this.createNickname(contactUri);

                        if (this.length == 0) {
                            this.addChat(nickname, target, message.args[1]);
                        } else {
                            this.each((chat, index) => {
                                if (chat.id == nickname) {
                                    chat.receiveMessage(this.cleanNickname(nickname), message.args[1]);
                                } else if (index == this.length - 1) {
                                    this.addChat(nickname, target, message.args[1]);
                                }
                            });
                        }
                    }
                }
            }
        }
    }
} );

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

export default Chats;