"use strict";

define( [
    'underscore',
    'backbone',
    'model/Page'
], function ( _, Backbone, Page ) {

    /**
     *  @constructor
     *  @augments module:Backbone.Collection
     */
    var Pages = Backbone.Collection.extend( {
        model:       Page,
        currentPage: null,

        initialize: function ( models, options ) {
            this.scenic = options.scenic;
        },

        /**
         * Get the current page
         *
         * @returns {*}
         */
        getCurrentPage: function () {
            if ( this.currentPage && this.currentPage.get( 'enabled' ) ) {
                return this.currentPage;
            } else if ( localStorage.getItem( 'currentPage' ) ) {
                var page = this.get( localStorage.getItem( 'currentPage' ) );
                if ( page && page.get( 'enabled' ) ) {
                    return page;
                }
            }
            return this.get( this.scenic.config.defaultPanelPage );
        },
        /**
         * Get the current page
         *
         * @returns {*}
         */
        getPage: function ( id ) {
            return this.get( id );
        },
        /**
         * Set current page
         *
         * @param page
         */
        setCurrentPage: function ( page ) {
            if ( !page.get('enabled')) {
                return;
            }
            
            this.each( function ( page ) {
                page.set( 'active', false );
            } );

            this.currentPage = page;
            localStorage.setItem( 'currentPage', this.currentPage ? this.currentPage.get( 'id' ) : null );

            if ( this.currentPage ) {
                this.currentPage.set( 'active', true );
            }

            this.trigger( 'change:current', this.currentPage );
        }
    } );

    return Pages;
} );
