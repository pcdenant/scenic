"use strict";

define( [
    'underscore',
    'backbone',
    'model/base/BaseModel'
], function ( _, Backbone, BaseModel ) {

    /**
     * Base Collection
     * In case we need to extend Backbone.Collection
     *
     * @constructor
     * @extends Backbone.Collection
     * @exports client/model/base/BaseCollection
     */
    var BaseCollection = Backbone.Collection.extend( {
        model: BaseModel,

        /**
         * Initialize the collection
         * @override
         */
        initialize: function () {
            Backbone.Collection.prototype.initialize.apply( this, arguments );
        },

        /**
         * Destroys the collection, triggering ```destroy``` on every child model.
         */
        destroy: function () {
            var model;
            while ( model = this.first() ) {
                model.trigger( 'destroy', model, this );
            }
        }
    } );

    return BaseCollection;
} );
