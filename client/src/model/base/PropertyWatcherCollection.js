"use strict";

define( [
    'underscore',
    'backbone',
    'backbone.cocktail',
    'model/mixins/PropertyWatcher'
], function ( _, Backbone, Cocktail, PropertyWatcher ) {

    /**
     * Watches the value of a property on a quiddity and creates a collection from the value of the property.
     * 
     * This is used to track when the quiddity and/or property is added/removed because in most circumstances we know 
     * what we need to track but the quiddity or property is not yet synced with the client. 
     *
     * @constructor
     * @extends Backbone.Collection
     * @mixes module:client/model/mixins/PropertyWatcher
     * @exports client/model/base/PropertyWatcherCollection
     */
    var PropertyWatcherCollection = Backbone.Collection.extend( {

        /**
         * @inheritdoc
         */
        modelOptions: {},

        /**
         * @override
         */
        initialize: function ( models, options ) {
            //
        },

        /**
         * Set the watched property value as our collection when it changes.
         * This is called by the PropertyWatcher mixin.
         */
        propertyChanged: function ( value ) {
            if ( value ) {
                this.set( value, this.modelOptions );
            } else {
                this.reset();
            }
        }
    } );

    // Mixin the PropertyWatcher
    Cocktail.mixin( PropertyWatcherCollection, PropertyWatcher );

    return PropertyWatcherCollection;
} );
