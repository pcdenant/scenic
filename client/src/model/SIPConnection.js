"use strict";

define( [
    'underscore',
    'backbone',
    'backbone.cocktail',
    'crypto-js',
    'model/sip/Contacts',
    'model/sip/Contact'
], function ( _, Backbone, Cocktail, CryptoJS, Contacts, Contact ) {

    /**
     * SIP Connection
     *
     * @constructor
     * @extends module:Backbone.Model
     */

    var SIPConnection = Backbone.Model.extend( {

        secretString: 'Les patates sont douces!',

        defaults: function () {
            return {
                connected:          false,
                login:              false,
                connecting:         false,
                server:             null,
                user:               null,
                port:               null,
                sameLogin:          false,
                stunServer:         null,
                turnServer:         null,
                groupDiscussion:    false,
                turnUser:           null
            }
        },

        statuses: [
            'available',
            'online',
            'busy',
            'away',
            'offline'
        ],

        /**
         * Initialize
         */
        initialize: function ( attributes, options ) {
            this.scenic = options.scenic;

            var sip = JSON.parse( localStorage.getItem( 'sip' ) );
            this.set( 'server', this.scenic.config.sip.commandLine || sip == undefined || sip.server == undefined ? this.scenic.config.sip.server : sip.server );
            this.set( 'port', this.scenic.config.sip.commandLine || sip == undefined || sip.port == undefined ? this.scenic.config.sip.port : sip.port );
            this.set( 'user', this.scenic.config.sip.commandLine || sip == undefined || sip.user == undefined ? this.scenic.config.sip.user : sip.user );
            this.set( 'sameLogin', sip == undefined || sip.sameLogin == undefined ? this.scenic.config.sip.sameLogin : sip.sameLogin );
            this.set( 'stunServer', sip == undefined || sip.stunServer == undefined ? this.scenic.config.sip.stunServer : sip.stunServer );
            this.set( 'turnServer', sip == undefined || sip.turnServer == undefined ? this.scenic.config.sip.turnServer : sip.turnServer  );
            this.set( 'turnUser', sip && sip.turnUser ? sip.turnUser : null );
            this.set( 'uri', sip && sip.uri ? sip.uri : null );

            this.contacts = new Contacts( null, { sip: this, scenic: this.scenic } );

            // Here we have to set up a watcher on quiddities updates because SIP is not always available
            // and we need to track properties on whichever instance will come up from the network
            this.listenTo( this.scenic.quiddities, 'add', this._onQuiddityAdded );
            this.listenTo( this.scenic.quiddities, 'remove', this._onQuiddityRemoved );
            this.listenTo( this.scenic.quiddities, 'reset', this._onQuidditiesReset );

            if ( this.scenic.quiddities.get( this.scenic.config.sip.quiddName ) ) {
                this._registerSipQuiddity( this.scenic.quiddities.get( this.scenic.config.sip.quiddName ) );
            }

            this.on( 'change:sameLogin', () => {
                this._saveToLocalStorage();
            } );

            this.on( 'destroy', () => {
                this.contacts.reset();
            } );
        },

        _registerSipQuiddity: function ( sipQuiddity ) {
            this.quiddity = sipQuiddity;
            this.contacts.setSipQuiddity( this.quiddity );

            this.listenTo( this.quiddity.properties.get( 'sip-registration' ), 'change:value', this._onSipRegistrationChange );
            this._onSipRegistrationChange();
            this.listenTo( this.quiddity.properties.get( 'port' ), 'change:value', this._onSipPortChange );
            this._onSipPortChange();
        },

        _onQuiddityAdded: function ( model, collection, options ) {
            if ( model.id == this.scenic.config.sip.quiddName ) {
                this._registerSipQuiddity( model );
            }
        },

        _onQuiddityRemoved: function ( model, collection, options ) {
            if ( model.id == this.scenic.config.sip.quiddName ) {
                if ( this.quiddity ) {
                    this.stopListening( this.quiddity.properties.get( 'sip-registration' ), 'change:value', this._onSipRegistrationChange );
                    this.stopListening( this.quiddity.properties.get( 'port' ), 'change:value', this._onSipPortChange );
                    this.quiddity = null;
                }
            }
        },

        /**
         * When the quiddities collection resets (a new file was loaded) do both the removal and adding of the sip quiddity.
         *
         * @param collection
         * @param options
         * @private
         */
        _onQuidditiesReset: function ( collection, options ) {
            if ( this.quiddity ) {
                this.stopListening( this.quiddity.properties.get( 'sip-registration' ), 'change:value', this._onSipRegistrationChange );
                this.stopListening( this.quiddity.properties.get( 'port' ), 'change:value', this._onSipPortChange );
                this.quiddity = null;
            }

            var sipQuiddity = collection.get( this.scenic.config.sip.quiddName );
            if ( sipQuiddity ) {
                this._registerSipQuiddity( sipQuiddity );
            }
        },

        /**
         * Handler for when the watched property 'sip-registration' changes.
         */
        _onSipRegistrationChange: function () {
            if ( this.quiddity ) {
                var connected = this.quiddity.properties.get( 'sip-registration' ).get( 'value' );
                this.set( 'connected', connected );
            }
        },

        /**
         * Handler for when the watched property 'port' changes.
         */
        _onSipPortChange: function () {
            if ( this.quiddity ) {
                var port = this.quiddity.properties.get( 'port' ).get( 'value' );
                this.set( 'port', port );
            }
        },


        /**
         * Login
         *
         * @param server
         * @param port
         * @param user
         * @param password
         * @param stunServer
         * @param turnServer
         * @param turnUser
         * @param turnPassword
         * @param callback
         */
        login: function ( server, port, user, password, stunServer, turnServer, turnUser, turnPassword, callback ) {
            var self = this;

            const sameLogin = this.get( 'sameLogin' );

            // Save for next time
            this.set( 'server', server == null ? this.get( 'server' ) : server );
            this.set( 'port', _.isEmpty( port ) ? this.get( 'port' ) : port );
            this.set( 'user', user );
            this.set( 'uri', this.get( 'user' ) + '@' + this.get( 'server' ) );
            this.set( 'stunServer', stunServer == null ? this.get( 'stunServer' ) : stunServer );
            this.set( 'turnServer', turnServer == null ? this.get( 'turnServer' ) : turnServer );
            this.set( 'turnUser', sameLogin ? this.get( 'user' ) : ( turnUser == null ? this.get( 'turnUser' ) : turnUser ) );

            this.set( 'connecting', true );

            this.scenic.sessionChannel.vent.trigger( 'sip:login' );

            const encryptedPassword = CryptoJS.AES.encrypt( password, this.secretString ).toString();
            var credentials         = {
                server:       this.get( 'server' ),
                port:         this.get( 'port' ),
                user:         this.get( 'user' ),
                password:     encryptedPassword,
                stunServer:   this.get( 'stunServer' ),
                turnServer:   this.get( 'turnServer' ),
                turnUser:     sameLogin ? this.get( 'user' ) : this.get( 'turnUser' ),
                turnPassword: sameLogin ? encryptedPassword : CryptoJS.AES.encrypt( turnPassword, this.secretString ).toString()
            };

            this._saveToLocalStorage();

            this.scenic.socket.emit( 'sip.login', credentials,  ( error, success ) => {
                this.set( 'connecting', false );
                this.set( 'login', true );
                if ( error ) {
                    this.scenic.sessionChannel.vent.trigger( 'sip:loggedout', error );
                    this.scenic.sessionChannel.vent.trigger( 'error', error );
                    if ( callback ) {
                        callback( error, success );
                    }
                    return;
                }
                this.scenic.sessionChannel.vent.trigger( 'sip:loggedin', this.get('uri') );

                if ( callback ) {
                    callback( null, success );
                }
            } );
        },

        /**
         * Logout
         */
        logout: function () {
            this.scenic.socket.emit( 'sip.logout',  error => {
                this.set( 'login', false );
                if ( error ) {
                    this.scenic.sessionChannel.vent.trigger( 'error', error );
                }
                this.scenic.sessionChannel.vent.trigger( 'sip:loggedout', error );
            } );
        },

        /**
         * Add Contact
         *
         * @param uri
         */
        addContact: function ( uri ) {
            var self    = this;
            var contact = new Contact( { uri: uri }, { scenic: self.scenic } );
            contact.save( null, {
                success: function () {
                    //no-op
                },
                error:   function ( error ) {
                    self.scenic.sessionChannel.vent.trigger( 'error', error );
                }
            } );
        },

        _saveToLocalStorage() {
            localStorage.setItem( 'sip', JSON.stringify( {
                server:     this.get( 'server' ),
                port:       this.get( 'port' ),
                user:       this.get( 'user' ),
                uri:        this.get( 'uri' ),
                stunServer: this.get( 'stunServer' ),
                turnServer: this.get( 'turnServer' ),
                turnUser:   this.get( 'turnUser' ),
                sameLogin:  this.get( 'sameLogin' )
            } ) );
        }
    } );

    return SIPConnection;
} );