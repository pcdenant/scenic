"use strict";

define( [
    'underscore'
], function ( _ ) {

    /**
     * Property Watcher Mixin
     * Lets the class mixing it watch for changes on a quiddity property.
     * Neither the quiddity or the property need to exists to watch for it. This mixin is intended to
     * simplify the work related to first watching for the quiddity existence, then the property existence
     * and only then watch for changes to that property.
     *
     * @mixin
     * @exports client/model/mixins/PropertyWatcher
     */
    var PropertyWatcher = {

        /**
         * The quiddity to watch for
         * @abstract
         * @type {string}
         */
        quiddityId: null,

        /**
         * The property to watch for
         * @abstract
         * @type {string}
         */
        propertyName: null,

        /**
         * The key to look for in the property in cases where the property value is an object and we only want part
         * of that object. ie: some properties return an object with a single key containing an array.
         * @abstract
         * @type {string}
         */
        key: null,

        /**
         * Initialize
         * @override
         */
        initialize: function(models, options) {
            this.scenic = options.scenic;

            // Setup listeners for quiddity additions/removals
            this.listenTo( this.scenic.quiddities, 'add', this._onQuiddityAdded );
            this.listenTo( this.scenic.quiddities, 'remove', this._onQuiddityRemoved );

            // Setup the sip quiddity (if it already exists)
            this.quiddity = this.scenic.quiddities.get( this.quiddityId );
            this._registerQuiddity();
        },

        /**
         * Quiddity Added Handler
         * Checks if the added quiddity is the one we are watching for and start watching for its properties.
         *
         * @private
         * @param {Object} quiddity
         * @param {Object[]} quiddities
         * @param {Object} options
         */
        _onQuiddityAdded: function ( quiddity, quiddities, options ) {
            if ( quiddity.id == this.quiddityId ) {
                this.quiddity = quiddity;
                this._registerQuiddity();
            }
        },

        /**
         * Quiddity Removed Handler
         * Stops watching the quiddity's properties when it is removed
         *
         * @private
         * @param {Object} quiddity
         * @param {Object[]} quiddities
         * @param {Object} options
         */
        _onQuiddityRemoved: function ( quiddity, quiddities, options ) {
            if ( quiddity.id == this.quiddityId ) {
                this._unregisterQuiddity();
            }
        },

        /**
         * Registers the quiddity event handlers
         * If the property we are watching is available, subscribe to its value change otherwise,
         * watch for property addition/removal in order to then track changes to the property's value.
         *
         * @private
         */
        _registerQuiddity: function () {
            if ( this.quiddity ) {
                this.listenTo( this.quiddity.properties, 'add', this._quiddityPropertyAdded );
                this.listenTo( this.quiddity.properties, 'remove', this._quiddityPropertyRemoved );

                var property = this.quiddity.properties.get( this.propertyName );
                if ( property ) {
                    this.listenTo( property, 'change:value', this._checkProperty );
                }
            }

            this._checkProperty();
        },

        /**
         * Unregisters the quiddity event handlers
         * Will stop watching for property addition/removal and/or property value change
         *
         * @private
         */
        _unregisterQuiddity: function () {
            if ( this.quiddity ) {
                this.stopListening( this.quiddity.properties, 'add', this._quiddityPropertyAdded );
                this.stopListening( this.quiddity.properties, 'remove', this._quiddityPropertyRemoved );

                var property = this.quiddity.properties.get( this.propertyName );
                if ( property ) {
                    this.stopListening( property, 'change:value', this._checkProperty );
                }

                this.quiddity = null;
            }

            this._checkProperty();
        },

        /**
         * Property Added Handler
         * Checks if the quiddity now has the property that we are watching, then subscribe to its change in value.
         *
         * @param {string} property
         * @param {Array} properties
         * @param {Object} options
         * @private
         */
        _quiddityPropertyAdded: function ( property, properties, options ) {
            if ( property.id == this.propertyName ) {
                this.listenTo( this.quiddity.properties.get( this.propertyName ), 'change:value', this._checkProperty );
                this._checkProperty();
            }
        },

        /**
         * Property Removed Handler
         * Stops watching the property for value changes since it doesn't exist anymore
         *
         * @param property
         * @param properties
         * @param options
         * @private
         */
        _quiddityPropertyRemoved: function ( property, properties, options ) {
            if ( property.id == this.propertyName ) {
                this.stopListening( this.quiddity.properties.get( this.propertyName ), 'change:value', this._checkProperty );
                this._checkProperty();
            }
        },

        /**
         * Checks the property for a change, get the watched value and call the propertyChanged method to notify
         * a mixin user of the change.
         */
        _checkProperty: function () {
            if ( this.quiddity ) {
                var property = this.quiddity.properties.get( this.propertyName );
                if ( property && property.get('value') !== null ) {
                    this.propertyChanged( this.key ? property.get( 'value' )[this.key] : property.get('value') );
                    return;
                }
            }
            this.propertyChanged( null );
        },

        /**
         * Property Changed Handler
         *
         * @abstract
         * @param {*} value - Property value
         */
        propertyChanged: function( value ) {
            //noop
        }
    };

    return PropertyWatcher;

} );