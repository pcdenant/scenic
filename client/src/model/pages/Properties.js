"use strict";

import Backbone    from "backbone";
import _           from 'underscore';
import i18n        from 'i18n';
import Page        from 'model/Page';

/**
 * Properties Page
 *
 * @constructor
 * @extends module:Backbone.Model
 */

var Properties = Page.extend( {
    idQuiddities: [],

    defaults: function () {
        return {
            id:          "properties",
            name:        i18n.t( 'Properties' ),
            type:        'properties',
            fullscreen:   false,
            description: i18n.t( "Display the properties of a quiddity" )
        }
    },

    /**
     * Initialize
     */
    initialize: function (attributes, options) {
        Page.prototype.initialize.apply( this, arguments );
    },

    /**
     * Activate a page
     */
    activate: function ( idQuiddity ) {
        this.set( 'quidditySelected', idQuiddity );
        this.collection.setCurrentPage( this );
    },
} );

export default Properties;