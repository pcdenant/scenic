"use strict";

import i18n from "i18n";
import Table from "model/pages/base/Table";

const Matrix = Table.extend( {
    defaults: function () {
        return {
            id:          "matrix",
            name:        i18n.t( 'Edit Scene' ),
            type:        'matrix',
            description: i18n.t( "Manage audio/video devices and network connections" )
        }
    },

    /**
     * Initialize
     */
    initialize: function ( attributes, options ) {
        Table.prototype.initialize.apply( this, arguments );

        this.scenic.sessionChannel.vent.on( 'file:reset', this._onTreeQuiddity, this  );
        this.scenic.sessionChannel.vent.on( 'file:loaded', this._onTreeQuiddity, this  );

        this.listenScene();
    },

    _onTreeQuiddity: function ( ) {
        this.scenic.refreshScenes();
        this.listenScene();
    },

    listenScene: function () {
        if( this.scenic.scenes.length == 1 && !this.scenic.scenes.getActiveScene() ) {
            this.scenic.scenes.activateScene( 'defaultScene' );
        }

        this.set('scenes', this.scenic.scenes);
    },

    //  ███████╗██╗███╗   ██╗██╗  ██╗
    //  ██╔════╝██║████╗  ██║██║ ██╔╝
    //  ███████╗██║██╔██╗ ██║█████╔╝
    //  ╚════██║██║██║╚██╗██║██╔═██╗
    //  ███████║██║██║ ╚████║██║  ██╗
    //  ╚══════╝╚═╝╚═╝  ╚═══╝╚═╝  ╚═╝

    getSinkSourceCollection: function () {
        return this.getQuidditySourceCollection();
    },

    /**
     * Source sort comparator function
     *
     * @param {Object} source
     * @returns {*}
     */
    sinkSourceComparator: function ( source ) {
        return this.quidditySourceComparator( source );
    },

    /**
     * Get a list of possible source classes for the menu
     *
     * Override in concrete table classes to retrieve the actual collection
     *
     * @returns {ClassDescription[]}
     */
    getSinkSources: function () {
        return this.scenic.classes.filter( function ( classDescription ) {
            return this._filterClass( classDescription, ['writer'] );
        }, this );
    },

    /**
     * Filter source for this table
     *
     * Override in concrete table classes to filter the actual collection
     *
     * @param {Shmdata} source
     * @param {boolean} useFilter
     * @returns {Quiddity[]}
     */
    filterSinkSource: function ( source, useFilter ) {
        const classDescription = source.get( 'classDescription' );
        const tags             = classDescription.get( 'tags' );

        // Determine if the quiddity is an occasional-writer and if it is, then if we should display it or not
        const isOccasional     = _.contains( tags, 'occasional-writer' );
        const showIfOccasional = !isOccasional || (isOccasional && source.shmdatas.where( { type: 'writer' } ).length > 0 );

        // If the previous rule is true, also check with the quiddity filter if we should display it
        return source.id != 'SIP' && showIfOccasional && this._filterQuiddity( source, ['writer', 'occasional-writer'], false, useFilter, true );
    },

    /**
     * Get destination collection
     *
     * This is the collection from which destinations are picked/filtered. This is not
     * the actual displayed destinations, only the collection from where they are chosen.
     *
     * @returns {Quiddities}
     */
    getSinkDestinationCollection: function () {
        return this.scenic.quiddities;
    },

    /**
     * Get a list of possible destination classes for the menu.
     *
     * @returns {ClassDescription[]}
     */
    getSinkDestinations: function () {
        return this.scenic.classes.filter( function ( classDescription ) {
            return this._filterClass( classDescription, ['reader'] );
        }, this );
    },

    /**
     * Filter destination for this table
     *
     * @param {Quiddity|Object} destination
     * @param {boolean} useFilter
     * @returns {Quiddity[]}
     */
    filterSinkDestination: function ( destination, useFilter ) {
        return this._filterQuiddity( destination, ['reader'], false, useFilter );
    },

    /**
     * Destination sort comparator function
     *
     * @param {Object} destination
     * @returns {*}
     */
    sinkDestinationComparator: function ( destination ) {
        return destination.getUserData( "order.destination" );
    },


    //   ██████╗ ██████╗ ███╗   ██╗████████╗ █████╗  ██████╗████████╗
    //  ██╔════╝██╔═══██╗████╗  ██║╚══██╔══╝██╔══██╗██╔════╝╚══██╔══╝
    //  ██║     ██║   ██║██╔██╗ ██║   ██║   ███████║██║        ██║
    //  ██║     ██║   ██║██║╚██╗██║   ██║   ██╔══██║██║        ██║
    //  ╚██████╗╚██████╔╝██║ ╚████║   ██║   ██║  ██║╚██████╗   ██║
    //   ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝   ╚═╝

    /**
     * Get destination collection
     *
     * @returns {Contacts}
     */
    getContactDestinationCollection: function () {
        return this.scenic.sip.contacts;
    },

    /**
     * Get destinations
     *
     * @returns {Contacts}
     */
    getContactDestinations() {
        return this.getContactDestinationCollection().filter( contact  => {
            return !contact.get( 'self' );
        } );
    },
    /**
     * Get destinations
     *
     * @returns {Contacts}
     */
    getActiveContactDestinations() {
        return this.getContactDestinationCollection().filter(  contact => {
            return !contact.get( 'self' ) && this.filterContactDestination( contact );
        } );
    },
    /**
     * Filter destination for SIP
     * Shows both contacts with connections and contacts flagged to be shown temporarily
     *
     * @inheritdoc
     */
    filterContactDestination: function ( destination, useFilter ) {
        return ( destination.has( 'connections' ) && destination.get( 'connections' ).length > 0 ) || destination.get( 'showInDestinations' );
    },

    /**
     * @inheritdoc
     */
    contactDestinationComparator: function ( destination ) {
        return destination.get( 'uri' );
    },

    /**
     * Retrieve the connection between a source and destination
     */
    getContactConnection: function ( source, destination ) {
        // console.log(destination.id + '  ', source)
        return destination.get( 'connections' ) && _.isArray( destination.get( 'connections' ) ) && destination.get( 'connections' ).indexOf( source.get( 'path' ) ) != -1 ? source.get( 'path' ) : null;
    },

    /**
     * @inheritdoc
     */
    isContactConnected: function ( source, destination ) {
        return this.getContactConnection( source, destination ) != null;
    },

    /**
     * @inheritdoc
     */
    canConnectContact: function ( source, destination, callback ) {
        var isRaw = source.get( 'category' ) == 'video';
        // No possible to resend a source to its transmitter
        var sameDestination = source.get('uri') == destination.id;
        var can   = !isRaw && !sameDestination;
        callback( can );
        return can;
    },

    /**
     * @inheritdoc
     */
    connectContact: function ( source, destination, cb ) {
        this.scenic.socket.emit( 'sip.contact.attach', destination.id, source.get( 'path' ), error => {
            if ( error ) {
                this.scenic.sessionChannel.vent.trigger( 'error', error );
            } else {
                if( cb ) {
                    cb();
                }
            }
        } );
    },

    /**
     * @inheritdoc
     */
    disconnectContact: function ( source, destination ) {
        this.scenic.socket.emit( 'sip.contact.detach', destination.id, source.get( 'path' ), error => {
            if ( error ) {
                this.scenic.sessionChannel.vent.trigger( 'error', error );
            }
        } );
    }
} );

export default Matrix;