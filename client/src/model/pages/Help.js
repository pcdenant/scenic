"use strict";

define( [
    'underscore',
    'backbone',
    'i18n',
    'model/Page'
], function ( _, Backbone, i18n, Page ) {

    /**
     * Help Page
     *
     * @constructor
     * @extends Page
     */

    var Help = Page.extend( {

        defaults: function () {
            return {
                id:          "help",
                name:        i18n.t( 'Help' ),
                type:        'help',
                description: i18n.t( "Learn how to use Scenic" )
            }
        },

        mutators: {
            markdown: {
                transient: true,
                get: function() {
                    const lang = i18n.lng();
                    let markdown;
                    try {
                        markdown = require(`./../../../HELP_${lang}.md`);
                    } catch(err) {
                        console.warn('Could not find help file');
                        markdown = i18n.t('Could not find help file for locale "__locale__"', { locale: lang } );
                    }
                    return markdown;
                }
            }
        },

        /**
         * Initialize
         */
        initialize: function (attributes, options) {
            Page.prototype.initialize.apply( this, arguments );
        }
    } );

    return Help;
} );