"use strict";

import Backbone from "backbone";
import _            from 'underscore';
import UserDataWatcherModel from './base/UserDataWatcherModel';

var saveContacts = UserDataWatcherModel.extend( {

    defaults: function () {
        return {
            contacts: []
        }
    },

    methodMap: {
        'create': function () {
            return ['quiddity.userData.set.branch', this.quiddityId, this.userDataPath, JSON.stringify(this) ]
        },
        'update': function () {
            return ['quiddity.userData.set.branch', this.quiddityId, this.userDataPath, JSON.stringify(this) ]
        },
        'patch':  null,
        'delete': function () {
            return ['quiddity.userData.remove', this.quiddityId, this.userDataPath ]
        },
        'read': function () {
            return ['quiddity.userData.get', this.quiddity.id, this.userDataPath]
        },
    },

    /**
     * Initialize
     */
    initialize: function ( models, options ) {
        this.scenic         = options.scenic;
        this.userDataPath   = 'contactsSelected';
        this.quiddityId     = this.scenic.config.userTree.quiddName;

        UserDataWatcherModel.prototype.initialize.apply( this, arguments );

    },

    _onRemoved: function ( idContact ) {
        this.remove( idContact );
    },

    add: function ( idContact ) {
        if ( !this.isContactExist( idContact ) ) {
            var contacts = _.clone(this.get('contacts'));
            contacts.push( idContact );
            this.save({contacts});
        }
    },

    remove: function ( idContact ) {
        if( this.isContactExist( idContact ) ) {
            var contacts = _.clone(this.get('contacts'));
            _.each( this.get('contacts'),  ( contact, index ) => {
                if ( contact == idContact ) {
                    contacts.splice(index, 1);
                }
            });

            this.save({contacts}, {silent: true});
        }
    },

    isContactExist: function ( idContact ) {
        var result = false;

        _.each( this.get('contacts'), contact  => {
            if (contact == idContact ) {
                result = true;
            }
        });

        return result;
    },

    userDataChanged: function( saveContacts ) {
        if ( saveContacts ) {
            if( this.get('contacts') !== saveContacts.contacts ) {
                this.set('contacts', JSON.parse(JSON.stringify( saveContacts.contacts )));
                this.trigger( 'change:contacts' );
            }
        }
    }

} );

export default saveContacts;