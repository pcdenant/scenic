"use strict";

import Backbone from 'backbone';
// import ScenicModel from "./base/ScenicModel";
/**
 * Session
 *
 * @constructor
 * @extends module:Backbone.Model
 */
const Chat = Backbone.Model.extend( {

    defaults: function () {
        return {
            id: null,
            type: 'chat',
            target: null,
            nicknames: [],
            message: null
        }
    },
    /**
     * Initialize the model
     * @override
     */
    initialize: function ( attributes, options ) {
        this.scenic         = options.scenic;

        this.set({ "id": options.id });
        this.set({ "nicknames": options.nicknames });

        this.target =  this.id;
    },

    receiveMessage: function ( username, message) {
        if ( this.get("message") && message == this.get("message").mess ) {
            this.trigger('change:message')
        } else {
            this.set({"message": {'username': username, 'mess': message}});
        }
    },

    sendMessage: function ( message ) {
        if( this.target.indexOf('#') == 0) {
            //this is a group
            this.scenic.socket.emit('irc.send.message', this.target, message);
        } else {
            // this is a private message
            var dest = this.target.replace(/[^a-zA-Z0-9]/g, '_').substring(0, 15);
            this.scenic.socket.emit('irc.send.message', dest , message);
        }

    },

    setNicknames: function ( nicknames ) {
        this.set({ "nicknames": nicknames });
        this.trigger('change:nicknames')
    },

    disconnect: function ( ) {
        this.destroy();
    }

} );

export default Chat;