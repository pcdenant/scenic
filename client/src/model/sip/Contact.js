"use strict";

define( [
    'underscore',
    'backbone',
    'model/base/ScenicModel'
], function ( _, Backbone, ScenicModel ) {

    /**
     * SIP Contact
     *
     * @constructor
     * @extends ScenicModel
     */

    var Contact = ScenicModel.extend( {

        defaults: {
            status:             'offline',
            status_text:        '',
            subscription_state: '',
            uri:                null,
            send_status:        null,
            recv_status:        null,
            name:               null,
            chatConnected:      false,
            onCall:             false,
            onHangUp:           false,
            notBother:          false,
            selected:           false,
            connections:        [],
            actions:            { 'call': { show : true, disable: true }, 'hangup': false, 'remove': true },
            whitelisted:        true, 
            // Dynamic
            self:               false,
            showInDestinations: false
        },

        methodMap: {
            'create': function () {
                return ['sip.contact.add', this.get( 'uri' )];
            },
            'update': function () {
                return ['sip.contact.update', this.get( 'uri' ), {
                    name:        this.get( 'name' ),
                    status:      this.get( 'status' ),
                    status_text: this.get( 'status_text' ),
                    group:       this.get( 'group' )
                }];
            },
            'patch':  null,
            'delete': function () {
                return ['sip.contact.remove', this.get( 'uri' )];
            },
            'read':   null
        },

        /**
         * Initialize
         */
        initialize: function ( attributes, options ) {
            ScenicModel.prototype.initialize.apply( this, arguments );
            this.scenic.sessionChannel.vent.on( 'file:reset', this._onFileReset.bind( this ) );
            // set the actions  
            this._updateActions();

            if(this.get('connections').length > 0 ) {
                this.set( 'showInDestinations', true );
            }

            this.listenTo(this, 'change:connections', this._updateActions);
            this.listenTo(this.scenic.saveContacts, 'change:contacts', _.bind( this._updateShowInDestinations, this ) );
            this.listenTo(this, 'change:send_status', this._updateActions);
            this.scenic.sessionChannel.vent.on( 'chat:connected', this._onChatConnected, this );
        },

        _updateActions: function(){
            var calling = this.get('send_status') == 'calling' ? true : false;
            var connections = this.get('connections').length < 1 ? true : false;
            this.set( 'actions', { 'call': { show : !calling, disabled: connections }, 'hangup': calling , 'remove': true } );   
        },

        _updateShowInDestinations: function(){
            var savedContact = false;
            _.each(this.scenic.saveContacts.get('contacts'), contact => {
                if ( contact == this.id ) {
                    savedContact = true;
                    if ( this.get('showInDestinations') == false  ) {
                        this.set('showInDestinations', true);
                        this.scenic.sessionChannel.vent.trigger('contact:added', this.id);
                        // A little hack here to trigger marionette's rendering of the CollectionView
                        if (this.collection) {
                            this.collection.trigger('reset');
                        }
                    }
                }
            });

            if ( !savedContact &&  this.get('showInDestinations') ) {
                this.set( 'showInDestinations', false );
                this.scenic.sessionChannel.vent.trigger( 'contact:removed', this.id );
                // A little hack here to trigger marionette's rendering of the CollectionView
                if (this.collection) {
                    this.collection.trigger('reset');
                }
            }
        },
        /**
         * Call Contact
         */
        call: function ( cb ) {
            if ( this.get('connections').length > 0 ) {
                this.set('onCall', true)
                this.scenic.socket.emit( 'sip.contact.call', this.id, ( error, called ) => {
                    this.set('onCall', false)
                    if ( error ) {
                        this.scenic.sessionChannel.vent.trigger( 'error', error );
                    } else {
                        this._updateActions();
                    }
                } );
            }
        },

        /**
         * Hang Up Contact
         */
        hangUp: function ( cb ) {
            this.set('onHangUp', true)
            this.scenic.socket.emit( 'sip.contact.hangup', this.id, ( error ) => {
                this.set('onHangUp', false)
                if ( error ) {
                    this.scenic.sessionChannel.vent.trigger( 'error', error );
                } else {
                    this._updateActions();
                }
            } );
        },

        _onChatConnected: function () {
            this.set('chatConnected', true);
        },
        /**
         * Disconnect All
         * Hangs up and removes all shmdata connections
         */
        disconnectAll: function () {
            // Start by removing the flag pinning the contact as a destination
            // It won't remove it from the table if it has connections but will prevent
            // it from sticking there as we explicitly want to remove it
            this.scenic.saveContacts.remove(this.id);
            this.set( 'showInDestinations', false );
            this.scenic.sessionChannel.vent.trigger( 'contact:removed', this.id );
            // A little hack here to trigger marionette's rendering of the CollectionView
            this.collection.trigger( 'reset' );

            // Then notify the server we want to disconnect this contact from everything
            // Later signals will then remove it from the table as it is not sticky anymore
            this.scenic.socket.emit( 'sip.contact.disconnect', this.id, function ( error ) {
                if ( error ) {
                    self.scenic.sessionChannel.vent.trigger( 'error', error );
                }
            } );
        },

        /**
         * authorize Contact
         */
        authorize: function ( authorized, cb ) {
            this.scenic.socket.emit( 'sip.contact.authorize', this.id, authorized,  ( error )  => {
                if ( error ) {
                    this.scenic.sessionChannel.vent.trigger( 'error', error );
                }
                if ( cb ) {
                    cb( error );
                }
            } );
        },

        /**
         * blocks all Contact
         */
        blockAllContact: function ( cb ) {
            this.scenic.socket.emit( 'sip.contact.block', ( error )  => {
                if ( error ) {
                    this.scenic.sessionChannel.vent.trigger( 'error', error );
                }
                if ( cb ) {
                    cb( error );
                }
            } );
        },

        /**
         * unblocks all Contact
         */
        unblockAllContact: function ( cb ) {
            this.scenic.socket.emit( 'sip.contact.unblock', ( error )  => {
                if ( error ) {
                    this.scenic.sessionChannel.vent.trigger( 'error', error );
                }
                if ( cb ) {
                    cb( error );
                }
            } );
        },

        /**
         * blocks all Contact
         */
        isNotBotherMode: function () {
            this.scenic.socket.emit( 'sip.contact.notBother', ( error, result )  => {
                if ( error ) {
                    this.scenic.sessionChannel.vent.trigger( 'error', error );
                }
                this.set( 'notBother', result );
            } );
        },

        /**
         *  Edit Contact
         */
        edit: function () {
            var self = this;
            this.scenic.sessionChannel.commands.execute( 'contact:edit', this, function ( info ) {
                self.set( info );
                self.save();
            } );
        },
        
        /**
         * Add a potential destination
         * Flag the contact to be temporarily shown as destination
         * Normally only contacts with connections appear as a destination
         */
        addAsDestination: function() {
            this.set( 'showInDestinations', true );
            this.scenic.saveContacts.add(this.id);
            this.scenic.sessionChannel.vent.trigger( 'contact:added', this.id );
            // A little hack here to trigger marionette's rendering of the CollectionView
            this.collection.trigger( 'reset' );
        },

        /**
         * File Reset Handler
         * When creating a new file we want to remove all temporarily added contacts from the board
         *
         * @private
         */
        _onFileReset() {
            this.scenic.saveContacts.remove(this.id);
            this.set( 'showInDestinations', false );
        }
    } );

    return Contact;
} );