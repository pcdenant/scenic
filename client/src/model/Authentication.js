"use strict";

define( [
    'underscore',
    'backbone'
], function ( _, Backbone  ) {

    /**
     * Authentication
     *
     * @constructor
     * @extends module:Backbone.Model
     */

    var Authentication = Backbone.Model.extend( {

        defaults: {
            message: ''
        },

        /**
         * Initialize
         */
        initialize: function () {
            this.set( 'message', '' );
        },

        login: function ( password ){
            var credentials = { "username": "admin","password": password };            
            var self = this;

            Backbone.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: '/login',
                dataType: 'json',
                data: JSON.stringify(credentials),
                success: function(data, textStatus, jqXHR){
                    window.location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown){
                    if ( jqXHR.responseText == "Unauthorized" ) {
                        self.set('message', 'Wrong password')
                    }
                }
            });
        },

        /**
         * GET /enable to delete the actual user
         */
        isEnabled: function (cb){
            var self = this;

            Backbone.ajax({
                type: 'GET',
                contentType: 'application/json',
                url: '/enable',
                dataType: "json",
                success: function(data, textStatus, jqXHR){
                    cb( null, data );
                },
                error: function(jqXHR, textStatus, errorThrown){
                    cb( errorThrown );
                }
            });
        },

        /**
         * POST /enable to delete the actual user
         */
        enable: function ( enable, cb ){
            var self = this;

            Backbone.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: '/enable',
                dataType: "json",
                data: JSON.stringify( { enable:enable } ),
                success: function(data, textStatus, jqXHR){
                    cb( null, data );
                },
                error: function(jqXHR, textStatus, errorThrown){
                    cb( errorThrown );
                }
            });
        },

        /**
         * Call the route /reset to delete the actual user
         */
        resetLogin: function (cb){
            var self = this;
            Backbone.ajax({
                type: 'GET',
                contentType: 'application/json',
                url: '/reset',
                dataType: "json",
                success: function(data, textStatus, jqXHR){
                    cb( null, data );
                },
                error: function(jqXHR, textStatus, errorThrown){
                    cb( errorThrown );
                }
            });
        },

        /**
         * Call the route /signup to create a user with the username admin
         */
        createLogin: function (credentials, cb){

            var self = this;
            Backbone.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: '/signup',
                dataType: "json",
                data: JSON.stringify(credentials),
                success: function(data, textStatus, jqXHR){
                    cb( null, data );
                },
                error: function(jqXHR, textStatus, errorThrown){
                    cb( errorThrown );
                }
            });
        }
    } );

    return Authentication;
} );