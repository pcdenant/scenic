"use strict";

import _ from 'underscore';
import Backbone from 'backbone';
import Session from './Session';

/**
 *  @constructor
 *  @augments module:Backbone.Collection
 */
const Sessions = Backbone.Collection.extend( {
    model: Session,

    initialize: function ( models, options ) {

    },

    /**
     * Get the current session
     *
     * @returns {Session}
     */
    getCurrentSession: function () {
        return this.findWhere( { active: true } );
    },

    /**
     * Set current Session
     *
     * @param {Session} session
     */
    setCurrentSession: function ( session ) {
        this.each( function ( s ) {
            s.set( 'active', false );
        } );
        session.set( 'active', true );
        this.trigger( 'change:current', session );
    }
} );

export default Sessions;