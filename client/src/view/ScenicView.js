"use strict";

// Lib
import _ from 'underscore';
import Marionette from 'marionette';
import i18n from 'i18n';

// Page Model
import Pages from 'model/Pages';
import Page from 'model/Page';
import ControlPage from 'model/pages/Control';
import SettingsPage from 'model/pages/Settings';
import HelpPage from 'model/pages/Help';
import PropertiesPage from 'model/pages/Properties';
import MatrixPage from 'model/pages/Matrix';
import SwitchScenesPage from 'model/pages/SwitchScenes';
// Page View
import ControlView from './scenic/pages/ControlTableView';
import SettingsView from './scenic/pages/SettingsTableView';
import HelpView from './scenic/pages/HelpTableView';
import AdvancedView from './scenic/pages/AdvancedTableView';
import PropertiesView from './scenic/pages/PropertiesTableView';
import MatrixView from './scenic/pages/MatrixTableView';
import SwitchScenesView from './scenic/pages/SwitchScenesView';

// Inspector Model
import Inspectors from 'model/Inspectors';
import Inspector from 'model/Inspector';

// Inspector Mediator
import InspectorMediator from './scenic/InspectorMediator';

// Inspector Views
import InformationInspectorView from './scenic/inspector/InformationInspectorView';
import SIPInspectorView from './scenic/inspector/SIPInspectorView';
import FilesInspectorView from './scenic/inspector/FilesInspectorView';

// Misc Views
import TooltipsView from './scenic/Tooltips';
import LanguageView from './scenic/Language';
import TabsView from './scenic/Tabs';
import SystemUsageView from './scenic/SystemUsage';
import NotificationsView from './scenic/Notifications';
import InspectorView from './scenic/Inspector';
import PreviewView from './scenic/Preview';
import ChatsView from './scenic/chat/ChatsView';
import ShutdownView from './scenic/ShutdownView';
import DisconnectedView from './scenic/DisconnectedView';
import SideMenuView from './scenic/SideMenu';

// Template
import ScenicTemplate from './ScenicView.html';

// Misc
import NotificationsHandler from "./NotificationsHandler";
import CommandsHandler from "./CommandsHandler";

/**
 * Scenic View
 *
 * @constructor
 * @extends Marionette.LayoutView
 * @exports client/view/ScenicView
 */
export default Marionette.LayoutView.extend( {
    template:   ScenicTemplate ,
    className: 'session',

    regions: {
        tabs:        '.tabs',
        usage:       '.usage',
        lang:        '.lang',
        page:        '.page',
        inspector:   '.inspector',
        "side-menu": '.side-menu',
        preview:     '.preview',
        chats:       '.chats',
        tooltips:    '.tooltips',
        modal:       '.modal-container'
    },

    ui: {
        sidebar:     '.sidebar',
        tabs:        '.tabs'
    },

    modelEvents: {
        'change:connected': '_onConnectedChanged',
        'change:fullscreen': 'render'
    },

    keyShortcuts: {
        '1':        function () {
            this.showTab( 0 );
        },
        '2':        function () {
            this.showTab( 1 );
        },
        '3':        function () {
            this.showTab( 2 );
        },
        '4':        function () {
            this.showTab( 3 );
        },
        '5':        function () {
            this.showTab( 4 );
        },
        '6':        function () {
            this.showTab( 5 );
        },
        '7':        function () {
            this.showTab( 6 );
        },
        '8':        function () {
            this.showTab( 7 );
        },
        '9':        function () {
            this.showTab( 8 );
        },
        '0':        function () {
            this.showTab( 9 );
        },
        'i':        function () {
            this.scenic.sessionChannel.commands.execute( 'inspector:show', 'info' );
        },
        's':        function () {
            this.scenic.sessionChannel.commands.execute( 'inspector:show', 'settings' );
        },
        'c':        function () {
            this.scenic.sessionChannel.commands.execute( 'inspector:show', 'sip' );
        },
        'f':        function () {
            this.scenic.sessionChannel.commands.execute( 'inspector:show', 'files' );
        },
        'a':        function () {
            this.scenic.sessionChannel.commands.execute( 'quiddity:add', _.bind( this.scenic.quiddities.create, this.scenic.quiddities ) );
        },
        'ctrl+s': function (e) {
            e.preventDefault();
            this.scenic.sessionChannel.commands.execute( 'file:save' );
        },
        'ctrl+shift+s': function (e) {
            e.preventDefault();
            this.scenic.sessionChannel.commands.execute( 'file:saveAs' );
        },
        'ctrl+o': function (e) {
            e.preventDefault();
            this.scenic.sessionChannel.commands.execute( 'file:open' );
        },
        'ctrl+alt+n': function (e) {
            e.preventDefault();
            this.scenic.sessionChannel.commands.execute( 'file:new' );
        }
    },

    initialize( options ) {
        this.scenic = this.model;
        this.notifications = new NotificationsView( { scenic: this.scenic } );

        // Pages
        this.pages = new Pages( null, { scenic: this.scenic } );
        this.pages.add( new MatrixPage( null, { scenic: this.scenic, viewClass: MatrixView } ) );
        this.pages.add( new SwitchScenesPage( null, { scenic: this.scenic, viewClass: SwitchScenesView } ) );
        this.pages.add( new ControlPage( null, { scenic: this.scenic, viewClass: ControlView } ) );
        this.pages.add( new Page( {
            id:          "advanced",
            name:        i18n.t( 'Advanced' ),
            type:        "advanced",
            description: i18n.t( "Advanced quiddity management" )
        }, { scenic: this.scenic, viewClass: AdvancedView } ) );
        this.pages.add( new SettingsPage( null, { scenic: this.scenic, viewClass: SettingsView } ) );
        this.pages.add( new HelpPage( null, { scenic: this.scenic, viewClass: HelpView } ) );

        if ( this.scenic.saveProperties.get('quiddities').length > 0 ) {
            this.pages.add(new PropertiesPage(null, {scenic: this.scenic, viewClass: PropertiesView}));
        }

        this.pages.on( 'change:current', _.bind( this.showPage, this ) );

        // Inspectors
        this.inspectors = new Inspectors( null, { scenic: this.scenic } );
        this.inspectors.add( new Inspector( {
            id:   'info',
            name: i18n.t( 'Information' ),
            icon: "assets/images/icons/724-info.png",
            view: InformationInspectorView
        }, { scenic: this.scenic } ) );
        this.inspectors.add( new Inspector( {
            id:   'settings',
            name: i18n.t( 'Settings' ),
            icon: "assets/images/icons/740-gear.png",
            view: InformationInspectorView
        }, { scenic: this.scenic } ) );
        this.inspectors.add( new Inspector( {
            id:   'sip',
            name: i18n.t( 'SIP' ),
            icon: "assets/images/icons/779-users.png",
            view: SIPInspectorView
        }, { scenic: this.scenic } ) );
        this.inspectors.add( new Inspector( {
            id:   'files',
            name: i18n.t( 'Files' ),
            icon: "assets/images/icons/710-folder.png",
            view: FilesInspectorView
        }, { scenic: this.scenic } ) );

        this.inspectorMediator = new InspectorMediator( { inspectors: this.inspectors, scenic: this.scenic } );


        // Wreqr Handlers
        this.scenic.sessionChannel.commands.setHandler( 'set:language', this.setLanguage, this );
        this.scenic.sessionChannel.vent.on( 'inspector:opened', _.bind( this.showInspector, this ) );
        this.scenic.sessionChannel.vent.on( 'inspector:closed', _.bind( this.closeInspector, this ) );
        this.scenic.sessionChannel.vent.on( 'properties:show', _.bind( this.showProperties, this ) );
        this.scenic.sessionChannel.commands.setHandler( 'set:fullscreen', this.setFullscreen, this );

        // Notifications Handler
        this.notificationsHandler = new NotificationsHandler( this.scenic );
        this.commandsHandler      = new CommandsHandler( this.scenic );

        // Window title
        this.listenTo(this.scenic.saveFiles,'update', this.updateWindowTitle);
        this.listenTo(this.scenic.saveFiles,'change:current', this.updateWindowTitle);
        this.listenTo(this.scenic.saveProperties,'change:quiddities', this.removeSavePropertiesPage);

        this.updateWindowTitle();
    },

    /**
     * Render
     * Special case for the moment as we don't use a master application view to render us
     */
    onRender() {
        if ( this.scenic.get( 'connected' ) ) {
            if ( !this.scenic.get('fullscreen') ) {
                this.showChildView('tabs', new TabsView({
                    collection: this.pages,
                    scenic: this.scenic
                }));
                this.showChildView('usage', new SystemUsageView({
                    model: this.scenic.quiddities.get('systemUsage'),
                    scenic: this.scenic
                }));
                this.showChildView('lang', new LanguageView({
                    scenic: this.scenic
                }));
                this.showChildView('inspector', new InspectorView({
                    scenic: this.scenic,
                    inspectors: this.inspectors
                }));
                this.showChildView('side-menu', new SideMenuView({
                    scenic: this.scenic
                }));
                this.showChildView('preview', new PreviewView({
                    scenic: this.scenic
                }));
                if ( !this.scenic.config.disableChat ) {
                    this.showChildView('chats', new ChatsView({
                        model: this.scenic.chats,
                        scenic: this.scenic
                    }));
                }
                this.showChildView('tooltips', new TooltipsView({
                    scenic: this.scenic
                }));
            }
            this.pages.setCurrentPage( this.pages.getCurrentPage() );
        } else {
            this.$el.addClass( 'disconnected' );
            
            this.getRegion( 'page' ).empty();
            this.getRegion( 'tabs' ).empty();
            this.getRegion( 'usage' ).empty();
            this.getRegion( 'lang' ).empty();
            this.getRegion( 'inspector' ).empty();
            this.getRegion( 'side-menu' ).empty();
            this.getRegion( 'preview' ).empty();
            this.getRegion( 'chats' ).empty();
            this.getRegion( 'tooltips' ).empty();

            if ( this.scenic.get( 'shutdown' ) ) {
                this.showChildView( 'modal', new ShutdownView() );
            } else {
                this.showChildView( 'modal', new DisconnectedView() );
            }


        }
    },

    onBeforeDestroy(){
        this.notificationsHandler.removeMessageListener();
    },

    showTab( index ) {
        var enabledPages = this.pages.where( { enabled: true } );
        if ( enabledPages[index] ) {
            enabledPages[index].activate();
        }
    },

    /**
     * Current page change handler
     * Displays the current page
     *
     * @param page
     * @private
     */
    showPage( page ) {
        if ( this.currentPage ) {
            this.$el.removeClass( this.currentPage.id );
        }
        if ( page ) {
            this.showChildView( 'page', page.getViewInstance() );
            this.currentPage = page;
        } else {
            this.getRegion( 'page' ).empty();
            this.currentPage = null;
        }
        if ( this.currentPage ) {
            this.$el.addClass( this.currentPage.id );
        }
    },

    /**
     * Current page change handler
     * Displays the current page
     *
     * @param page
     * @private
     */
    showProperties( idQuiddity ) {
        this.scenic.saveProperties.add(idQuiddity);
        if ( this.scenic.saveProperties.get('quiddities').length == 1 ) {
            this.pages.add(new PropertiesPage(null, {scenic: this.scenic, viewClass: PropertiesView}));
        }
        this.pages.get('properties').activate( idQuiddity );
    },

    removeSavePropertiesPage( ) {
        if ( this.scenic.saveProperties.get('quiddities').length == 0 ) {
            this.pages.remove(this.pages.get('properties'));
            this.pages.setCurrentPage(this.pages.get(this.scenic.config.defaultPanelPage))
        }
    },

    setFullscreen () {
        if ( this.scenic.get( 'fullscreen' ) ) {
            this.scenic.set( 'fullscreen', false )
        } else {
            this.closeInspector();
            this.scenic.set( 'fullscreen', true )
        }
    },

    setLanguage( language ) {
        if ( !_.contains( this.scenic.config.locale.supported, language ) ) {
            console.warn( 'Invalid language', language );
        }
        var currentLanguage = localStorage.getItem( 'lang' );
        if ( currentLanguage != language ) {
            localStorage.setItem( 'lang', language );
            location.reload();
        }
    },

    showInspector() {
        this.$el.addClass( 'inspector-opened' ).removeClass( 'inspector-closed' );
    },

    closeInspector() {
        this.$el.removeClass( 'inspector-opened' ).addClass( 'inspector-closed' );
    },

    updateWindowTitle() {
        var currentFile = this.scenic.saveFiles.findWhere( { current: true } );
        var fileName = currentFile ? currentFile.get( 'name' ) : i18n.t( '*unsaved session' );
        document.title = i18n.t('__fileName__ | Scenic | Société des arts technologiques', { fileName });
    },

    //  ███████╗██╗  ██╗██╗   ██╗████████╗██████╗  ██████╗ ██╗    ██╗███╗   ██╗
    //  ██╔════╝██║  ██║██║   ██║╚══██╔══╝██╔══██╗██╔═══██╗██║    ██║████╗  ██║
    //  ███████╗███████║██║   ██║   ██║   ██║  ██║██║   ██║██║ █╗ ██║██╔██╗ ██║
    //  ╚════██║██╔══██║██║   ██║   ██║   ██║  ██║██║   ██║██║███╗██║██║╚██╗██║
    //  ███████║██║  ██║╚██████╔╝   ██║   ██████╔╝╚██████╔╝╚███╔███╔╝██║ ╚████║
    //  ╚══════╝╚═╝  ╚═╝ ╚═════╝    ╚═╝   ╚═════╝  ╚═════╝  ╚══╝╚══╝ ╚═╝  ╚═══╝

    /**
     * Connected handler
     *
     * @private
     */
    _onConnectedChanged() {
        if ( !this.model.get( 'connected' ) ) {
            if ( this.stopSpinner ) {
                this.stopSpinner();
            }
            this.showPage( null );
        }

        this.render();
    }
} );