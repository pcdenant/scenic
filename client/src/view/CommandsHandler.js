"use strict";

import _ from 'underscore';
import i18n from 'i18n';

import FilesView from 'view/scenic/inspector/files/Files';
import SaveAsView from 'view/scenic/inspector/files/file/SaveAs';

export default class CommandsHandler {

    constructor( scenic ) {
        this.scenic = scenic;

        this.scenic.sessionChannel.commands.setHandler( 'file:new', this.newDocument, this );
        this.scenic.sessionChannel.commands.setHandler( 'file:open', this.showFileList, this );
        this.scenic.sessionChannel.commands.setHandler( 'file:save', this.saveFile, this );
        this.scenic.sessionChannel.commands.setHandler( 'file:saveAs', this.saveFileAs, this );
    }

    newDocument() {
        var self = this;
        this.scenic.scenicChannel.commands.execute( 'confirm', i18n.t( 'Are you sure you want to reset the current document? You will lose changes to the current session if you continue.' ), function ( confirmed ) {
            if ( confirmed ) {
                self.scenic.reset();
            }
        } );
    }

    showFileList() {
        this.scenic.sessionChannel.commands.execute( 'inspector:toggle', 'files', {
            view:    FilesView,
            options: { collection: this.scenic.saveFiles, scenic: this.scenic }
        }, false );
    }

    saveFile() {
        var file = this.scenic.saveFiles.findWhere( { current: true } );
        if ( file ) {
            file.save();
        } else {
            this.saveFileAs();
        }
    }

    saveFileAs() {
        this.scenic.sessionChannel.commands.execute( 'inspector:show', 'files', {
            view:    SaveAsView,
            options: {
                close:  () => this.scenic.sessionChannel.commands.execute( 'inspector:close' ),
                scenic: this.scenic
            }
        }, false );
    }

}