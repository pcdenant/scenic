"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'view/application/SessionView'
], function ( _, Backbone, Marionette, SessionView ) {

    /**
     * Sessions View
     * 
     * @constructor
     * @extends Marionette.CollectionView
     * @exports client/application/SessionsView
     */
    var SessionsView = Marionette.CollectionView.extend( {
        childView: SessionView,

        /**
         * Initialize
         */
        initialize: function () {

        }
    } );

    return SessionsView;
} );
