define( [
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    './ShmdataPreview.html'
], function ( _, Backbone, Marionette, i18n, ShmdataPreviewTemplate ) {

    /**
     * Shmdata Preview Panel
     *
     * @constructor
     * @extends module:Marionette.ItemView
     */
    var ShmdataPreview = Marionette.ItemView.extend( {
        template:   ShmdataPreviewTemplate ,
        className: 'shmdata-preview',

        modelEvents: {
            'change:preview': 'updatePreview',
            'destroy': 'destroyPreview'
        },

        events: {
            'keydown':       'checkForEscapeKey'
        },

        ui: {
            preview:      '.preview',
            image:        '.image',
            previewImage: '.image img'
        },

        templateHelpers: function () {

        },

        initialize: function ( options ) {
            this.scenic = options.scenic;

            this.title  = i18n.t( 'Shmdata preview for __shmdata__', { shmdata: this.model.get( 'shortName' ) } );

            var imgInfo = this.getImgInfo();
            options.initSize( imgInfo.imageWidth, imgInfo.imageHeight );
        },

        onBeforeShow: function () {
            this.model.subscribeToPreview();
        },

        onBeforeDestroy: function () {
            this.model.unsubscribeFromPreview();
        },

        onResize: function ( width, height ) {
            var windowRatio = width / height;
            var imgInfo     = this.getImgInfo();

            if ( windowRatio >= imgInfo.imageRatio ) {
                this.ui.previewImage.css( 'width', '' );
                this.ui.previewImage.css( 'height', imgInfo.imageHeight >= height ? height : '' );
            } else if ( windowRatio < imgInfo.imageRatio ) {
                this.ui.previewImage.css( 'width', imgInfo.imageWidth >= width ? width : '' );
                this.ui.previewImage.css( 'height', '' );
            }
        },

        getImgInfo: function () {
            var caps      = this.model.get( 'capabilities' );
            var shmWidth  = parseInt( caps.width );
            var shmHeight = parseInt( caps.height );
            var info      = {
                imageRatio:  shmWidth / shmHeight,
                imageWidth:  this.scenic.config.timelapse.preview.width,
                imageHeight: this.scenic.config.timelapse.preview.height
            };
            if ( info.imageWidth && !info.imageHeight ) {
                info.imageHeight = info.imageWidth / info.imageRatio;
            } else if ( !imageWidth && imageHeight ) {
                info.imageWidth = info.imageHeight * info.imageRatio;
            } else if ( !info.imageWidth && !info.imageHeight ) {
                info.imageWidth  = shmWidth;
                info.imageHeight = shmHeight;
            }
            return info;
        },

        checkForEscapeKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 27 ) {
                event.preventDefault();
                this.scenic.sessionChannel.commands.execute( 'inspector:close' );
            }
        },

        updatePreview: function () {
            this.ui.previewImage.attr( 'src', this.model.get( 'preview' ) );
        },

        destroyPreview: function() {
            this.scenic.sessionChannel.commands.execute( 'preview:close' );
        }
    } );

    return ShmdataPreview;
} );