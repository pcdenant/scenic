'use strict';

import _ from 'underscore';
import i18n from 'i18n';
import Backbone from 'backbone';
import Marionette from 'marionette';
import SideMenuItemView from 'view/scenic/sideMenu/SideMenuItem';

/**
 * Side Menu View
 *
 * @constructor
 * @extends module:Marionette.LayoutView
 */
export default Marionette.CollectionView.extend( {
    tagName:          'ul',
    className:        'tabs',
    childView:        SideMenuItemView,
    childViewOptions: function () {
        return {
            scenic: this.options.scenic
        }
    },
    initialize() {
        this.collection = new Backbone.Collection( [
            {
                id:         'info',
                name:       i18n.t( 'Information' ),
                type:       'inspector',
                active:     false,
                icon:       'assets/images/icons/724-info.png',
                activeIcon: 'assets/images/icons/724-info-selected.png',
                command:    { name: 'inspector:toggle', params: { panel: 'info' } }
            },
            {
                id:         'settings',
                name:       i18n.t( 'Settings' ),
                type:       'inspector',
                active:     false,
                icon:       'assets/images/icons/740-gear.png',
                activeIcon: 'assets/images/icons/740-gear-selected.png',
                command:    { name: 'inspector:toggle', params: { panel: 'settings' } }
            },
            {
                id:         'sip',
                name:       i18n.t( 'SIP' ),
                type:       'inspector',
                active:     false,
                icon:       'assets/images/icons/779-users.png',
                activeIcon: 'assets/images/icons/779-users-selected.png',
                command:    { name: 'inspector:toggle', params: { panel: 'sip' } }
            },
            {
                id:      'spacer',
                command: { name: 'inspector:close' }
            },
            {
                id:         'new',
                name:       i18n.t( 'New' ),
                type:       'command',
                active:     false,
                icon:       'assets/images/icons/738-document-1.png',
                activeIcon: 'assets/images/icons/738-document-1-selected.png',
                command:    { name: 'file:new' }
            },
            {
                id:         'open',
                name:       i18n.t( 'Open' ),
                type:       'command',
                active:     false,
                icon:       'assets/images/icons/710-folder.png',
                activeIcon: 'assets/images/icons/710-folder-selected.png',
                command:    { name: 'file:open' }
            },
            {
                id:         'save',
                name:       i18n.t( 'save' ),
                type:       'command',
                active:     false,
                icon:       'assets/images/icons/785-floppy-disk.png',
                activeIcon: 'assets/images/icons/785-floppy-disk-selected.png',
                command:    { name: 'file:save' }
            },
            {
                id:         'saveAs',
                name:       i18n.t( 'Save As' ),
                type:       'command',
                active:     false,
                icon:       'assets/images/icons/save-as.png',
                activeIcon: 'assets/images/icons/save-as.png',
                command:    { name: 'file:saveAs' }
            }
        ] );
    }
} );