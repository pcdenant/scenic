"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'jquery',
    'view/scenic/inspector/sip/contacts/SelfView',
    'view/scenic/inspector/sip/contacts/ControlView',
    'view/scenic/inspector/sip/contacts/ContactAuthorizedListView',
    'view/scenic/inspector/sip/contacts/ContactUnauthorizedListView',
    'view/scenic/inspector/sip/contacts/ContactGroupListView',
    './ContactsView.html',
    'jquery-ui/ui/widgets/sortable'
], function ( _, Backbone, Marionette, $, SelfView, ControlView, ContactAuthorizedListView, ContactUnauthorizedListView, ContactGroupListView, ContactsTemplate ) {

    /**
     * SIP Contacts View
     *
     * @constructor
     * @extends module:Marionette.LayoutView
     */
    var ContactsView = Marionette.LayoutView.extend( {
        template:  ContactsTemplate ,

        ui: {
            logout:             '.action.logout',
            addContact:         '.action.add',
            contacts:           '.contacts',
            authorizedlist:     '.authorizedlist',
            unauthorizedlist:   '.unauthorizedlist',
            groupMessage:       '.action.group',
            createGroup:        '.action.create',
            closeGroupMessage:  '.action.close'
        },

        regions: {
            self:               '.self',
            contacts:           '.contacts',
            authorizedlist:     '.authorized-contacts',
            unauthorizedlist:   '.unauthorized-contacts',
            groupContacts:      '.group-contacts'
        },

        events: {
            'click @ui.logout':             'logout',
            'click @ui.addContact':         'addContact',
            'click @ui.groupMessage':       'addGroup',
            'click @ui.createGroup':        'createGroup',
            'click @ui.closeGroupMessage':  'closeGroupMessage'
        },

        modelEvents: {
            'change:groupDiscussion':  'render'
        },

        templateHelpers: function () {
            return {
                groupDiscussion : this.model.get('groupDiscussion')
            }
        },
        /**
         * Initialize
         */
        initialize: function ( options ) {
            Marionette.LayoutView.prototype.initialize.apply( this, arguments );
            
            this.scenic = options.scenic;
            this.listenTo( this.model.contacts, 'update', this.showSelf );
        },

        onBeforeShow: function () {
            this.showContacts();
        },

        onRender: function () {
            this.showContacts();
        },

        showContacts: function() {
            if ( this.model.get('groupDiscussion') == false ) {
                this.showChildView('authorizedlist', new ContactAuthorizedListView({
                    table: this.model,
                    scenic: this.scenic,
                    collection: this.model.contacts
                }));

                this.showChildView('unauthorizedlist', new ContactUnauthorizedListView({
                    table: this.model,
                    collection: this.model.contacts
                }));
            } else if ( !this.scenic.config.disableChat ) {
                this.showChildView('groupContacts', new ContactGroupListView({
                    table: this.model,
                    collection: this.model.contacts
                }));
            }
            this.showSelf();
        },

        showSelf: function () {
            if ( !this.model.contacts ) {
                console.log( 'THIS SHOULD NOT APPEAR ONCE THE SIP PANEL HAS BEEN MOVED CORRECTLY' );
                return;
            }
            var self = this.model.contacts.findWhere( { self: true } );
            
            if ( self ) {
               this.showChildView( 'self', new SelfView( { model: self } ) );
            } else {
                this.getRegion( 'self' ).empty();
            }
        },

        logout: function () {
            this.model.logout();
        },

        addContact: function (e) {
            this.scenic.sessionChannel.commands.execute(
                'contact:add',
                _.bind( this.model.addContact, this.model )
            );
        },

        addGroup: function () {
            this.model.set('groupDiscussion', true );
        },

        createGroup: function () {
            var contactsChat = this.model.contacts.filter( contact => { return  contact.get('selected') ==  true } );
            this.scenic.sessionChannel.commands.execute( 'open:group:chat', contactsChat );
            this.model.set('groupDiscussion', false );
        },

        closeGroupMessage: function () {
            this.model.set('groupDiscussion', false );
        }
    } );

    return ContactsView;
} );
