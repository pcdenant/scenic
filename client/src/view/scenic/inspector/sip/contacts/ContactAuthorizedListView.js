"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'jquery',
    'view/scenic/inspector/sip/contacts/ContactView',
    'jquery-ui/ui/widgets/sortable'
], function ( _, Backbone, Marionette, $, ContactView ) {

    /**
     * Contact Authorized List View
     *
     * @constructor
     * @extends module:Marionette.CollectionVIew
     */
    var ContactAuthorizedListView = Marionette.CollectionView.extend( {
        className:        'authorizedList contact-list',
        id:               "authorizedList",
        childView:        ContactView,
        childViewOptions: function () {
            return {
                table: this.options.table,
                scenic: this.options.scenic
            }
        },
        /**
         * Initialize
         */
        initialize: function () {
            this.listenTo( this.options.collection, 'change', this.render );
        },
        onShow: function() {
            this.$el.sortable( {
                axis:                 'y',
                cursor:               "move",
                connectWith:          '.unAuthorizedList',
                receive: ( event, ui ) => {
                    var contact = this.options.collection.findWhere( { id: ui.item.attr('id') });
                    contact.authorize( true ); 
                }
            } ).disableSelection();
        },
        /**
         * Contacts View Filter
         *
         * @param contact
         * @returns {boolean}
         */
        filter: function ( contact ) {
            return contact.get( 'whitelisted' ) && !contact.get( 'self' );
        }
    } );

    return ContactAuthorizedListView;
} );
