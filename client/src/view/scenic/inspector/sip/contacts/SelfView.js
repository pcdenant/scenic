"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    './SelfView.html',
    './self/Status.html'
], function ( $, _, Backbone, Marionette, SelfTemplate, StatusTemplate ) {

    /**
     * SIP Self View
     *
     * @constructor
     * @extends module:Marionette.ItemVIew
     */
    var SelfView = Marionette.ItemView.extend( {

        /**
         * Get Template Method
         * Dynamically chooses which template to render
         * As the "self" user can be null we don't want to render a template in that case
         *
         * @returns {*}
         */
        getTemplate: function () {
            if ( this.model ) {
                return  SelfTemplate ;
            } else {
                return null;
            }
        },

        className: 'contact self',

        ui: {
            'alias':            '#alias',
            'checkboxBlock':    '#checkboxBlock',
            'selfStatus':       '.contact-status',
            'status':           '.status',
            'statusText':       '.contact-status-text'
        },

        events: {
            'keypress @ui.alias':      'checkForEnterKey',
            'blur @ui.alias':          'update',
            'keypress @ui.statusText': 'checkForEnterKey',
            'blur @ui.statusText':     'update',
            'click @ui.selfStatus':    'showStatusList',
            'click @ui.status':        'changeStatus',
            'change @ui.checkboxBlock': 'updateModel'
        },

        modelEvents: {
            'change': 'render'
        },

        templateHelpers: function () {
            return {
                statuses: this.model.scenic.sip.quiddity.properties.get('status' ).get('options'),
                notBother: this.model.get('notBother'),
                chatConnected: this.model.get('chatConnected')
            }
        },

        attributes: function () {
            return {
                class: ['contact self', this.model.get( 'status' ).toLowerCase(), this.model.get( 'subscription_state' ).toLowerCase()].join( ' ' ),
                id: this.model.get( 'id' )
            }
        },

        /**
         * Initialize
         */
        initialize: function () {
            this.model.isNotBotherMode();
        },

        onRender: function () {
            // Update Dynamic Attributes
            this.$el.attr( this.attributes() );
        },

        checkForEnterKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 13 ) {
                event.preventDefault();
                this.update();
            }
        },

        update: function () {
            var self = this;

            this.model.save( { name: this.ui.alias.val(), status_text: this.ui.statusText.val() }, {
                error: function ( error ) {
                    self.model.scenic.sessionChannel.vent.trigger( 'error', error );
                }
            } );
        },

        showStatusList: function ( event ) {
            event.preventDefault();
            event.stopImmediatePropagation();
            this.$el.append(  StatusTemplate( { statuses: this.model.collection.sip.quiddity.properties.get( 'status' ).get( 'values' ) } ) );
            $( 'body' ).on( 'click', _.bind( this.closeStatusList, this ) );
        },

        changeStatus: function ( event ) {
            var self   = this;
            var status = $( event.currentTarget ).data( 'index' ).toString();
            this.model.save( { status: status }, {
                error: function ( error ) {
                    self.model.scenic.sessionChannel.vent.trigger( 'error', error );
                }
            } );
            this.closeStatusList();
        },

        closeStatusList: function () {
            $( 'body' ).off( 'click', _.bind( this.closeStatusList, this ) );
            $( '.status-list', this.$el ).remove();
        },

        openStatusList: function () {
          console.log('openStatusList');
        },
        /**
         * Update Model Value
         *
         * @param event
         */
        updateModel: function ( event ) {
            // We have a custom checkbox, this is needed to toggle and set the actual value
            var checked = this.ui.checkboxBlock.is( ':checked' );
            this._updateChecked( checked );

            // Update the model
            if ( checked ) {
                this.model.blockAllContact();
            } else {
                this.model.unblockAllContact();
            }
            this.model.isNotBotherMode();
        },

        _updateChecked( checked ) {
            this.ui.checkboxBlock.val( checked ).attr( 'checked', checked );
            this.ui.checkboxBlock.addClass( checked ? 'checked' : 'unchecked' ).removeClass( checked ? 'unchecked' : 'checked' );
        }

    } );

    return SelfView;
} )
;
