"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    './ContactView.html'
], function ( _, Backbone, Marionette, ContactTemplate ) {

    /**
     * Contact View
     *
     * @constructor
     * @extends module:Marionette.CompositeView
     */
    var ContactView = Marionette.ItemView.extend( {
        template:   ContactTemplate ,
        className: 'contact',

        ui: {
            add:  '.add-destination',
            talk:  '.talk-destination',
            edit: '.edit-contact',
            info: '.contact-info'
        },

        events: {
            'click @ui.add':        'addContactAsDestination',
            'click @ui.talk':       'openChat',
            'click @ui.edit':       'editContact',
            'mousedown @ui.info':   'dragStart',
            'mouseup @ui.info':     'dragStop'
        },

        attributes: function () {
            return {
                class: ['contact', this.model.get( 'status' ).toLowerCase(), this.model.get( 'subscription_state' ).toLowerCase()].join( ' ' ),
                id: this.model.get( 'id' )
            }
        },

        templateHelpers: function () {
            return {
                connected: this.model.get( 'chatConnected' )
            };
        },

        modelEvents: {
            'change': 'render'
        },

        /**
         * Initialize
         */
        initialize: function ( options ) {
            // Keep options internally
            this.table = options.table;
            this.scenic = options.scenic;
        },

        onRender: function () {
            // Update Dynamic Attributes
            this.$el.attr( this.attributes() );
        },

        /**
         * Disable the display of the edit and addContact buttons when drag the contact
         */
        dragStart: function () {
            this.$el.addClass('no-hover');
        },

        dragStop: function () {
            this.$el.removeClass('no-hover');
        },

        addContactAsDestination: function () {
            if ( this.model.get( 'subscription_state' ) == "ACTIVE" ) {
                this.model.addAsDestination();
            }
        },

        openChat: function () {
            if ( this.model.get('chatConnected') ) {
                this.scenic.sessionChannel.commands.execute('open:chat', this.model);
            }
        },

        editContact: function () {
            this.model.edit();
        }
    } );

    return ContactView;
} );
