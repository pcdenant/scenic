"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'lib/spin',
    'i18n',
    './LoginView.html',
    './login/Connecting.html',
    './login/LoginAdvanced.html'
], function ( $, _, Backbone, Marionette, spin, i18n, LoginTemplate, ConnectingTemplate, AdvancedTemplate ) {

    /**
     * SIP Login View
     *
     * @constructor
     * @extends module:Marionette.ItemVIew
     */
    var LoginView = Marionette.ItemView.extend( {
        className: 'login',

        ui: {
            server:        '#sipServer',
            user:          '#sipUsername',
            password:      '#sipPassword',
            port:          '#sipPort',
            login:         '#sipLogin',
            advanced:      '#sipAdvanced',
            back:          '#sipSimpleLogin',
            stunServer:    '#stunServer',
            turnServer:    '#turnServer',
            turnUser:      '#turnUser',
            turnPassword:  '#turnPassword',
            checkboxTurn:  '#checkboxTurn',
            fieldTurnUser: '#fieldTurnUser',
            fieldTurnPass: '#fieldTurnPass',
            turnLogin:     '#turnLogin'
        },

        events: {
            'click @ui.login':           'login',
            'click @ui.advanced':        'showAdvanced',
            'click @ui.back':            'showSimpleLogin',
            'keypress @ui.server':       'checkForEnterKey',
            'keyup @ui.user':            'checkForEnterKey',
            'keyup @ui.password':        'checkForEnterKey',
            'keypress @ui.port':         'checkForEnterKey',
            'keypress @ui.stunServer':   'checkForEnterKey',
            'keypress @ui.turnServer':   'checkForEnterKey',
            'keypress @ui.turnUser':     'checkForEnterKey',
            'keypress @ui.turnPassword': 'checkForEnterKey',
            'change @ui.checkboxTurn':   'updateSameLogin'
        },

        modelEvents: {
            'change:sameLogin': 'render'
        },

        getTemplate: function () {
            if ( this.model.get( 'connecting' ) ) {
                return ConnectingTemplate;
            } else if ( this.advanced ) {
                return AdvancedTemplate;
            } else {
                return LoginTemplate;
            }
        },

        templateHelpers: function () {
            return {
                error:          this.error,
                tmpPassword:    this.tmpPassword,
                showTurnOption: this.model.get( 'stunServer' ) && this.model.get( 'turnServer' )
            };
        },

        /**
         * Initialize
         */
        initialize: function () {
            this.firstRender = true;
        },

        onRender: function () {
            $( '*', this.$el ).prop( 'disabled', false );
            if ( !this.firstRender ) {
                this.$el.fadeTo( 250, 1.0 );
            }
            this.firstRender = false;
        },

        checkForEnterKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 13 ) {
                event.preventDefault();
                this.login();
            }
        },

        /**
         * Set the value of the sameLogin property of the SIP connection from the checkbox
         */
        updateSameLogin() {

            // We need to save and update the value of STUN and TURN server inputs when the value of the same Login checkbox change 
            const saveStunServer = this.ui.stunServer.val();
            const saveTurnServer = this.ui.turnServer.val();

            this.model.set('sameLogin', this.ui.checkboxTurn.is( ':checked' ) );

            this.ui.stunServer.val(saveStunServer);
            this.ui.turnServer.val(saveTurnServer);    
        },

        /*_updateChecked() {
            const sameLogin = this.model.get('sameLogin');
            this.ui.checkboxTurn.val( sameLogin ).attr( 'checked', sameLogin );
            this.ui.checkboxTurn.addClass( sameLogin ? 'checked' : 'unchecked' ).removeClass( sameLogin ? 'unchecked' : 'checked' );
        },*/

        /**
         * Login
         */
        login: function () {
            var self = this;
            $( '*', this.$el ).prop( 'disabled', true );
            this.$el.fadeTo( 250, 0.5, function () {
                if ( !self.isDestroyed ) {
                    self.render();
                }
            } );

            // TODO: Validation
            this.model.login(
                this.ui.server.val(),
                this.ui.port.val(),
                this.ui.user.val(),
                this.ui.password.val(),
                this.ui.stunServer.val(),
                this.ui.turnServer.val(),
                this.ui.turnUser.val(),
                this.ui.turnPassword.val(),
                ( error, success ) => {
                    if ( error ) {
                        this.error = error;
                        this.render();
                    } else if ( !success ) {
                        this.render();
                    }
                } );
        },

        /**
         * Show advanced login
         */
        showAdvanced: function () {
            var self = this;

            this.tmpPassword =  this.ui.password.val() ? this.ui.password.val() : null;

            $( '*', this.$el ).prop( 'disabled', true );
            this.$el.fadeTo( 250, 0.5, function () {
                if ( !self.isDestroyed ) {
                    self.render();
                }
            } );
            this.advanced = true;
        },

        /**
         * Show advanced login
         */
        showSimpleLogin: function () {
            var self = this;

            this.tmpPassword =  this.ui.password.val() ? this.ui.password.val() : null;

            $( '*', this.$el ).prop( 'disabled', true );
            this.$el.fadeTo( 250, 0.5, function () {
                if ( !self.isDestroyed ) {
                    self.render();
                }
            } );
            this.advanced = false;
        }
    } );

    return LoginView;
} )
;
