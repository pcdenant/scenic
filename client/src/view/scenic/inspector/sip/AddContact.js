"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    './AddContact.html'
], function ( _, Backbone, Marionette, i18n, AddContactTemplate ) {

    /**
     * Add Contact Form
     *
     * @constructor
     * @extends module:Marionette.ItemView`
     */
    var AddContactView = Marionette.ItemView.extend( {
        template:  AddContactTemplate,
        className: 'add-contact dialog',

        ui: {
            'uri':    '.uri',
            'add':    '#addContact',
            'cancel': '#cancel'
        },

        events: {
            'click @ui.add':    'add',
            'click @ui.cancel': 'cancel',
            'keydown':          'checkForEscapeKey',
            'keyup':            'checkForURI',
            'keypress @ui.uri': 'checkForEnterKey'
        },

        initialize: function ( options ) {
            this.scenic   = options.scenic;
            this.title    = i18n.t( 'Add Contact' );
            this.callback = options.callback;
        },

        templateHelpers: function () {
            return {
                error: this.error,
                uri:   this.uri
            };
        },

        onRender: function () {
            _.defer( _.bind( this.ui.uri.focus, this.ui.uri ) );
        },

        checkForEscapeKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 27 ) {
                event.preventDefault();
                this.scenic.sessionChannel.commands.execute( 'inspector:show', 'sip' );
            }
        },

        checkForEnterKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 13 ) {
                event.preventDefault();
                this.add();
            }
        },

        add: function () {
            if ( !_.isEmpty( this.ui.uri.val() ) ) {
                this.uri = this.ui.uri.val().toLowerCase();

                if ( this.ui.uri.val() === this.scenic.sip.get( 'uri' ) ) {
                    this.error = i18n.t( '__uri__ already added', { uri: this.ui.uri.val() } );
                    this.render();

                } else {
                    this.callback( this.ui.uri.val().toLowerCase() );
                    this.scenic.sessionChannel.commands.execute( 'inspector:show', 'sip' );
                }
            }
        },

        cancel: function () {
            this.scenic.sessionChannel.commands.execute( 'inspector:show', 'sip' );
        }

    } );
    return AddContactView;
} );