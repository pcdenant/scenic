define( [
    'underscore',
    'backbone',
    'marionette',
    'view/scenic/inspector/files/Files',
    'view/scenic/inspector/InspectorPanelView'
], function ( _, Backbone, Marionette, FilesView, InspectorPanelView ) {

    /**
     *  @constructor
     *  @augments module:InspectorPanelView
     */
    var FilesInspector = InspectorPanelView.extend( {
        className:  'sip',
        initialize: function () {
            InspectorPanelView.prototype.initialize.apply( this, arguments );
            this.defaultView = FilesView;
            this.defaultOptions = {
                scenic:     this.scenic,
                collection: this.scenic.saveFiles
            };
        }
    } );
    return FilesInspector;
} );