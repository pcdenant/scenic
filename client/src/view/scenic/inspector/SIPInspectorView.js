define( [
    'underscore',
    'backbone',
    'marionette',
    'view/scenic/inspector/sip/LoginView',
    'view/scenic/inspector/sip/ContactsView',
    'view/scenic/inspector/InspectorPanelView'
], function ( _, Backbone, Marionette, SIPLoginView, SIPContactsView, InspectorPanelView ) {

    /**
     *  @constructor
     *  @augments module:InspectorPanelView
     */
    var SIPInspector = InspectorPanelView.extend( {
        className:  'sip',
        initialize: function () {
            InspectorPanelView.prototype.initialize.apply( this, arguments );
            this.listenTo( this.scenic.sip, 'change:connected', this.selectView );
        },

        onShow: function () {
            this.selectView();
        },

        selectView: function () {
            var options = this.model.get('currentOptions');
            if ( !options ) {
                if ( this.scenic.sip.get( 'connected' ) ) {
                    this.showChildView( 'content', new SIPContactsView( {
                        scenic: this.scenic,
                        model:  this.scenic.sip
                    } ) );
                    this.ui.title.html( '');
                } else {
                    this.showChildView( 'content', new SIPLoginView( {
                        scenic: this.scenic,
                        model:  this.scenic.sip
                    } ) );
                    this.ui.title.html( 'SIP Login');
                }
            }
        }
    } );
    return SIPInspector;
} );