define( [
    'underscore',
    'backbone',
    'marionette',
    './InspectorPanelView.html'
], function ( _, Backbone, Marionette,
              InspectorTemplate ) {

    /**
     *  @constructor
     *  @augments module:Marionette.ItemView
     */
    var InspectorPanelView = Marionette.LayoutView.extend( {
        tagName:   'div',
        className: 'panel',
        template:   InspectorTemplate ,
        ui: {
            title: '.title'
        },
        regions: {
            content: '.content'
        },

        attributes: function () {
            return {
                class: ['panel', this.model.get( 'id' )].join(' ')
            }
        },

        modelEvents: {
            'change:currentOptions': 'showPanel'
        },
        
        initialize: function ( options ) {
            Marionette.LayoutView.prototype.initialize.apply(this,arguments);
            this.scenic = options.scenic;
        },

        onRender: function () {
            // Update Dynamic Attributes
            this.$el.attr( this.attributes() );
        },

        onBeforeShow: function() {
            this.showPanel();
        },

        showPanel: function ( ) {
            var options = this.model.get('currentOptions') || this.model.get('lastOptions') || { view: this.defaultView, options: this.defaultOptions };
            if ( options && options.view ) {
                var panelView = new options.view( options.options );
                this.showChildView( 'content', panelView );
                if ( this.ui.title && panelView.title ) {
                    this.ui.title.html( panelView.title );
                }
            }
        }
    } );
    
    return InspectorPanelView;
} );