"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    'model/SaveFile',
    './SaveAs.html'
], function ( _, Backbone, Marionette, i18n, SaveFile, SaveAsTemplate ) {

    /**
     * Save As
     *
     * @constructor
     * @extends module:Marionette.ItemView
     */

    var SaveAsView = Marionette.ItemView.extend( {
        template:   SaveAsTemplate ,
        className: 'file-save-as',

        ui: {
            'name':   '.name',
            'save':   '.confirm',
            'cancel': '.cancel'
        },

        events: {
            'keydown':          'checkKey',
            'click @ui.save':   'saveFile',
            'click @ui.cancel': 'cancel'
        },

        /**
         * Initialize
         */
        initialize: function ( options ) {
            this.scenic = options.scenic;
            this.close  = options.close;
            //this.model = new SaveFile(null, {scenic: this.scenic});
            this.title     = i18n.t( 'Save file as' );
        },

        onAttach: function () {
            _.defer( _.bind( this.ui.name.focus, this.ui.name ) );
        },

        saveFile: function () {
            var self = this;
            var name = this.ui.name.val();
            var file = this.scenic.saveFiles.get( name );
            if ( file ) {
                this.scenic.scenicChannel.commands.execute(
                    'confirm',
                    i18n.t( 'Are you sure you want to replace the file __file__?', { file: name } ),
                    function ( confirmed ) {
                        if ( confirmed ) {
                            file.save( null, {
                                success: function () {
                                    self.close();
                                }
                            } );
                        }
                    } );
            } else {
                this.scenic.saveFiles.create( { name: name }, {
                    scenic:  this.scenic,
                    wait:    true,
                    success: function () {
                        self.close();
                    }
                } );
            }
        },

        checkKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 27 ) {
                event.preventDefault();
                this.close();
            } else if ( key == 13 ) {
                event.preventDefault();
                this.saveFile();
            }
        },

        cancel: function () {
            this.close();
        }
    } );

    return SaveAsView;
} );
