"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    './File.html',
    'browser-filesaver',
], function ( _, Backbone, Marionette, i18n, FileTemplate, filesaver ) {

    /**
     * File
     *
     * @constructor
     * @extends module:Marionette.ItemView
     */

    var FileView = Marionette.ItemView.extend( {
        template: FileTemplate,
        tagName: 'li',
        className: 'file',

        ui: {
            'name':         '.name',
            'filename':     '.file-info',
            'rename':       '.rename',
            'inputName':    '.inputName',
            'remove':       '.remove',
            'export':       '.export',
        },

        events:    {
            'click @ui.filename':       'loadFile',
            'click @ui.rename':         'displayInput',
            'keypress @ui.inputName':   'checkForKey',
            'click @ui.remove':         'removeFile',
            'keydown':                  'checkForKey',
            'click @ui.export':         'exportFile'
        },

        modelEvents: {
            'change:name':      'render',
            'change:editName':  'render'
        },

        templateHelpers: function () {
            return {
                editName: this.model.get('editName')
            };
        },
        /**
         * Initialize
         */
        initialize: function (options) {
            this.scenic = options.scenic;
        },

        checkForKey: function ( event ) {
            var key = event.which || event.keyCode;
            // escape on the input, render the name of the file
            if ( key == 27 && this.model.get('editName') ){
                event.preventDefault();
                this.inputEscape();
            }
            if ( key == 13 && this.model.get('editName') ) {
                event.preventDefault();
                this.inputEnter();
            }
        },

        inputEscape: function ( ) {
            this.model.set('editName', false );
        },

        inputEnter: function ( ) {
            this.renameFile(this.ui.inputName.val());
            this.model.set('editName', false );     
        },

        loadFile: function() {
            if ( !this.model.get('editName') ) {
                this.triggerMethod('closeList', this.model);
                this.model.loadFile();
            }
        },

        displayInput: function( event ) {
            this.model.set('editName', true );
            this.ui.inputName.focus();
            this.ui.inputName.select();
        },

        renameFile: function( name ) {
            this.model.renameFile(name);
        },

        exportFile: function() {
            this.model.getFile( data => {
                var json = JSON.stringify( data );
                var blob = new Blob([json], { type: "application/json" });
                filesaver.saveAs(blob, this.model.get( 'name' ) + '.json' );
            });
        },

        removeFile: function(event) {
            event.stopImmediatePropagation();
            var self = this;
            this.scenic.scenicChannel.commands.execute( 'confirm', i18n.t('Are you sure you want to delete __file__?', {file:this.model.get('name')}), function( confirmed ) {
                if ( confirmed ) {
                    self.model.destroy();
                }
            });
        }
    } );

    return FileView;
} );
