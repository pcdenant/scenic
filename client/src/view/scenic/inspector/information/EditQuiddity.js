define( [
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    'view/scenic/inspector/information/edit/Tree',
    'view/scenic/inspector/information/edit/Properties',
    'view/scenic/inspector/information/edit/Methods',
    './EditQuiddity.html',
    'view/scenic/inspector/information/edit/property/Bool'
], function ( _, Backbone, Marionette, i18n, TreeView, PropertiesView, MethodsView, EditQuiddityTemplate, BoolView ) {

    /**
     * Edit Quiddity Form
     *
     * @constructor
     * @extends module:Marionette.LayoutView
     */
    var EditQuiddity = Marionette.LayoutView.extend( {
        template:   EditQuiddityTemplate ,
        className: 'edit-quiddity',

        ui:{
            'edit':        '.edit',
            'inputName':   '.inputName',
            'rename':      '.rename-title',
            'expand':      '.expand'
        },        
        regions: {
            tree:           '.tree-container',
            properties:     '.properties-container',
            methods:        '.methods-container',
            inputStarted:   '.classInfo .field'
        },

        modelEvents: {
            'destroy': '_onQuiddityRemoved',
            'change:name':  'render',
            'change:editName':  'focusInputName'
        },

        events: {
            'keypress':                 'checkForEscapeKey',
            'click @ui.rename':         'displayInput',
            'click @ui.expand':         'displayPropertiesPage',
            'keypress @ui.inputName':   'checkForKey'
        },


        templateHelpers: function () {
            this.started = this.model.properties.get('started');

            return {
                className: this.scenic.classes.get( this.model.get( 'class' ) ).get( 'name' ),
                classCategory: this.scenic.classes.get( this.model.get( 'class' ) ).get( 'category' ),
                classDescription: this.scenic.classes.get( this.model.get( 'class' ) ).get( 'description' ),
                id: this.started ? this.started.get( 'id' ) : null,
                label: this.started ? this.started.get( 'label' ) : null,
                description: this.started ? this.started.get( 'description' ) : null,
                advanced: this.options.advanced,
                editName: this.model.get('editName'),
                expand: this.scenic.config.propertiesPage
            }
        },

        initialize: function ( options ) {
            this.scenic    = options.scenic;
            this.inspector = options.inspector;
            this.model.subscribe();
            this.started = this.model.properties.get('started');
            this.displayInput = false;
        },

        onDestroy: function () {
            this.model.unsubscribe();
        },

        onRender: function () {
            this.showProperties();
        },

        focusInputName: function () {
            if(  this.displayInput ) {
                this.render();
                if (this.model.get('editName')) {
                    this.ui.inputName.focus();
                    this.ui.inputName.select();
                }
            }
        },

        showProperties: function () {
            // display the property started on the top, in classInfo
            if( this.started ){
                this.showChildView( 'inputStarted', new BoolView( {model: this.started }) );
            }

            this.showChildView( 'properties', new PropertiesView( { collection: this.model.properties } ) );
            this.showChildView( 'methods', new MethodsView( { collection: this.model.methods } ) );

            if ( this.options.advanced ) {
                this.showChildView( 'tree', new TreeView( { model: this.model } ) );
            }
        },

        checkForKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 27 ){
                event.preventDefault();
                this.inputEscape();
            }
            if ( key == 13 ) {
                event.preventDefault();
                this.inputEnter();
            }
        },

        inputEscape: function ( ) {
            this.model.set('editName', false );
            this.displayInput = false;
        },

        inputEnter: function ( ) {
            this.renameQuiddity(this.ui.inputName.val());
            this.model.set('editName', false );
            this.displayInput = false;
        },

        displayInput: function( event ) {
            this.displayInput = true;
            this.model.set('editName', true );
        },

        displayPropertiesPage: function( event ) {
            this.scenic.sessionChannel.commands.execute( 'inspector:close' );
            this.options.scenic.sessionChannel.vent.trigger( 'properties:show', this.model.id );
        },

        renameQuiddity: function( name ) {
            this.model.rename( name );
        },

        checkForEscapeKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 27 ) {
                event.preventDefault();
                this.scenic.sessionChannel.commands.execute( 'inspector:close' );
            }
        },

        _onQuiddityRemoved: function ( quiddity ) {
            this.scenic.sessionChannel.commands.execute( 'inspector:close', true );
        }

    } );
    return EditQuiddity;
} );