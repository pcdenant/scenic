"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    './Tree.html'
], function ( _, Backbone, Marionette, TreeTemplate ) {

    /**
     * Tree View
     *
     * @constructor
     * @extends module:Marionette.ItemView
     */
    var Tree = Marionette.ItemView.extend( {
        template:  TreeTemplate ,

        templateHelpers: function () {
            return {
                tree: JSON.stringify(this.model.get('tree'), null, '  ')
            }
        },

        ui: {
            tree: '.tree-data'
        },

        modelEvents: {
            'change:tree': 'render'
        }

    } );

    return Tree;
} );
