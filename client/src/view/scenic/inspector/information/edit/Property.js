"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    'view/scenic/inspector/information/edit/property/Number',
    'view/scenic/inspector/information/edit/property/Bool',
    'view/scenic/inspector/information/edit/property/String',
    'view/scenic/inspector/information/edit/property/Select',
    'view/scenic/inspector/information/edit/property/Fraction',
    'view/scenic/inspector/information/edit/property/Color',
    './Property.html'
], function ( _, Backbone, Marionette, i18n, NumberView, BoolView, StringView, SelectView, FractionView, ColorView, PropertyTemplate ) {

    /**
     * Property View
     *
     * @constructor
     * @extends module:Marionette.LayoutView
     */
    var Property = Marionette.LayoutView.extend( {
        template:   PropertyTemplate ,
        tagName:   'div',
        className: 'property-form',

        regions: {
            field: '.field'
        },
        events: {
            'click': 'displayDialog'
        },
        modelEvents: {
            'change': 'renderOnChange'
        },

        /**
         * Initialize
         */
        initialize: function ( options ) {
            this.scenic = options.model.scenic;
        },

        /**
        * Show a confirmation dialog if the quiddity is started
        */
        displayDialog: function () {
            if ( this.model.get('disabled') ) {
                var message = this.model.get( 'whyDisabled' );
                this.scenic.scenicChannel.commands.execute( 'inform', message);
            }
        },
        
        renderOnChange: function() {
            // Ignore if only the value has changed, let the field view handle this
            if ( _.keys(this.model.changedAttributes()).length == 1 && this.model.hasChanged('value') ) {
                return;
            }
            this.render();
        },

        /**
         * On Show Handler
         */
        onRender: function () {
            if ( !this.model.get('writable') || this.model.get('disabled') ) {
                this.$el.addClass( 'readonly' );
            } else {
                this.$el.removeClass( 'readonly' );
            }
            if ( this.model.get('disabled') ) {
                this.$el.addClass( 'disabled' );
            } else {
                this.$el.removeClass( 'disabled' );
            }
            this.showFieldView();
        },

        /**
         * Shows the view & template associated with the property type
         */
        showFieldView: function () {
            var view = null;
            switch ( this.model.get( 'type' ) ) {
                case 'float':
                case 'int':
                case 'int64':
                case 'short':
                case 'long':
                case 'double':
                case 'long double':
                case 'long long':
                case 'uint':
                case 'unsigned int':
                case 'unsigned short':
                case 'unsigned long':
                case 'unsigned long long':
                    this.$el.addClass( 'number' );
                    view = new NumberView( {model: this.model} );
                    break;
                case 'fraction':
                    this.$el.addClass( 'fraction' );
                    view = new FractionView( {model: this.model} );
                    break;
                case 'bool':
                case 'boolean':
                    this.$el.addClass( 'bool' );
                    view = new BoolView( {model: this.model} );
                    break;
                case 'selection':
                case 'enum':
                    this.$el.addClass( 'select' );
                    view = new SelectView( {model: this.model} );
                    break;
                case 'string':
                    this.$el.addClass( 'string' );
                    view = new StringView( {model: this.model} );
                    break;
                case 'color':
                    this.$el.addClass( 'color' );
                    view = new ColorView( {model: this.model} );
                    break;
                default:
                    console.warn('Unhandled property type:',this.model.get('type'));
                    this.$el.addClass( 'string' );
                    view = new StringView( {model: this.model} );
                    break;
            }
            if ( view ) {
                this.showChildView( 'field', view );
            }
        }
    } );

    return Property;
} );
