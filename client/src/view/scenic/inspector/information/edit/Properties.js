"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    'view/scenic/inspector/information/edit/Property',
    './Properties.html'
], function ( _, Backbone, Marionette, i18n, PropertyView, PropertiesTemplate ) {

    /**
     * Properties Collection View
     *
     * @constructor
     * @extends module:Marionette.CompositeView
     */
    var PropertiesView = Marionette.CompositeView.extend( {
        template:            PropertiesTemplate ,
        childViewContainer: '.properties',

        getChildView: function ( property ) {
            if ( property.get( 'type' ) == 'group' ) {
                return PropertiesView;
            } else {
                return PropertyView;
            }
        },

        templateHelpers: function () {
            return {
                isGroup: !!this.model,
                label: this.model ? i18n.t(this.model.get('label')) : ""
            }
        },

        modelEvents: {
            'change': 'renderOnChange'
        },

        ui: {
            header: '.group-header'
        },

        events: {
            'click @ui.header': 'toggle'
        },

        excludedProperties: ['started', 'devices-json', 'shmdata-writers', 'shmdata-readers'],

        /**
         * Initialize
         */
        initialize: function ( options ) {
            if ( options.model ) {
                this.expanded   = false;
                this.collection = options.model.collection;
            }
        },

        onShow: function() {
            if ( this.model && this.model.get('type') == 'group' ) {
                this.$el.addClass('group');
            }
        },

        toggle: function () {
            if ( !this.model ) {
                return;
            }
            this.model.set( 'expanded', !this.model.get( 'expanded' ) );
            this.expandOrCollapse();
        },

        renderOnChange: function () {
            // Ignore if only the value has changed, let the field view handle this
            if ( _.keys( this.model.changedAttributes() ).length == 1 && this.model.hasChanged( 'value' ) ) {
                return;
            }
            this.render();
        },

        onRender: function () {
            if ( !this.model ) {
                return;
            }
            if ( this.model.get( 'disabled' ) ) {
                this.$el.addClass( 'disabled' );
            } else {
                this.$el.removeClass( 'disabled' );
            }
            this.expandOrCollapse();
        },

        expandOrCollapse: function () {
            if ( !this.model ) {
                return;
            }
            if ( this.model.get( 'expanded' ) ) {
                this.$el.removeClass( 'collapsed' ).addClass( 'expanded' );
            } else {
                this.$el.addClass( 'collapsed' ).removeClass( 'expanded' );
            }
        },

        /**
         * Property Filter
         *
         * @param {Property} property
         * @returns {boolean}
         */
        filter: function ( property ) {
            // Filter out excluded properties
            if ( _.contains( this.excludedProperties, property.id ) ) {
                return false;
            }

            // Filter by parent if we have a model
            if ( this.model && this.model.get( 'type' ) == 'group' ) {
                return property.get( 'parent' ) == this.model.id;
            }

            // Top level keep only unparented
            if ( !this.model ) {
                return property.get( 'parent' ) == null;
            }
        }
    } );

    return PropertiesView;
} );
