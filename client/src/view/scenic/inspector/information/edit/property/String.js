"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'view/scenic/inspector/information/edit/property/Field',
    './String.html'
], function ( _, Backbone, Marionette, FieldView, StringTemplate ) {

    /**
     * String View
     *
     * @constructor
     * @extends FieldView
     */
    var StringProperty = FieldView.extend( {
        template:  StringTemplate ,

        ui: {
            property: '.property',
            update:   '.update'
        },

        events: {
            'click @ui.update': 'updateModel',
            'keypress @ui.property': 'checkForEnterKey'
        },

        /**
         * Initialize
         */
        initialize: function () {
            FieldView.prototype.initialize( this, arguments );
        },

        /**
         * Update
         *
         * @param event
         */
        updateModel: function ( event ) {
            this.model.updateValue( this.ui.property.val() );
        },

        /**
         * Set the value of the slider
         * @inheritdoc
         */
        onModelValueChanged: function( model, value, options ) {
            this.ui.property.val( value );
        },

        checkForEnterKey: function() {
            var key = event.which || event.keyCode;
            if ( key == 13 ) {
                event.preventDefault();
                this.updateModel();
            }
        }
    } );

    return StringProperty;
} );
