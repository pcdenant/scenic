"use strict";

define( [
    'underscore',
    'jquery',
    'backbone',
    'marionette',
    'view/scenic/inspector/information/edit/property/Field',
    './Color.html',
    'jquery-minicolors',
    'jquery-minicolors/jquery.minicolors.css',
], function ( _, $, Backbone, Marionette, FieldView, StringTemplate ) {

    /**
     * String View
     *
     * @constructor
     * @extends FieldView
     */
    var ColorProperty = FieldView.extend( {
        template:  StringTemplate ,

        ui: {
            property: '.property',
            update:   '.update'
        },

        events: {
            'click @ui.update': 'updateModel',
            'keypress @ui.property': 'checkForEnterKey'
        },

        /**
         * Initialize
         */
        initialize: function () {
            FieldView.prototype.initialize( this, arguments );
        },

        /**
         * Update
         *
         * @param event
         */
        updateModel: function ( event ) {
            this.model.updateValue( this.rgbaToHex(this.ui.property.minicolors('value')) );
        },

        onShow:     function () {
            $.minicolors.defaults = $.extend($.minicolors.defaults, {
                changeDelay: 500,
                position: 'bottom left',
                opacity: true,
                letterCase: 'uppercase'
               //format: 'rgb'
            });

            this.ui.property.minicolors({
                control: 'hue',
                change: ( hex, opacity ) => {
                    if ( hex ) {
                        this.model.updateValue( this.hexToRgba( hex, opacity )); 
                    }
                }
            });

            var value = this.rgbaToHex( this.model.get('value') )

            // init the minicolors value for the first time without triggering change event 
            this.ui.property.data('minicolors-initialized', false);
            this.ui.property.minicolors('value',  value.color );
            this.ui.property.minicolors('opacity', value.opacity );
            this.ui.property.data('minicolors-initialized', true);
        },

        // convert hexa to RGBA
        hexToRgba: function ( hex, opacity ) {
            var result = hex.indexOf("#") > -1 ? hex.substr(1) : hex;
            var value =  Math.round( Number( opacity ) *255 ).toString(16) ;

            result = result + ( value.length == 1 ? '0' + value : value  );

            return result;
        },

        // convert RGBA to hexa
        rgbaToHex: function ( rgba ) {
            var rgbaStr = String( rgba );
            var color = rgbaStr.slice( 0, rgbaStr.length -2 );
            var opacity = parseInt( rgbaStr.slice( rgbaStr.length -2, rgbaStr.length ), 16) / 255;

            return { color: '#' + color, opacity: opacity };
        },

        onModelValueChanged: function ( model, value, options ) {
            var valueConvert = this.rgbaToHex( value );

            this.ui.property.minicolors('value',  valueConvert.color );
            this.ui.property.minicolors('opacity', valueConvert.opacity );    
        },
        
        checkForEnterKey: function() {
            var key = event.which || event.keyCode;
            if ( key == 13 ) {
                event.preventDefault();
                this.updateModel();
            }
        }
    } );

    return ColorProperty;
} );
