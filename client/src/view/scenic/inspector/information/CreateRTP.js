define( [
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    './CreateRTP.html'
], function ( _, Backbone, Marionette, i18n, CreateRTPTemplate ) {

    /**
     * Create RTP Form
     *
     * @constructor
     * @extends module:Marionette.ItemView
     */
    var CreateRTP = Marionette.ItemView.extend( {
        template:   CreateRTPTemplate ,
        className: 'create-rtp dialog',
        ui:        {
            'name':   '.name',
            'host':   '.host',
            'port':   '.port',
            'create': '#create',
            'cancel': '#cancel'
        },
        events:    {
            'click @ui.create':  'create',
            'click @ui.cancel':  'cancel',
            'keydown':           'checkForEscapeKey',
            'keypress @ui.name': 'checkForEnterKey',
            'keypress @ui.host': 'checkForEnterKey',
            'keypress @ui.port': 'checkForEnterKey'
        },

        initialize: function ( options ) {
            this.scenic   = options.scenic;
            this.title    = i18n.t( 'Create an RTP destination' );
            this.callback = options.callback;
        },

        onAttach: function () {
            _.defer( _.bind( this.ui.name.focus, this.ui.name ) );
        },

        checkForEscapeKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 27 ) {
                event.preventDefault();
                this.scenic.sessionChannel.commands.execute( 'inspector:close' );
            }
        },

        checkForEnterKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 13 ) {
                event.preventDefault();
                this.create();
            }
        },

        create: function () {
            this.callback( {
                name: this.ui.name.val(),
                host: this.ui.host.val(),
                port: this.ui.port.val()
            } );
        },

        cancel: function () {
            this.scenic.sessionChannel.commands.execute( 'inspector:close' );
        }

    } );
    return CreateRTP;
} );