define( [
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    'view/scenic/inspector/information/edit/Tree',
    'view/scenic/inspector/information/edit/Properties',
    'view/scenic/inspector/information/edit/Methods',
    './PropertiesInfoView.html',
    'view/scenic/inspector/information/edit/property/Bool'
], function ( _, Backbone, Marionette, i18n, TreeView, PropertiesView, MethodsView, PropertiesInfoTemplate, BoolView ) {

    /**
     * Edit Quiddity Form
     *
     * @constructor
     * @extends module:Marionette.LayoutView
     */
    var PropertiesinfoView = Marionette.LayoutView.extend( {
        template:   PropertiesInfoTemplate ,
        className: 'properties-quiddity',

        ui:{
            'edit':        '.edit',
            'inputName':   '.inputName',
            'rename':      '.rename-title',
            'expand':      '.expand'
        },
        regions: {
            tree:           '.tree-container',
            properties:     '.properties-container',
            methods:        '.methods-container',
            inputStarted:   '.classInfo .field'
        },

        events: {
            'keypress':                 'checkForEscapeKey',
            'click @ui.rename':         'displayInput',
            'click @ui.expand':         'displayPropertiesPage',
            'keypress @ui.inputName':   'checkForKey'
        },

        templateHelpers: function () {
            this.started = this.model.properties.get('started');

            return {
                className: this.scenic.classes.get( this.model.get( 'class' ) ).get( 'name' ),
                classCategory: this.scenic.classes.get( this.model.get( 'class' ) ).get( 'category' ),
                classDescription: this.scenic.classes.get( this.model.get( 'class' ) ).get( 'description' ),
                id: this.started ? this.started.get( 'id' ) : null,
                label: this.started ? this.started.get( 'label' ) : null,
                description: this.started ? this.started.get( 'description' ) : null,
                advanced: this.options.advanced,
                editName: this.model.get('editName'),
                expand: this.scenic.config.propertiesPage
            }
        },

        initialize: function ( options ) {
            this.scenic    = options.scenic;
            this.inspector = options.inspector;
            this.started = null;
        },

        onBeforeShow: function () {
            this.showProperties();
        },

        onRender: function () {
            this.showProperties();
        },

        showProperties: function () {
            // display the property started on the top, in classInfo
            if( this.started ){
                this.showChildView( 'inputStarted', new BoolView( {model: this.started }) );
            }

            this.showChildView( 'properties', new PropertiesView( { collection: this.model.properties } ) );

            this.showChildView( 'methods', new MethodsView( { collection: this.model.methods } ) );
            if ( this.options.advanced ) {
                this.showChildView( 'tree', new TreeView( { model: this.model } ) );
            }
        }

    } );
    return PropertiesinfoView;
} );