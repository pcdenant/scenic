"use strict";

import TableView          from './base/TableView';
import SwitchScenesTemplate from './SwitchScenesView.html';

/**
 * PropertiesTableView
 *
 * @constructor
 * @extends module:Backbone.Model
 */

var SwitchScenesView = TableView.extend( {
    tagName: 'div',
    className: 'switchScenes',
    template: SwitchScenesTemplate,

    ui:  {
        button: '.buttonLabel'
    },

    events: {
        'click @ui.button': 'switchScene'
    },

    modelEvents: {
        'change:active': 'render',
    },

    templateHelpers: function () {
        return {
            scenes: this.model.scenic.scenes
        }
    },
    /**
     * Initialize
     */
    initialize: function( ) {
        this.listenTo( this.model.scenic.scenes, 'change:active', this.render );
    },

    switchScene: function ( event ) {
        this.model.switchScene( event.target.id );
    }
} );

export default SwitchScenesView;
