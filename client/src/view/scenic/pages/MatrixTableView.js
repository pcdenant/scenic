"use strict";

import _ from 'underscore';
import Backbone from 'backbone';
import TableView from './base/TableView';
import MatrixTableTemplate from './MatrixTableView.html';
import SourcesCollectionView from './matrix/common/SourcesCollectionView';
import MatrixMenusView from './matrix/MatrixMenusView';
import SceneActivationView from './matrix/SceneActivationView';
import SceneToggleView from './matrix/SceneToggleView';
import ScenesMenuView from './matrix/ScenesMenuView';
import DummySourceView from './base/table/DummySourceView';
import DummyDestinationView from './base/table/DummyDestinationView';
import SinkDestinationsView from './matrix/sink/SinkDestinationsView';
import ContactDestinationsView from './matrix/contacts/ContactDestinationsView';
import $ from 'jquery';

const MatrixTableView = TableView.extend( {
    className: 'table matrix',

    template:  MatrixTableTemplate,

    regions:   _.defaults( {
        'header':                   '.table-header',
        'dummyDestination':         '.destinations.dummies',
        'sinkDestinations':         '.destinations.sink',
        'contactDestinations':      '.destinations.contact',
        'rtpDestinations':          '.destinations.rtp',
        'sources':                  '.sources.items',
        'dummySource':              '.sources.dummies',
        'scenesMenu':               '.menu-scene',
        'menu':                     '.menu-matrix',
        'sceneActivation':          '.sceneActivation',
        'sceneToggle':              '.sceneToggle'
    }, TableView.prototype.regions ),

    ui: _.defaults( {
        'createSource':      '.btn.createSource',
        'createDestination': '.btn.createDestination',
        'destinations':      '.destinations',
        'content':           '.page-content',
    }, TableView.prototype.ui ),

    events: _.defaults( {
        'click @ui.createSource':      'createSource',
        'click @ui.createDestination': 'createDestination'
    }, TableView.prototype.events ),

    modelEvents: {
        'change:scenes': function() {
            // when reset or load a file, need to recreate all the view
            this.model.unsubscribeFromShmdataThumbnails();
            this._counts = {};
            this.createScenesMenu();
            this.createMatrix();
        }
    },

    templateHelpers: function () {
        return {
            disableSourcesMenu:         this.scenic.config.disableSourcesMenu,
            disableDestinationsMenu:    this.scenic.config.disableDestinationsMenu
        }
    },

    /**
     * Initialize
     */
    initialize: function () {
        TableView.prototype.initialize.apply( this, arguments );

        this._counts = {};
        this.listenTo( this.model.sip, 'change:connected', this._onConnectedChanged );
        this.listenTo( this.scenic.sip, 'change:connected', this._onShow.bind(this, 'contact' ) );

        this.listenTo( this.scenic.scenes, "add remove", this.scrollToInitialPosition );
    },

    scrollToInitialPosition: function () {
        // little hack to refresh the matrix at the good position
        $('.page-content').scrollTop($('.page-content').scrollTop()-1);
        $('.page-content').scrollLeft($('.page-content').scrollLeft()-1);
    },

    /**
     * Before Show Handler
     *
     * @private
     */
    onBeforeShow: function () {

        this.createScenesMenu();
        this.createMatrix();

        this.ui.content.scroll(function() {
            $('.destinations-wrapper-nav').css('top' , $(this).scrollTop());
            $('.activation-wrapper-nav').css('top' , $(this).scrollTop());
            $('.activation-wrapper-nav').css('left' , $(this).scrollLeft());
            $('.actions-source').css('left' , $(this).scrollLeft());
            $('.source-list-heading').css('top' , $(this).scrollTop());
            $('.noscrollH').css('left' , $(this).scrollLeft());
            $('.noscrollV').css('top' , $(this).scrollTop());
        });
    },

    onShow: function(){
        // display th ematrix with the previous scroll value
        if ( localStorage.getItem('scrollX') ) {
            $('.page-content').scrollLeft(Number(localStorage.getItem('scrollX')));
        }

        if ( localStorage.getItem('scrollY') ) {
            $('.page-content').scrollTop(Number(localStorage.getItem('scrollY')));
        }
    },

    createScenesMenu: function () {
        this.scenesMenu = new ScenesMenuView( {
            scenic: this.scenic,
            model:  this.scenic.scenes
        } );
        this.showChildView( 'scenesMenu', this.scenesMenu );

        this.sceneActivation = new SceneActivationView( {
            scenic: this.scenic,
            model:  this.model
        } );
        this.showChildView( 'sceneActivation', this.sceneActivation );

        this.sceneToggle = new SceneToggleView( {
            scenic: this.scenic,
            model:  this.model
        } );
        this.showChildView( 'sceneToggle', this.sceneToggle );
    },

    createMatrix: function () {
        if ( this.scenic.scenes.getSelectedScene() ) {
            this.menu = new MatrixMenusView({
                scenic: this.scenic,
                model: this.model
            });
            this.showChildView('menu', this.menu);

            this.showChildView('sources', new SourcesCollectionView({
                scenic: this.scenic,
                table: this.model,
                collection: this.model.getSinkSourceCollection(),
                onUpdate: this._onShow.bind(this, 'sources')
            }));

            this.showChildView('sinkDestinations', new SinkDestinationsView({
                scenic: this.scenic,
                table: this.model,
                collection: this.model.getSinkDestinationCollection(),
                onUpdate: this._onShow.bind(this, 'sink')
            }));

            this.showChildView('contactDestinations', new ContactDestinationsView({
                scenic: this.scenic,
                table: this.model,
                collection: this.model.getContactDestinationCollection(),
                onUpdate: this._onShow.bind(this, 'contacts')
            }));

            this.model.subscribeToShmdataThumbnails();
        }
    },

    onBeforeDestroy: function () {
        this.model.unsubscribeFromShmdataThumbnails();
    },

    createSource() {
        this.menu.dropSources( null, { modal: true } );
    },

    createDestination() {
        this.menu.dropDestinations( null, { modal: true } );
    },

    _onConnectedChanged: function () {
        this.scenic.sessionChannel.commands.execute( 'inspector:show', 'sip' );
    },

    /**
     * This is called back when any of the sub views renders, so that we can count how many sources/destinations
     * are currently displayed and show a message when the board is empty.
     * @param groupId
     * @param count
     * @private
     */
    _onShow( groupId, count ) {
        // retrieve the scroll value 
        if( count == 0 && groupId == 'sink') {
            var currentScrollY = $('.page-content').scrollTop();
            var currentScrollX = $('.page-content').scrollLeft();
            var scrollY = localStorage.getItem('scrollY');
            var scrollX = localStorage.getItem('scrollX');
            if (currentScrollY != scrollY) {
                localStorage.setItem('scrollY', currentScrollY);
            }
            if (currentScrollX != scrollX) {
                localStorage.setItem('scrollX', currentScrollX);
            }
        }

        if ( Number.isInteger(count) ) {
            this._counts[groupId] = count;
        }

        // TOTAL
        const total = _.reduce( this._counts, ( memo, value ) => memo + value, 0 );
        if ( total ) {
            this.$el.removeClass( 'empty' );
        } else {
            this.$el.addClass( 'empty' );
        }

        // BY TYPE
        const dummySource = this.getRegion('dummySource');

        if (dummySource) {
            if (this._counts['sources']) {
                dummySource.empty();

                // We need to check if the region dummySource exists,
                // during the destroy of this view the onUpdate is still called
            } else if (dummySource && !dummySource.hasView()) {
                dummySource.show(new DummySourceView({
                    scenic: this.scenic,
                    table: this.model,
                    model: new Backbone.Model({id: 'dummy'}),
                    onClick: this.createSource.bind(this)
                }));
            }
        }

        const dummyDestination = this.getRegion('dummyDestination');

        if (dummyDestination) {
            if (this._counts['sink'] + this._counts['contacts']) {
                dummyDestination.empty();
                // We need to check if the region dummyDestination exists,
                // during the destroy of this view the onUpdate is still called
            } else if (!dummyDestination.hasView()) {
                dummyDestination.show(new DummyDestinationView({
                    scenic: this.scenic,
                    table: this.model,
                    model: new Backbone.Model({id: 'dummy'}),
                    onClick: this.createDestination.bind(this)
                }));
            }
        }
    }

} );

export default MatrixTableView;