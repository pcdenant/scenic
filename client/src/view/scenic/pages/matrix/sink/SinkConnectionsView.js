"use strict";

import ConnectionsView from "../../base/table/ConnectionsView";
import SinkConnectionView from "./SinkConnectionView";

const SinkConnectionsView = ConnectionsView.extend({
    childView: SinkConnectionView,

    viewComparator: function ( destination ) {
        return this.options.table.sinkDestinationComparator( destination );
    },

    filter: function ( destination ) {
        return this.options.table.filterSinkDestination( destination, true );
    }
});

export default SinkConnectionsView;