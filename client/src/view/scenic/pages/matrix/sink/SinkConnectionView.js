"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'view/scenic/pages/base/table/ConnectionView'
], function ( _, Backbone, Marionette, ConnectionView ) {

    /**
     * Connection View
     *
     * @constructor
     * @extends ConnectionView
     */
    var SinkConnectionView = ConnectionView.extend( {

        /**
         * Initialize
         */
        initialize: function ( options ) {
            ConnectionView.prototype.initialize.apply( this, arguments );
            this.listenTo( this.destination.shmdatas, 'update', this.render );
        },

        select( source, destination ) {
            return this.scenic.scenes.selectSinkConnection( source, destination );
        },

        deselect( source, destination ) {
            return this.scenic.scenes.deselectConnection( source.collection.quiddity.id, destination.id );
        },

        canConnect( source, destination, callback ) {
            return destination.canConnect( source, callback );
        },

        isConnected( source, destination ) {
            return destination.isConnected( source );
        },

        connect( source, destination ) {
            return destination.connect( source, true );
        },

        disconnect( source, destination ) {
            return destination.disconnect( source, true );
        }
    } );

    return SinkConnectionView;
} );
