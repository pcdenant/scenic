"use strict";

import QuidditySourceView from "../../base/table/QuidditySourceView";
import ShmdataView from "./ShmdataView";

const QuiddityWithShmdataSourceView = QuidditySourceView.extend( {
    childView: ShmdataView,
    childViewOptions: function () {
        return {
            scenic: this.scenic,
            table:  this.options.table
        }
    },
    /**
     * Initialize
     */
    initialize: function ( options ) {
        QuidditySourceView.prototype.initialize.apply( this, arguments );
        this.collection = this.model.shmdatas;
    },

    /**
     * Filter shmdata per table
     *
     * @param shmdata
     * @returns {boolean}
     */
    filter: function ( shmdata ) {
        // Get back up to the table model to filter the displayed connections
        //return this.options.table.filterShmdata( shmdata, true );
        return shmdata.get( 'type' ) == 'writer';
    },

    onSourceOver: function ( source ) {
        this.$el.addClass( this.model.id == source.collection.quiddity.id ? "highlighted" : "faded" );
    }
} );

export default QuiddityWithShmdataSourceView;