"use strict";

import $ from 'jquery';
import i18n from 'i18n';
import Marionette from 'marionette';
import HideWhenEmpty from "../../../../behaviors/HideWhenEmpty";
import QuiddityWithShmdataSourceView from './QuiddityWithShmdataSourceView';
import SourceCollectionViewTemplate from './SourcesCollectionView.html';
import 'jquery-ui/ui/widgets/sortable';

/**
 * Source Collection View
 *
 * @constructor
 * @extends module:Marionette.CompositeView
 */
const SourcesCollectionView = Marionette.CompositeView.extend( {
    template:           SourceCollectionViewTemplate,
    childView:          QuiddityWithShmdataSourceView,
    reorderOnSort:      true,
    childViewContainer: '.sources',
    className:          'sources-container',

    ui: {
        bar:  '.source-list-heading',
        list: '.sources'
    },

    behaviors: {
        HideWhenEmpty: { behaviorClass: HideWhenEmpty }
    },

    childViewOptions: function () {
        return {
            scenic: this.scenic,
            table:  this.options.table
        }
    },

    templateHelpers: function () {
        return {
            title:     this.title
        }
    },

    viewComparator: function ( source ) {
        return this.options.table.sinkSourceComparator( source );
    },

    /**
     * Initialize
     */
    initialize: function ( options ) {
        this.scenic     = options.scenic;
        this.table      = options.table;
        this.collection = this.table.getSinkSourceCollection();

        this.title = i18n.t('Sources');

        // Listen for destination order changes to reorder the list
        this.listenTo( this.collection, "change:userData:order.source", this.reorder );

        //this.listenTo( this.collection, 'shmdata:update', this.render );
        this.listenTo( this.options.table, 'change:filter', this.render );

        // this.listenTo( this.scenic.scenes, "change:select", this.render );
        this.listenTo( this.scenic.scenes, "add remove", this.render );
    },

    onRender: function () {
        var prev = -1;
        var self = this;

        this.ui.list.on('click', 'div', function (e) {
            if ($(this).attr("id")) {
                // Select multiple items with ctrl
                if (e.ctrlKey) {
                    prev = -1;
                    if ($(this).hasClass("selectedItem")) {
                        $(this).removeClass("selectedItem");
                    } else {
                        $(this).addClass("selectedItem");
                    }

                    // Select multiple items with shift
                } else if (e.shiftKey) {
                    var curr = $(this).index();
                    $(this).addClass("selectedItem");
                    if (prev > -1) {
                        $(self.el).children(".source").each((index, ui) => {
                            if (index >= Math.min(prev, curr) && Math.max(prev, curr) >= index) {
                                $(ui).addClass('selectedItem')
                            }
                        });
                        prev = -1;
                    } else if (curr > prev) {
                        prev = curr;
                    }
                } else {
                    $('.selectedItem').removeClass('selectedItem');
                    prev = -1;
                }
            }
        }).sortable({
            axis:                       'y',
            distance:                   10,
            delay:                      150,
            cursor:                     "move",
            forceHelperSize:            true,
            forcePlaceholderSize:       true,
            handle:                     ".actions-source",
            cancel:                     ".ui-state-disabled",
            helper: (e, item) => {
                // If grab an unselected item, deselect every other item
                if (!item.hasClass('selectedItem')) {
                    item.addClass('selectedItem').siblings().removeClass('selectedItem');
                }

                // Add selected items to the multiselect item and remove them form the source list
                var elements = $('.selectedItem').clone(true);
                item.data('multiselect', elements);
                item.siblings('.selectedItem').hide();

                var helper = $('<div/>').append(elements);

                return helper;
            },
            stop: (e, ui) => {
                this._reorderSources(ui);
            },
            sort: function(event, ui) {
                var $target = $(event.target);
                // fix the offset of the sortable helper element when we have some scroll on the page
                if (!/html|body/i.test($target.offsetParent()[0].tagName)) {
                    var top = event.pageY - $target.offsetParent().offset().top - (ui.helper.outerHeight(true) / 2);
                    ui.helper.css({'top' : top + 'px'});
                }
            }
        });
    },

    _reorderSources: function( ui ) {
        var elements = ui.item.data('multiselect');
        if( elements ) {
            ui.item.after( elements );
            ui.item.siblings(":hidden").remove();
            ui.item.remove();

            this.ui.list.children( ".source" ).each(( index, ui ) => {
                if ( $( ui ).attr( "id" ) ) {
                    this.collection.get( $( ui ).attr( "id" ) ).setUserData( "order.source", index );
                }
            } );
            this.render();
        }
    },
    /**
     * Sources View Filter
     * Filters quiddities for the current table view table view
     *
     * @param quiddity
     * @returns {boolean}
     */
    filter: function ( quiddity ) {
        return this.options.table.filterSinkSource( quiddity, true );
    }

} );

export default SourcesCollectionView;