"use strict";

import $ from 'jquery';
import _ from 'underscore';
import i18n from 'i18n';
import TableMenusView from 'view/scenic/pages/base/TableMenusView';
import MatrixMenusTemplate from './MatrixMenusView.html';

const MatrixMenusView = TableMenusView.extend( {
    template: MatrixMenusTemplate,
    ui:       {
        'source':      '.menu.source',
        'customMenu':  '.menu.customMenu',
        'destination': '.menu.destination',
        'categories':  '.filter.categories'
    },
    events:   {
        'click @ui.source .button':             'dropSources',
        'click @ui.source .quiddity.item':      'createSource',
        'click @ui.customMenu .button':         'dropCustomMenu',
        'click @ui.customMenu .custom.item':    'createCustomItem',
        'click @ui.destination .button':        'dropDestinations',
        'click @ui.destination .quiddity.item': 'createQuiddityDestination',
        'click @ui.destination .rtp.item':      'addRTPDestination',
        'click @ui.destination .contact.item':  'addContactDestination',
        'click @ui.categories .button':         'dropCategories',
        'click @ui.categories .category.item':  'filter',
    },

    templateHelpers: function () {
        return {     
            disableSourcesMenu: this.scenic.config.disableSourcesMenu,
            customMenus: this.scenic.config.customMenus,
            disableDestinationsMenu: this.scenic.config.disableDestinationsMenu
        }
    },

    initialize: function () {
        TableMenusView.prototype.initialize.apply( this, arguments );
        this.listenTo( this.model.scenic.quiddities, 'update', this.render );
        this.listenTo( this.model.scenic.quiddities, 'shmdata:update', this.render );

        // this.scenic.sessionChannel.vent.on( 'quiddity:added', this._onQuiddityAdded, this );
        this.listenTo( this.model.scenic.quiddities, 'add', this._onQuiddityAdded );
        this.listenTo( this.model.scenic.quiddities, 'remove', this._onQuiddityRemoved );

        this.customMenus =        JSON.parse(JSON.stringify(this.scenic.config.customMenus));

        // Check if the quiddity is exclusive, in this case remove it from the source, destination or custom menu
        this.model.scenic.quiddities.each( quiddity => {

            var quidInfo = function( item ) {
                return item.quiddity == quiddity.get('class')
            };

            var quidInCustomMenu;

            this.customMenus.forEach( ( menu , index ) => {
                if ( menu.items ){
                    quidInCustomMenu = menu.items.find( quidInfo );
                } else {
                    menu.subMenus.forEach( ( subMenu, indexSub ) => {
                        quidInCustomMenu =  subMenu.items.find( quidInfo );
                        var menu = this.customMenus[index];
                        var subMenu = menu.subMenus[indexSub];
                        subMenu.label = i18n.t(subMenu.label);
                        if ( quidInCustomMenu && quidInCustomMenu.exclusive ) {
                            var items =  subMenu.items.filter( item => { return item.quiddity != quidInCustomMenu.quiddity; });
                            subMenu.items = items;
                        }
                    });
                }
            } )
        });

    },

    onRender: function () {
    },

    // Remove the quiddity in the source, destination or custom menus if exclusive
    _onQuiddityAdded( quiddity ) {
        var quidInfo =  item => {
                return item.quiddity == quiddity.get('class')
        };

        if ( this.scenic.config.customMenus ) {
            this.scenic.config.customMenus.forEach( ( menu , index ) => {
                if ( menu.items ){
                    var quidInCustomMenu = menu.items.find( quidInfo );
                    if ( quidInCustomMenu && quidInCustomMenu.exclusive ){
                        this.customMenus[index].items = this.customMenus[index].items.filter( item => { return item.quiddity != quidInCustomMenu.quiddity; });

                    }
                } else {
                    menu.subMenus.forEach( ( subMenu, indexSub ) => {
                        var quidInCustomMenu =  subMenu.items.find( quidInfo );
                        if ( quidInCustomMenu && quidInCustomMenu.exclusive ){
                            var menuQuid = this.customMenus[index];
                            var subMenuQuid = menuQuid.subMenus[indexSub];
                            subMenuQuid.items = subMenuQuid.items.filter( item => { return item.quiddity != quidInCustomMenu.quiddity; })
                        }
                    });
                }
            } )
        }
    },

    // Add the quiddity in the source, destination or custom menus if exclusive
    _onQuiddityRemoved( quiddity ) {
        var quidInfo = item => {
                return item.quiddity == quiddity.get('class')
        };
        
        if ( this.scenic.config.customMenus ) {
            this.scenic.config.customMenus.forEach( ( menu , index ) => {
                if ( menu.items ){
                    var quidInCustomMenu = menu.items.find( quidInfo );
                    if ( quidInCustomMenu && quidInCustomMenu.exclusive ){
                        var indexQuid = this.customMenus[index].items.findIndex( item => { return item == quidInCustomMenu } );
                        this.customMenus[index].items.splice( indexQuid, 0, quidInCustomMenu );
                    }
                } else {
                    menu.subMenus.forEach( ( subMenu, indexSub ) => {
                        var quidInCustomMenu =  subMenu.items.find( quidInfo );
                        if ( quidInCustomMenu && quidInCustomMenu.exclusive ){
                            var menuQuid = this.customMenus[index];
                            var subMenuQuid = menuQuid.subMenus[indexSub];
                            var indexQuid = subMenu.items.findIndex( item  => { return item == quidInCustomMenu } );
                            subMenuQuid.items.splice( indexQuid, 0, quidInCustomMenu );
                        }
                    });
                }
            } )
        }
    },
    /**
     * Drop the sources menu
     *
     * @param event
     */
    dropSources: function ( event, options = {} ) {
        const sourcesMenu = this.mapQuiddityMenu( this.model.getSinkSources() );
        this.drop( this.ui.source, sourcesMenu, null, options.modal );
    },

    createSource( event ) {
        return this.createQuidditySource( event );
    },

    /**
     * Drop the custom menu
     *
     * @param event
     */
    dropCustomMenu: function ( event, options = {} ) {
        const customMenu  = {};
        const customMenuConfig = _.findWhere(this.customMenus, {label: $(event.currentTarget).attr('id')});
        var menuSelected = $(event.currentTarget).closest('.menu');

        customMenu.label = customMenuConfig.label;
        customMenu.items = {};

        if ( this.scenic.config.customMenus ) {
            // No subMenu display directly the items in the menu
            if ( customMenuConfig.items ) {
                customMenu.items['noGroup'] = this.getCustomMenu( customMenuConfig.items );
            } else {
                customMenuConfig.subMenus.forEach(  submenu  => {
                    customMenu.items[submenu.label] = this.getCustomMenu( submenu.items );
                });                
            }
        }
        
        customMenu.label = customMenuConfig.label;
        this.drop( menuSelected , customMenu, null, options.modal );
    },

    createCustomItem( event ) {
        var customMenu = _.findWhere(this.customMenus, {label: $( event.currentTarget ).attr( 'label' )});
        var customItem;
        var subMenu;

        if( customMenu.items ) {
            customItem = customMenu.items[$( event.currentTarget ).data( 'id' )];
        } else {
            subMenu = _.findWhere(customMenu.subMenus, {label : $( event.currentTarget ).data( 'submenu' )});
            customItem = subMenu.items[$( event.currentTarget ).data( 'id' )];
        }

        if ( !customItem ) {
            console.warn( "Custom item not found", $( event.currentTarget ).data( 'id' ) );
            return;
        }

        this.closeMenu();

        var quiddity = {
                type:   this.model.scenic.classes.get( customItem.quiddity ).get( 'class' ),
                name:   customItem.name,
                device: null,
                properties: customItem.properties
        };
        this.model.scenic.quiddities.create( quiddity );
    },

    /**
     * Drop the destinations menu
     *
     * @param event
     */
    dropDestinations: function ( event, options = {} ) {
        const destinationsMenu = this.mapQuiddityMenu( this.model.getSinkDestinations() );

        const contacts = this.model.getContactDestinations().map( contact => ({
            id:      contact.id,
            label:   contact.get( 'name' ),
            title:   i18n.t( 'Add contact' ),
            tooltip: i18n.t( 'Add __contact__ as a destination to the board', { contact: contact.get( 'name' ) } ),
            type:    'contact',
            status:  contact.get( 'status' )
        }) );

        if ( contacts.length ) {
            destinationsMenu['contacts'] = contacts;
        }

        if ( !this.scenic.config.disableRTP ) {
            destinationsMenu['rtp'] = [
                {
                    id:      'rtp',
                    label:   i18n.t( 'RTP Destination' ),
                    title:   i18n.t( 'RTP destination' ),
                    tooltip: i18n.t( 'Add an RTP destination to the board' ),
                    group:   'RTP',
                    type:    'rtp'
                }
            ];
        }
        this.drop( this.ui.destination, destinationsMenu, null, options.modal );
    },

    dropCategories: function ( event, options = {} ) {
        var categories = [];

        this.model.scenic.quiddities
            .filter( quiddity => this.model.filterSinkSource( quiddity ) || this.model.filterSinkDestination( quiddity ) )
            .forEach( quiddity => {
                // Add quiddity category to the menu
                categories.push( quiddity.get( 'classDescription' ).get( 'category' ) );
                // Add shmdata categories to the menu
                quiddity.shmdatas.forEach( shmdata => {
                    const shmCategory = shmdata.get( 'category' );
                    if ( !_.isEmpty( shmCategory ) ) {
                        categories.push( shmCategory );
                    }
                } );
            } );
        
        // detect which filter is already selected
        var data = [];

        data.push( {'category': i18n.t('all categories') ,  'label': i18n.t( 'all categories'), 'selected': this.model.get( 'filter' ).length == 0 ? 'state-selected' : ''} );

        _.uniq( categories ).sort().forEach ( category => { 
            var selected = "";

            if( _.contains( this.model.get( 'filter' ), category ) ) {
                selected = "state-selected";
            }
            data.push( {'category': category , 'label': i18n.t( category ), 'selected': selected} );
        })

        this.dropMultiSelect( this.ui.categories, data, null, options.modal );
    },

    /**
     * Add a contact destination
     *
     * @param event
     */
    addContactDestination: function ( event ) {
        this.closeMenu();
        this.model.scenic.sip.contacts.get( $( event.currentTarget ).data( 'id' ) ).addAsDestination()
    },

    /**
     * Create RTP Destination
     *
     * @param event
     */
    addRTPDestination: function ( event ) {
        this.closeMenu();
        this.scenic.sessionChannel.commands.execute( 'rtp:create', _.bind( this.model.createRTPDestination, this.model ) );
    },

    getCustomMenu( items ) {
        return items.map( ( item, index ) => ({
            id:    index,
            label: i18n.t( item.name ),
            group: 'Custom',
            type:  'custom'
        }) );
    },
    /**
     * Filter table
     *
     * @param event
     */
    filter: function ( event ) {
        this.closeMenu();

        if( $( event.currentTarget ).data( 'id' ) == 'all categories' ) {
            this.model.set( 'filter', [] );
        } else {
            var filters = this.model.get( 'filter' );
            // remove filter if already present otherwise add it
            if (_.contains(filters, $( event.currentTarget ).data( 'id' ))) {
                filters = _.without(filters, $( event.currentTarget ).data( 'id' ));
            } else {
                filters.push($( event.currentTarget ).data( 'id' ));
            } 
            // TO FIX
            this.model.set( 'filter', [] );
            this.model.set( 'filter', filters);
        }
    }
} );

export default MatrixMenusView;