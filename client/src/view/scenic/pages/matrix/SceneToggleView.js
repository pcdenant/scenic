"use strict";

import Field from 'view/scenic/inspector/information/edit/property/Field';
import SceneToggleTemplate from './SceneToggleView.html';

const ScenesToggleView = Field.extend( {
    template: SceneToggleTemplate,


    ui: {
        'toggle':  '.toggle',
    },

    events: {
        'click @ui.toggle': 'updateModel'
    },

    templateHelpers: function () {
        return {
            toggled:  this.model.scenic.scenes.getSelectedScene() ? this.model.scenic.scenes.getSelectedScene().get('properties').get('toggle') : false
        }
    },

    /**
     * Initialize
     */
    initialize: function () {
        this.listenTo( this.model.scenic.scenes, 'add remove', this.render );
        this.listenTo( this.model.scenic.scenes, 'change:active', this.render );
        this.listenTo( this.model.scenic.scenes, 'change:select', this.render );
        this.listenTo( this.model.scenic.scenes, 'change:toggle', this.render );
    },

    /**
     * Update Model Value
     *
     * @param event
     */
    updateModel: function ( event ) {
        var toggled =  this.model.scenic.scenes.get(this.model.scenic.scenes.getSelectedScene().id ).get('properties').get('toggle') ;

        if ( !toggled ){
            this.model.scenic.scenes.toggleScene( this.model.scenic.scenes.getSelectedScene().id );
        } else {
            this.model.scenic.scenes.untoggleScene( this.model.scenic.scenes.getSelectedScene().id );
        }
    }

} );

export default ScenesToggleView;