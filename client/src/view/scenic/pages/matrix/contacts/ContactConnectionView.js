"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'view/scenic/pages/base/table/ConnectionView'
], function ( _, Backbone, Marionette, ConnectionView ) {

    /**
     * Connection View
     *
     * @constructor
     * @extends ConnectionView
     */
    var ContactConnectionView = ConnectionView.extend( {

        /**
         * Initialize
         */
        initialize: function ( options ) {
            ConnectionView.prototype.initialize.apply( this, arguments );
            this.listenTo( this.model, 'change:connections', this.render );
        },

        templateHelpers: function () {
            return {
                connected: this.isConnected( this.source, this.destination ),
                selected:  this.isSelected( this.source, this.destination ),
            }
        },

        toggleConnection: function () {
            if ( !this._canConnect ) {
                return;
            }

            if ( this.isConnected( this.source, this.destination ) ) {
                this.disconnect( this.source, this.destination );
            } else {
                this.connect( this.source, this.destination );
            }
        },

        select( source, destination ) {
            return this.scenic.scenes.selectConnection( source, destination, 'contact' );
        },

        deselect( source, destination ) {
            return this.scenic.scenes.deselectConnection( source, destination );
        },

        canConnect( source, destination, callback ) {
            return this.options.table.canConnectContact( source, destination, callback );
        },

        isConnected( source, destination ) {
            return this.options.table.isContactConnected( source, destination );
        },

        connect( source, destination ) {
            return this.options.table.connectContact( source, destination );
        },

        disconnect( source, destination ) {
            return this.options.table.disconnectContact( source, destination );
        }
    } );

    return ContactConnectionView;
} );
