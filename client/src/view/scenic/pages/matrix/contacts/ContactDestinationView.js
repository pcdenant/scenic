"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    'view/scenic/pages/base/table/DestinationView',
    './ContactDestinationView.html'
], function ( _, Backbone, Marionette, i18n, DestinationView, ContactDestinationTemplate ) {

    /**
     * SIP Destination View
     *
     * @constructor
     * @extends DestinationView
     */
    var ContactDestinationView = DestinationView.extend( {
        template:  ContactDestinationTemplate ,
        className: 'destination',

        attributes: function () {
            return {
                class: [ 'sip', 'destination', this.model.get('send_status') == 'calling' ? 'connected' : 'disconnected'].join(' ')
            }
        },

        ui: {
            call:   '.actions-menu .items .item.call',
            hangup: '.actions-menu .items .item.hangup',
            remove: '.actions-menu .items .item.remove',
            more:   '.action.more-action'
        },

        events: {
            'click @ui.call':   'callDestination',
            'click @ui.hangup': 'hangDestination',
            'click @ui.remove': 'removeDestination',
            'click @ui.more':   'dropMore'
        },

        modelEvents: {
            'change:send_status': 'render',
            'change:name':  'render',
            'change:actions':  'render',
            'change:onCall':  'render'
        },

        templateHelpers: function () {
            return {
                spin: this.model.get('onCall')
            }
        },

        /**
         * Initialize
         */
        initialize: function( ) {
            DestinationView.prototype.initialize.apply(this, arguments);
        },

        /**
         * Attach handler
         */
        onRender: function() {
            // Update Dynamic Attributes
            this.$el.attr(this.attributes());
        },

        callDestination: function() {
            this.closeMenu();
            this.model.call();   
        },

        hangDestination: function() {
            this.closeMenu();
            this.model.hangUp();
        },
        /**
         * Drop the menu More with edit and remove actions
         * @param event
         */
        dropMore: function ( event ) {
            this.drop( this.$el, this.model.get( 'actions' ) );
        },
        /**
         * Remove Handler
         *
         * @inheritdoc
         * @param event
         */
        removeDestination: function( event ) {
            var self = this;
            this.closeMenu();
            this.scenic.scenicChannel.commands.execute( 'confirm', i18n.t('Are you sure you want to remove the __destinationName__ destination?', {destinationName:this.model.get('name')}), function( confirmed ) {
                if ( confirmed ) {
                    self.model.disconnectAll();
                }
            });
        }
    } );

    return ContactDestinationView;
} );
