"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    './SettingsTableView.html'
], function ( _, Backbone, Marionette, SettingsTemplate ) {

    /**
     *  @constructor
     *  @augments PageView
     */
    var SettingsTableView = Marionette.ItemView.extend( {
        tagName: 'div',
        className: 'settings',
        template: SettingsTemplate,
        ui: {
            resetLogin:     '#resetLogin',
            newPassword:    '#newPassword',
            createLogin:    '#createLogin',
            checkboxAuth:   '#checkboxAuth'
        },
        events: {
            'click @ui.resetLogin': 'resetLogin',
            'click @ui.createLogin': 'createLogin',
            'change @ui.checkboxAuth':   'updateAuthentication'
        },
        modelEvents: {
            'change:authOn': 'render',
            'change:haveUser': 'render'
        },
        /**
         * Initialize
         */
        initialize: function( ) {

        },
        updateAuthentication() {
            this.model.set('authOn', this.ui.checkboxAuth.is( ':checked' ) );
        },
        resetLogin() {
            this.model.resetLogin();
        },
        createLogin() {
            this.model.createLogin( this.ui.newPassword.val() );
        },
    } );

    return SettingsTableView;
} );
