"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'view/scenic/pages/advanced/QuiddityView',
    './QuidditiesView.html'
], function ( _, Backbone, Marionette, QuiddityView, QuidditiesTemplate ) {

    /**
     *  @constructor
     *  @augments Marionette.ItemView
     */
    var QuidditiesView = Marionette.CompositeView.extend( {
        tagName:          'table',
        className:        'quiddities',
        template:          QuidditiesTemplate ,
        childView:        QuiddityView,
        childViewOptions: function () {
            return {
                scenic: this.scenic
            }
        },
        /**
         * Initialize
         */
        initialize:       function ( options ) {
            Marionette.CompositeView.prototype.initialize.apply( this, arguments );
            this.scenic     = options.scenic;
            this.collection = this.scenic.quiddities;
        }
    } );

    return QuidditiesView;
} );
