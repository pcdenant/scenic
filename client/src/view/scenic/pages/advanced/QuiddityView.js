"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    './QuiddityView.html'
], function ( _, Backbone, Marionette, i18n, QuiddityTemplate ) {

    /**
     *  @constructor
     *  @augments Marionette.ItemView
     */
    var QuiddityView = Marionette.ItemView.extend( {
        template:   QuiddityTemplate ,
        tagName:   'tr',
        className: "quiddity",

        templateHelpers: function () {
            return {
                readOnly:         this.model.get( 'readOnly' ),
                startable:        this.model.properties.get( 'started' ) != null,
                started:          this.model.properties.get( 'started' ) ? this.model.properties.get( 'started' ).get( 'value' ) : true,
                classDescription: this.model.get( 'classDescription' ).toJSON()
            }
        },

        events: {
            'click':   'editQuiddity'
        },
        
        modelEvents: {
            'change:name':  'render'
        },
        /**
         * Initialize
         */
        initialize: function ( options ) {
            this.scenic = options.scenic;
            if ( this.model.properties.get('started' ) ) {
                this.listenTo( this.model.properties.get( 'started' ), 'change:value', this.render );
            }
        },

        /**
         * Edit Handler
         * @param event
         */
        editQuiddity: function ( event ) {
            this.model.edit({advanced: true});
        }
    } );

    return QuiddityView;
} );
