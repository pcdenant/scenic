'use strict';

import $ from 'jquery';
import _ from 'underscore';
import i18n from 'i18n';
import Marionette from 'marionette';
import SubMenuTemplate from './menu/SubMenu.html';
import SubMenuCategoryTemplate from './menu/SubMenuCategory.html';
import 'jquery-ui/ui/widgets/button';
import 'jquery-ui/ui/widgets/accordion';

/**
 *  @constructor
 *  @augments module:Marionette.ItemView
 */
const TableMenusView = Marionette.ItemView.extend( {
    initialize: function ( options ) {
        this.scenic          = options.scenic;
        this.subMenuTemplate = SubMenuTemplate;
        this.SubMenuCategoryTemplate = SubMenuCategoryTemplate;
    },

    /**
     * Map a list of quiddities to the menu structure
     *
     * @param models
     * @returns {*}
     */
    mapQuiddityMenu( models ) {
        return _.groupBy( _.map( models, function ( model ) {
            return {
                id:      model.get( 'class' ),
                label:   i18n.t( model.get( 'name' )),
                title:   i18n.t( model.get( 'name' )),
                tooltip: i18n.t( model.get( 'description' )),
                group:   i18n.t( model.get( 'category' ) ),
                type:    'quiddity'
            };
        }, this ), 'group' );
    },

    /**
     * Drop Menu
     *
     * @param data
     */
    drop( anchor, data, activeIndex, modal = false ) {
        this.closeMenu();

        if ( data.length == 0 ) {
            return;
        }
        if (data.items){
             $( anchor ).append( this.subMenuTemplate( {
                groups: data.items,
                label: data.label
            } ) );           
         } else {
             $( anchor ).append( this.subMenuTemplate( {
                groups: data,
                label: ''
            } ) );      
         }


        $( '.sub-menu .content' ).accordion( {
            collapsible: true,
            active:      activeIndex != null ? activeIndex : false,
            heightStyle: 'content',
            icons:       false,
            animate:     125
        } );

        //TODO: Just style the link correctly
        $( '.sub-menu a' ).button();

        this.bodyClickHandler = _.bind( this.closeMenu, this );

        const $overlay = $( '.overlay' );
        if ( modal ) {
            $overlay.addClass('visible');
        }
        $overlay.on( 'click', this.bodyClickHandler );
    },

    /**
     * Drop Menu
     *
     * @param data
     */
    dropMultiSelect( anchor, data, activeIndex, modal = false ) {
        this.closeMenu();

        if ( data.length == 0 ) {
            return;
        }

        $( anchor ).append( this.SubMenuCategoryTemplate( {
            items: data,
        } ) );           

        //TODO: Just style the link correctly
        $( '.sub-menu-categories a' ).button();

        this.bodyClickHandler = _.bind( this.closeMenu, this );

        const $overlay = $( '.overlay' );
        if ( modal ) {
            $overlay.addClass('visible');
        }
        $overlay.on( 'click', this.bodyClickHandler );
    },

    /**
     * Close Menu
     *
     * @param event
     */
    closeMenu( event ) {
        $( '.sub-menu-categories' ).remove();
        $( '.sub-menu' ).remove();
        $( '.overlay' ).off( 'click', this.bodyClickHandler );
    },

    /**
     * Create a quiddity source
     *
     * @param event
     */
    createQuidditySource( event ) {
        this.closeMenu();
        var quiddity = {
                type:   this.model.scenic.classes.get( $( event.currentTarget ).data( 'id' ) ).get( 'class' ),
                name:   '',
                device: null
        };
        this.model.scenic.quiddities.create( quiddity );
    },

    /**
     * Create a quiddity destination
     *
     * @param event
     */
    createQuiddityDestination( event ) {
        this.closeMenu();
        var quiddity = {
                type:   this.model.scenic.classes.get( $( event.currentTarget ).data( 'id' ) ).get( 'class' ),
                name:   '',
                device: null
        };
        this.model.scenic.quiddities.create( quiddity );
    }
} );

export default TableMenusView;