"use strict";

import Marionette from 'marionette';

/**
 *  @constructor
 *  @augments module:Marionette.LayoutView
 */
const TableView = Marionette.LayoutView.extend( {
    tagName:   'div',
    className: 'table',


    /**
     * Initialize
     */
    initialize: function ( options ) {
        this.scenic = options.scenic;
    }

} );

export default TableView;
