"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    './SourceView',
    'jquery-ui/ui/widgets/sortable'
], function ( $, _, Backbone, Marionette, SourceView ) {

    /**
     * Source Collection
     *
     * @constructor
     * @augments module:Marionette.CollectionView
     */
    var SourcesView = Marionette.CollectionView.extend( {
        className:        'source-list',
        reorderOnSort:    true,
        getChildView:     function ( item ) {
            // This is per-quiddity and not per-table that's why
            // it was not passed as an option
            return this.options.sourceView ? this.options.sourceView : SourceView;
        },
        childViewOptions: function () {
            return {
                scenic:          this.scenic,
                table:           this.options.table,
                connectionView:  this.options.connectionView
            }
        },

        viewComparator: function ( source ) {
            return this.options.table.quidditySourceComparator( source );
        },

        /**
         * Initialize
         */
        initialize: function ( options ) {
            this.scenic = options.scenic;
            this.listenTo( this.options.table, 'change:filter', this.render );
            
            // Listen for destination order changes to reorder the list
            this.listenTo( this.collection, "change:userData:order.source", this.reorder );
        },

        onRender: function () {
                var self = this;
                this.$el.sortable( {
                    axis:                 'y',
                    distance:             5,
                    delay:                150,
                    //containment:          "parent",
                    cursor:               "move",
                    forceHelperSize:      true,
                    forcePlaceholderSize: true,
                    handle:               "> .controls",
                    update:               function ( event, ui ) {
                        self.$el.children( ".source" ).each( function ( index, ui ) {
                            self.collection.get( $( ui ).attr( "id" ) ).setUserData( "order.source", index );
                        } );
                    }
                } );
        },

        /**
         * Sources View Filter
         * Filters quiddities for the current table view table view
         *
         * @param quiddity
         * @returns {boolean}
         */
        filter: function ( quiddity ) {
            return this.options.table.filterSource( quiddity, true );
        }
    } );

    return SourcesView;
} );
