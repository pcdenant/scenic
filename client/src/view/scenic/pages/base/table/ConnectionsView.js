"use strict";

import Marionette from "marionette";
import HideWhenEmpty from "../../../../behaviors/HideWhenEmpty";
import ConnectionsViewTemplate from "./ConnectionsView.html";

const ConnectionsView = Marionette.CompositeView.extend( {
    template:         ConnectionsViewTemplate,
    reorderOnSort:    true,
    childViewOptions: function () {
        return {
            scenic: this.options.scenic,
            table:  this.options.table,
            source: this.options.source
        };
    },
    
    behaviors: {
        HideWhenEmpty: { behaviorClass: HideWhenEmpty }
    },

    /**
     * Initialize
     */
    initialize: function ( options ) {
        Marionette.CompositeView.prototype.initialize.apply( this, arguments );

        // Listen for destination order changes to reorder the list
        this.listenTo( this.collection, "change:userData:order.destination", this.reorder );
    }

} );

export default ConnectionsView;