"use strict";

import $ from 'jquery';
import i18n from 'i18n';
import Marionette from 'marionette';
import HideWhenEmpty from "../../../../behaviors/HideWhenEmpty";
import DestinationViewTemplate from './DestinationsView.html';
import 'jquery-ui/ui/widgets/sortable';

/**
 * Destinations  View
 *
 * @constructor
 * @extends module:Marionette.CompositeView
 */
const DestinationView = Marionette.CompositeView.extend( {
    template:           DestinationViewTemplate,
    reorderOnSort:      true,
    className:          'destinations-container',
    childViewContainer: '.destination-list',

    ui: {
        list: '.destination-list'
    },


    childViewOptions: function () {
        return {
            scenic: this.scenic,
            table:  this.options.table
        }
    },

    templateHelpers: function () {
        return {
            title:     this.title
        }
    },

    behaviors: {
        HideWhenEmpty: { behaviorClass: HideWhenEmpty }
    },

    /**
     * Initialize
     */
    initialize: function ( options ) {
        this.scenic     = options.scenic;
        this.table      = options.table;

        // Listen for destination order changes to reorder the list
        this.listenTo( this.collection, "change:userData:order.destination", this.reorder );
        this.listenTo( this.options.table, 'change:filter', this.render );
    },

    onRender: function () {
        var prev = -1;
        var self = this;

        this.ui.list.on('click', 'div', function ( e ) {

            if ( $( this ).attr( "id" ) ) {
                // Select multiple items with ctrl
                if ( e.ctrlKey ) {
                    prev = -1;
                    if($( this ).hasClass("selectedItem")) {
                        $( this ).removeClass("selectedItem");
                    } else {
                        $( this ).addClass("selectedItem");
                    }

                    // Select multiple items with shift
                } else if ( e.shiftKey ) {
                    var curr = $( this ).index();
                    $( this ).addClass( "selectedItem" );
                    if( prev > -1 ) {
                        self.ui.list.children( ".destination" ).each(( index, ui ) => {
                            if ( index >= Math.min( prev, curr ) && Math.max( prev, curr ) >= index ) {
                                $( ui ).addClass( 'selectedItem' )
                            }
                        } );
                        prev = -1;
                    } else if ( curr > prev ) {
                        prev = curr;
                    }
                } else {
                    $('.selectedItem').removeClass('selectedItem');
                    prev = -1;
                }
            }
        }).sortable( {
            axis:                 'x',
            distance:             10,
            delay:                150,
            cursor:               "move",
            forceHelperSize:      true,
            forcePlaceholderSize: true,
            handle:               "> .controls",
            //items: "div:not(.ui-state-disabled)",
            helper: (e, item) => {
                // If grab an unselected item, deselect every other item
                if ( !item.hasClass('selectedItem') ) {
                    item.addClass('selectedItem').siblings().removeClass('selectedItem');
                }

                // Add selected items to the multiselect item and remove them form the source list
                var elements = $('.selectedItem').clone( true );
                item.data( 'multiselect', elements );
                item.siblings('.selectedItem').hide();

                var helper = $('<div/>').append( elements );

                // Add flex to display in a row
                helper.addClass('destination-list');

                return helper;
            },
            // Set the width of the helper depending of the number of selected item
            over: function(e, ui){
                var count = $(".ui-sortable-helper > *").length;
                ui.helper.width(count * 72);
            },
            sort: function(event, ui) {
                var $target = $(event.target);
                // fix the offset of the sortable helper element when we have some scroll on the page
                if (!/html|body/i.test($target.offsetParent()[0].tagName)) {
                    var top = event.pageX - $target.offsetParent().offset().left - (ui.helper.outerHeight(true) / 2);
                    ui.helper.css({'left' : top + 'px'});
                }
            },
            stop: (e, ui) => {
                this._reorderDestinations( ui );
            }
        } );
    },

    _reorderDestinations: function( ui ) {
        var elements = ui.item.data('multiselect');

        if( elements ) {
            ui.item.after( elements );
            ui.item.siblings(":hidden").remove();
            ui.item.remove();

            this.ui.list.children( ".destination" ).each(( index, ui ) => {
                if ( this.collection.get( $( ui ).attr( "id" ) ).setUserData ) {
                    this.collection.get($(ui).attr("id")).setUserData("order.destination", index);
                }
            } );
            this.render();
        }
    }

} );

export default DestinationView;