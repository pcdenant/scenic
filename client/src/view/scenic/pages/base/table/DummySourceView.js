"use strict";

import _ from "underscore";
import SourceView from "./SourceView";
import DummySourceTemplate from "./DummySourceView.html";

const DummySourceView = SourceView.extend( {
    template:  DummySourceTemplate ,

    className: 'dummy source',

    events: _.defaults( {
        'click': 'addDestination'
    }, SourceView.prototype.events ),

    templateHelpers: function () {
        return {
            disableSourcesMenu: this.scenic.config.disableSourcesMenu
        };
    },

    initialize: function ( options ) {
        this.scenic = options.scenic;
        if ( this.scenic.config.disableSourcesMenu ) {
             this.$el.addClass('nohover');
        }
    },

    addDestination() {
        if ( this.options.onClick ) {
            this.options.onClick();
        }
    }
} );

export default DummySourceView;