"use strict";

import TableView from './base/TableView';
import SourcesView from './base/table/SourcesView';
import ControlMenusView from './control/ControlMenusView';
import ControlDestinationsView from './control/ControlDestinationsView';
import ControlSourceView from './control/ControlSourceView';
import ControlConnectionView from './control/ControlConnectionView';
import ControlTableViewTemplate from "./ControlTableView.html";

/**
 *  @constructor
 *  @augments TableView
 */
const ControlTableView = TableView.extend( {
    template: ControlTableViewTemplate,
    className: 'table control',

    regions: {
        'destinations': '.destinations',
        'sources':      '.sources',
        'menu':         '.menu-bar'
    },
    /**
     * Initialize
     */
    initialize: function () {
        TableView.prototype.initialize.apply( this, arguments );
    },

    /**
     * Before Show Handler
     *
     * @private
     */
    onBeforeShow: function () {
        this.showChildView( 'menu', new ControlMenusView({
            scenic: this.scenic,
            model: this.model
        }));

        this.showChildView( 'sources', new SourcesView( {
            scenic:          this.scenic,
            table:           this.model,
            collection:      this.model.getSourceCollection(),
            sourceView:      ControlSourceView,
            connectionView:  ControlConnectionView
        } ) );

        this.showChildView( 'destinations', new ControlDestinationsView( {
            scenic:     this.scenic,
            table:      this.model,
            collection: this.model.getDestinationCollection()
        } ) );
    }
} );

export default ControlTableView;
