"use strict";

import i18n from "i18n";
import DestinationView from '../base/table/DestinationView';
import ControlDestinationTemplate from './ControlDestinationView.html';

/**
 * Control Destination View
 *
 * @constructor
 * @extends DestinationView
 */
const ControlDestination = DestinationView.extend( {
    template:  ControlDestinationTemplate,
    className: 'control destination',
    ui: {
        remove: '.actions .action.remove'
    },
    events: {
        'click @ui.remove': 'removeDestination'
    },
    templateHelpers: function () {
        return {
            quiddity: this.model.has( 'property' ) ? this.model.get( 'property' ).collection.quiddity.toJSON() : null,
            property: this.model.has( 'property' ) ? this.model.get( 'property' ).toJSON() : null
        }
    },

    /**
     * Initialize
     */
    initialize: function () {
        DestinationView.prototype.initialize.apply( this, arguments );
    },

    /**
     * Remove Handler
     * @param event
     */
    removeDestination: function ( event ) {
        var self = this;
        this.scenic.scenicChannel.commands.execute( 'confirm', i18n.t( 'Are you sure you want to remove all mappings to property __property__ of __quiddity__?', {
            quiddity: this.model.get( 'property' ).collection.quiddity.id,
            property: this.model.get( 'property' ).get( 'name' )
        } ), function ( confirmed ) {
            if ( confirmed ) {
                self.model.destroy();
            }
        } );
    }

} );

export default ControlDestination;
