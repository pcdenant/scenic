"use strict";

import $ from 'jquery';
import _ from 'underscore';
import i18n from 'i18n';
import Backbone from 'backbone';
import Marionette from 'marionette';
import TableMenusView from 'view/scenic/pages/base/TableMenusView';
import ControlMenusTemplate from './ControlMenusView.html';
import 'jquery-ui/ui/widgets/selectmenu';

/**
 * @constructor
 * @extends TableMenusView
 */
const ControlMenusView = TableMenusView.extend( {
    template: ControlMenusTemplate,

    ui: {
        'source':     '.menu.source',
        'properties': '.menu.properties',
        'filter':     '.filter-select',
        'categories':  '.filter.categories'
    },

    events: {
        'click @ui.source .button':             'dropSources',
        'click @ui.source .item':               'createQuidditySource',
        'click @ui.properties .button':         'dropProperties',
        'click @ui.properties .item':           'createControlDestination',
        'click @ui.categories .button':         'dropCategories',
        'click @ui.categories .category.item':  'filter'
    },

    /**
     * Initialize
     */
    initialize: function () {
        TableMenusView.prototype.initialize.apply( this, arguments );
    },

    onRender: function () {
        var self = this;
        this.ui.filter.selectmenu( {
            change: function ( event, ui ) {
                self.model.set( 'filter', [ui.item.value] );
            }
        } );
    },

    /**
     * Drop the sources menu
     *
     * @param event
     */
    dropSources: function ( event ) {
        this.drop( this.ui.source, this.mapQuiddityMenu( this.model.getSources() ) );
    },

    /**
     * Drop the properties menu
     *
     * @param event
     */
    dropProperties: function ( event ) {
        //TODO: Show message when empty
        // Map for the menu structure
        this.drop( this.ui.properties, _.groupBy( _.map( this.model.getDestinations(), function ( property ) {
            return {
                group:   property.collection.quiddity.id,
                id:      property.id,
                label:   property.get( 'label' ),
                title:   property.get( 'label' ),
                tooltip: property.get( 'description' ),
                type:    'property'
            };
        }, this ), 'group' ) );
    },

    createControlDestination: function ( event ) {
        this.closeMenu();
        this.model.createPropertyDestination( $( event.currentTarget ).data( 'group' ), $( event.currentTarget ).data( 'id' ) );
    },

    dropCategories: function ( event, options = {} ) {
        var categories = _.uniq( _.map( this.model.scenic.quiddities.filter( function ( quiddity ) {
            return this.model.filterSource( quiddity );
        }, this ), function ( quiddity ) {
            return quiddity.get( 'classDescription' ).get( 'category' );
        } ) );

        // detect which filter is already selected
        var data = [];

        data.push( {'category': i18n.t('all categories') , 'selected': this.model.get( 'filter' ).length == 0 ? 'state-selected' : ''} );

        _.uniq( categories ).sort().forEach ( category => { 
            var selected = "";

            if( _.contains( this.model.get( 'filter' ), category ) ) {
                selected = "state-selected";
            }
            data.push( {'category': category , 'selected': selected} );
        })

        this.dropMultiSelect( this.ui.categories, data, null, options.modal );
    },
    /**
     * Filter table
     *
     * @param event
     */
    filter: function ( event ) {
        this.closeMenu();

        if( $( event.currentTarget ).data( 'id' ) == 'all categories' ) {
            this.model.set( 'filter', [] );
        } else {
            var filters = this.model.get( 'filter' );
            // remove filter if already present otherwise add it
            if (_.contains(filters, $( event.currentTarget ).data( 'id' ))) {
                filters = _.without(filters, $( event.currentTarget ).data( 'id' ));
            } else {
                filters.push($( event.currentTarget ).data( 'id' ));
            } 
            // TO FIX
            this.model.set( 'filter', [] );
            this.model.set( 'filter', filters);
        }
    }

} );

export default ControlMenusView;