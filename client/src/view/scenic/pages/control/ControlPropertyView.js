"use strict";

import _ from 'underscore';
import ConnectionsView from '../base/table/ConnectionsView';
import ControlConnectionView from './ControlConnectionView';
import ControlPropertyViewTemplate from './ControlPropertyView.html';

/**
 * Control Property View
 *
 * @constructor
 * @extends ConnectionsView
 */
const ControlPropertyView = ConnectionsView.extend( {
    template:           ControlPropertyViewTemplate,
    childView:          ControlConnectionView,
    childViewContainer: '.connections',
    className:          'source-child control-property',

    ui: {
        info:  '.actions .action.more',
        value: '.info .property-value',
        bar:   '.bar'
    },

    events: {
        'click @ui.info': 'showInfo'
    },

    modelEvents: {
        'change:value': 'updateValue'
    },

    templateHelpers: function () {
        return {
            percent: function () {
                if ( this.min != null && this.max != null ) {
                    return ( ( this.value - this.min ) / ( this.max - this.min ) ) * 100;
                } else {
                    return 0;
                }
            }
        }
    },

    /**
     * Initialize
     */
    initialize: function ( options ) {
        ConnectionsView.prototype.initialize.apply( this, arguments );
        this.collection = this.options.table.getDestinationCollection();
        this.options.source     = this.model
    },

    onRender: function () {
        this.updateValue();
    },

    /**
     * Update displayed value, done here as it is more performant than simply re-rendering
     */
    updateValue: function () {
        // Update textual value
        this.ui.value.html( this.model.get( 'value' ) );

        var percent = 0;
        if ( this.model.get( 'min' ) != null && this.model.get( 'max' ) != null ) {
            percent = ( ( this.model.get( 'value' ) - this.model.get( 'min' )) / ( this.model.get( 'max' ) - this.model.get( 'min' )) ) * 100;
        }
        this.ui.bar.css( 'width', percent + '%' );
    },

    /**
     * Show Info
     * @param event
     */
    showInfo: function ( event ) {
        //this.scenic.sessionChannel.commands.execute( 'shmdata:info', this.model );
    }
} );

export default ControlPropertyView;
