"use strict";

define( [
    'jquery',
    'underscore',
    'i18n',
    'backbone',
    'marionette',
    './Language.html'
], function ( $, _, i18n, Backbone, Marionette, LanguageTemplate ) {

    /**
     * Language
     *
     * @constructor
     * @extends module:Marionette.ItemView
     */

    var LanguageView = Marionette.ItemView.extend( {
        template:  LanguageTemplate ,
        className: "menu",
        ui: {
            'lang':   '.lang-btn'
        },

        events: {
            'click @ui.lang':   'changeLanguage'
        },

        /**
         * Initialize
         */
        initialize: function ( options ) {
            this.scenic = options.scenic;
        },

        onAttach: function () {
            $( ".lang-btn[data-lang='" + (localStorage.getItem( 'lang' ) || 'en') + "']" ).addClass( "active" );
        },
        
        changeLanguage: function ( event ) {
            this.scenic.sessionChannel.commands.execute( 'set:language', $( event.currentTarget ).data( 'lang' ) );
        }
    } );

    return LanguageView;
} );
