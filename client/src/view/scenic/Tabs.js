"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    './Tab'
], function ( _, Backbone, Marionette, TabView ) {

    /**
     * Tabs View
     *
     * @constructor
     * @extends module:Marionette.CollectionView
     */

    var TabsView = Marionette.CollectionView.extend( {
        childView: TabView,

        /**
         * Initialize
         */
        initialize: function () {

        },

        /**
         * Pages Filter
         *
         * @param page
         * @returns {boolean}
         */
        filter: function ( page ) {
            return page.get('enabled');
        }
    } );

    return TabsView;
} );
