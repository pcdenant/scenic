import Backbone from 'backbone';
import MessageView from './MessageView';
import $ from 'jquery';

const MessagesView = Backbone.View.extend({
    tagName:          'ul',
    className:        'messages',

    initialize: function() {

    },

    addMessage: function( message ) {
        var messageView = new MessageView({model: message});
        this.$el.append(messageView.render().el);

        if ( $('.messages')[0] ){
            $('.messages').scrollTop($('.messages')[0].scrollHeight);
        }
    }
} );

export default MessagesView;