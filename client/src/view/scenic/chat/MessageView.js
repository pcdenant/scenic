import Backbone from 'backbone';
import $ from 'jquery';

const MessagesView = Backbone.View.extend({
    tagName:          'li',

    initialize: function() {
        this.$el.append('<div class="content"><div class="username">'+ this.model.username + '</div> <div class="message">'+ this.model.mess +'</div></div>');
    }

} );

export default MessagesView;