"use strict";

import Marionette from 'marionette';
import ChatViewTemplate from './ChatsView.html';
import ChatView from './ChatView';

/**
 * Chats  View
 *
 * @constructor
 * @extends module:Marionette.CompositeView
 */
const ChatsView = Marionette.CompositeView.extend( {
    template:           ChatViewTemplate,
    className:          'chats-container',
    childViewContainer: '.chat-list',
    childView:          ChatView,
    
    ui: {
        list: '.chat-list'
    },

    childViewOptions: function () {
        return {
            scenic: this.scenic
        }
    },

    /**
     * Initialize
     */
    initialize: function ( options ) {
        this.scenic     = options.scenic;
        this.collection = this.model
    },

    onRender: function () {
    }

} );

export default ChatsView;