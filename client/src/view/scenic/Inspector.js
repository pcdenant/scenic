"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    './Inspector.html',
    'jquery-ui/ui/widgets/resizable'
], function ( _, Backbone, Marionette, InspectorTemplate ) {

    /**
     * Inspector View
     *
     * @constructor
     * @extends module:Marionette.LayoutView
     */
    var InspectorView = Marionette.LayoutView.extend( {
        template:     InspectorTemplate,
        getChildView: function ( item ) {
            return item.get( 'view' );
        },
        regions:      {
            container: '.panel-container'
        },

        /**
         * Initialize
         */
        initialize: function () {
            Marionette.LayoutView.prototype.initialize.apply( this, arguments );

            this.listenTo( this.options.inspectors, 'change:current', this.updateInspector.bind( this ) );
            this.options.scenic.sessionChannel.commands.setHandler( 'inspector:toggle', this.toggle.bind( this ) );
            this.options.scenic.sessionChannel.commands.setHandler( 'inspector:show', this.show.bind( this ) );
            this.options.scenic.sessionChannel.commands.setHandler( 'inspector:close', this.close.bind( this ) );
            this.options.scenic.sessionChannel.vent.on( 'file:reset', this.close.bind( this, true ) );
        },

        onBeforeShow: function () {
            this.updateInspector();
        },

        toggle: function ( params ) {
            var currentInspector = this.options.inspectors.findWhere( { active: true } );//getCurrentInspector();
            var inspector        = params.panel ? this.options.inspectors.get( params.panel ) : this.options.inspectors.get( params );
            if ( inspector && inspector != currentInspector ) {
                inspector.activate();
            } else {
                this.close();
            }
        },

        show: function ( id, options, saveState ) {
            var inspector = this.options.inspectors.get( id );
            if ( inspector ) {
                this.options.inspectors.setCurrentInspector( null );
                inspector.activate( options, saveState );
            }

        },

        updateInspector: function () {
            var inspector = this.options.inspectors.findWhere( { active: true } );
            if ( inspector ) {
                this.showChildView( 'container', inspector.getViewInstance() );
                this.options.scenic.sessionChannel.vent.trigger( 'inspector:opened', inspector.id );
            } else {
                this.getRegion( 'container' ).empty();
                this.options.scenic.sessionChannel.vent.trigger( 'inspector:closed' );
            }
        },

        close: function ( reset = false ) {
            var inspector = this.options.inspectors.findWhere( { active: true } );
            if ( inspector ) {
                inspector.set( 'currentOptions', null );
            }

            //FIXME: Until we have a selection system we'll just reset all inspector panels here
            if ( reset ) {
                this.options.inspectors.forEach( inspector => inspector.set( 'lastOptions', null ) );
            }
            
            this.options.inspectors.setCurrentInspector( null );
        }

    } );

    return InspectorView;
} );