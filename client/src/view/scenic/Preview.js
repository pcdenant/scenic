define( [
    'underscore',
    'backbone',
    'marionette',
    'view/scenic/preview/ShmdataPreview',
    './Preview.html',
    'jquery-ui/ui/widgets/draggable',
    'jquery-ui/ui/widgets/resizable'
], function ( _, Backbone, Marionette,
              ShmdataPreviewView,
              PreviewTemplate ) {

    /**
     *  @constructor
     *  @augments module:Marionette.ItemView
     */
    var Preview = Marionette.LayoutView.extend( {
        tagName:   'div',
        className: 'window-panel preview',
        template:   PreviewTemplate ,

        ui: {
            title:   '.title',
            close:   '.close',
            content: '.content'
        },

        events: {
            'click .close': 'close'
        },

        regions: {
            content: '.content'
        },

        childEvents: {
            'show': function () {
                this.ui.title.html( this.currentPanel.title );
            }
        },

        initialize: function ( options ) {
            this.scenic = options.scenic;

            this.scenic.sessionChannel.commands.setHandler( 'shmdata:preview', _.bind( this._onShmdataPreview, this ) );
            this.scenic.sessionChannel.commands.setHandler( 'preview:close', _.bind( this.close, this ) );
            this.scenic.sessionChannel.vent.on( 'file:reset', _.bind( this.close, this ) );
            this.scenic.sessionChannel.vent.on( 'file:loading', _.bind( this.close, this ) );
        },

        onShow: function() {
            var self = this;

            // Draggable
            this.$el.draggable( {
                cursor:      "move",
                //handle:      ".title",
                containment: ".page",
                stack:       ".window-panel",
                opacity:     0.75
            } ).resizable({
                 minWidth: 160,
                 minHeight: 120,
                 aspectRatio: false,
                 resize: function( event, ui ) {
                    self.onResize(ui);
                 }
             });
            // We need absolute at the start... to align right
            this.$el.css( 'position', '' );
        },

        close: function () {
            this.currentPanel = null;
            if ( this.getRegion( 'content' ) ) {
                this.getRegion( 'content' ).empty();
            }
            this.$el.fadeOut( 250 );
        },

        onResize: function(ui) {
            if ( this.currentPanel && this.currentPanel.onResize ) {
                var width  = ui.size.width;
                var height = ui.size.height - this.ui.content.position().top;
                this.currentPanel.onResize( width, height );
            }
        },
        
        resize: function(width, height) {
            this.$el.css('width', width);
            this.$el.css('height', height);
        },

        /**
         * Show Preview Panel for shmdata
         *
         * @param {Shmdata} shmdata
         * @private
         */
        _onShmdataPreview: function ( shmdata ) {
            this.currentPanel = new ShmdataPreviewView( { scenic: this.scenic, model: shmdata, initSize: _.bind( this.resize, this ) } );
            this.showChildView( 'content', this.currentPanel );
            this.$el.fadeIn( 250 );
        }

    } );

    return Preview;
} );