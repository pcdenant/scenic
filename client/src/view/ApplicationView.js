"use strict";

import $ from "jquery";
import _ from 'underscore';
import Backbone from 'backbone';
import { LayoutView } from 'marionette';
import ScenicView from 'view/ScenicView';
import ConfirmationView from 'view/scenic/modal/Confirmation';
import InformationView from 'view/scenic/modal/Information';
import ApplicationTemplate from './ApplicationView.html';

/**
 * Application View
 *
 * @constructor
 * @extends Marionette.LayoutView
 * @exports client/view/ApplicationView
 */
export default LayoutView.extend( {
    template: ApplicationTemplate,
    el:       '#scenic',

    ui: {
        modal: '#modal'
    },

    regions: {
        sessions: '#sessions',
        session:  '#session-container',
        modal:    '#modal'
    },

    keyShortcuts: {
        'esc': function () {
            // If we have a modal, don't close the inspector, let the modal handle the escape key if it wants
            if(!this.getRegion( 'modal' ).hasView() && this.currentSession) {
                this.currentSession.sessionChannel.commands.execute( 'inspector:close' );
            }
        }
    },

    initialize: function ( options ) {
        this.sessions = options.sessions;
        this.sessions.on( 'change:current', _.bind( this.showSession, this ) );

        // Wreqr Handlers
        this.scenicChannel = Backbone.Wreqr.radio.channel( 'scenic' );
        this.scenicChannel.commands.setHandler( 'confirm', this._onConfirm, this );
        this.scenicChannel.commands.setHandler( 'inform', this._onInform, this );

        // Tooltips
        $( document ).on( 'mouseover', e => {
            const closestWithTitle = $( e.target ).closest( '[title]' );
            if ( closestWithTitle.length ) {
                setImmediate( () => {
                    this.scenicChannel.vent.trigger( 'tooltip', {
                        title:   closestWithTitle.attr( 'title' ),
                        content: closestWithTitle.data( 'tooltip' )
                    } );
                }, 500 );
            }
        } );
    },

    /**
     * Render
     * Special case for the moment as we don't use a master application view to render us
     */
    onRender: function () {
        //MULTISESSION: this.showChildView( 'sessions', new SessionsView( { collection: this.sessions } ) );
        this.showSession( this.sessions.getCurrentSession() );
        this.$el.fadeIn( 500 );
    },

    /**
     * Current session change handler
     * Displays the current session
     *
     * @param {Session} session
     * @private
     */
    showSession: function ( session ) {
        if ( this.currentSession ) {
            this.$el.removeClass( this.currentSession.id );
        }
        if ( session ) {
            this.showChildView( 'session', new ScenicView( { model: session } ) );
            this.currentSession = session;
        } else {
            this.getRegion( 'session' ).empty();
            this.currentSession = null;
        }
        if ( this.currentSession ) {
            this.$el.addClass( this.currentSession.id );
        }
    },

    //  ███╗   ███╗ ██████╗ ██████╗  █████╗ ██╗
    //  ████╗ ████║██╔═══██╗██╔══██╗██╔══██╗██║
    //  ██╔████╔██║██║   ██║██║  ██║███████║██║
    //  ██║╚██╔╝██║██║   ██║██║  ██║██╔══██║██║
    //  ██║ ╚═╝ ██║╚██████╔╝██████╔╝██║  ██║███████╗
    //  ╚═╝     ╚═╝ ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚══════╝

    /**
     * Confirmation Handler
     * Shows a modal to confirm an action
     *
     * @param message
     * @param callback
     * @private
     */
    _onConfirm: function ( message, callback ) {
        if ( !callback ) {
            callback = message;
            message  = i18n.t( 'Are you sure?' );
        }
        this.$el.addClass( 'blur' );
        this.ui.modal.css( 'opacity', 1 );
        this.showChildView( 'modal', new ConfirmationView( {
            message:  message,
            callback: _.bind( this.closeModal, this, callback )
        } ) );
    },

    /**
     * Information Handler
     * Shows a modal to display an information
     *
     * @param message
     * @param callback
     * @private
     */
    _onInform: function ( message ) {
        this.$el.addClass( 'blur' );
        this.ui.modal.css( 'opacity', 1 );
        this.showChildView( 'modal', new InformationView( {
            message:  message,
            callback: _.bind( this.closeModal, this)
        } ) );
    },

    /**
     * Close Modal
     *
     * @param callback
     * @param result
     */
    closeModal: function ( callback, result ) {
        this.$el.removeClass( 'blur' );
        this.ui.modal.css( 'opacity', 0 );
        if ( callback ) {
            callback( result );
        }
        var self = this;
        setTimeout( function () {
            self.getRegion( 'modal' ).empty();
        }, 500 );
    }

} );