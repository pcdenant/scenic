| Touche        	| Description                              |
| ----------------- | ---------------------------------------- |
| (1-9)             | Sélection d'onglet				       |
| i                 | Information sur la ressource sélectionnée|
| s                 | Paramètres de la ressource sélectionnée  |
| c                 | Contacts SIP      			           |
| f                 | Déployer la fenêtre en plein écran	   |  
| a                 | Ajouter une ressource	           		   | 
| ctrl+s            | Enregistrer le fichier     	           |
| ctrl+shift+s      | Enregistrer sous...           	       |
| ctrl+o            | Ouvir un fichier    	           		   |
| ctrl+alt+n        | Créer un nouveau fichier           	   |
