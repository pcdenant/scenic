VERSION := $(shell node -p 'require("./package.json").version')

PREFIX := /usr/local
SCENICDIR := $(PREFIX)/share/scenic
ARCHIVE := scenic_$(VERSION)

LOCALES := public/locales
BUILD_DIR := build
DIST_DIR := dist
NODE_MODULES_BIN := node_modules/.bin
I18NEXT := $(NODE_MODULES_BIN)/i18next

.PHONY: doc build install uninstall clean dist test i18n

doc:
	@echo Compiling documentation
	$(NODE_MODULES_BIN)/jsdoc -c jsdoc-conf.json

build:
	@echo Cleaning previous build
	@rm -rf $(BUILD_DIR)
	@echo Updating Local NPM Dependencies
	npm install
	@echo Building Scenic
	@mkdir -p $(BUILD_DIR)
	npm run build
	@echo Installing dependencies
	@cp -r switcher $(BUILD_DIR)/switcher
	cd $(BUILD_DIR) && NODE_ENV=production npm install
	@rm -rf $(BUILD_DIR)/switcher

install:
	@echo Installing Scenic $(VERSION)
	@mkdir -p $(DESTDIR)$(SCENICDIR)
	@cp -r $(BUILD_DIR)/* $(DESTDIR)$(SCENICDIR)
	@echo Creating Run Script
	@echo "#!/bin/bash\ncd $(DESTDIR)$(SCENICDIR) && NODE_ENV=production node server.js \$$@" > $(DESTDIR)$(PREFIX)/bin/scenic
	@chmod +x $(DESTDIR)$(PREFIX)/bin/scenic
	@echo "#!/bin/bash\ncd $(DESTDIR)$(SCENICDIR) && SHELL=/bin/sh NODE_ENV=production gdb --args node server.js \$$@" > $(DESTDIR)$(PREFIX)/bin/scenic-gdb
	@chmod +x $(DESTDIR)$(PREFIX)/bin/scenic-gdb
	@echo Installing Launcher
	install -D scenic-launcher.desktop $(DESTDIR)/usr/share/applications/

install-mac:
	@echo Installing Scenic $(VERSION)
	@mkdir -p $(DESTDIR)$(SCENICDIR)
	@cp -r $(BUILD_DIR)/* $(DESTDIR)$(SCENICDIR)
	@echo Creating Run Script
	@echo "#!/bin/bash\ncd $(DESTDIR)$(SCENICDIR) && NODE_ENV=production node server.js \$$@" > $(DESTDIR)$(PREFIX)/bin/scenic
	@chmod +x $(DESTDIR)$(PREFIX)/bin/scenic
	@echo "#!/bin/bash\ncd $(DESTDIR)$(SCENICDIR) && NODE_ENV=production lldb -- node server.js \$$@" > $(DESTDIR)$(PREFIX)/bin/scenic-lldb
	@chmod +x $(DESTDIR)$(PREFIX)/bin/scenic-lldb

uninstall:
	@echo Uninstalling
	@echo removing $(DESTDIR)$(SCENICDIR)
	@rm -rf $(DESTDIR)$(SCENICDIR)
	@echo removed $(DESTDIR)$(SCENICDIR)
	@echo removing $(DESTDIR)$(PREFIX)/bin/scenic
	@rm $(DESTDIR)$(PREFIX)/bin/scenic
	@echo removed $(DESTDIR)$(PREFIX)/bin/scenic
	@echo removing $(DESTDIR)$(PREFIX)/bin/scenic-gdb
	@rm $(DESTDIR)$(PREFIX)/bin/scenic-gdb
	@echo removed $(DESTDIR)$(PREFIX)/bin/scenic-gdb
	@echo removing /usr/share/applications/scenic-launcher.desktop
	@rm /usr/share/applications/scenic-launcher.desktop
	@echo removed /usr/share/applications/scenic-launcher.desktop

clean:
	@echo Cleaning
	@echo removing $(BUILD_DIR)
	@rm -rf $(BUILD_DIR)
	@echo removed $(BUILD_DIR)
	@echo removing node_modules
	@rm -rf node_modules
	@echo removed node_modules

dist:
	@rm -rf $(DIST_DIR)/$(ARCHIVE)
	mkdir -p $(DIST_DIR)/$(ARCHIVE)$(SCENICDIR)
	cp -r $(BUILD_DIR)/* $(DIST_DIR)/$(ARCHIVE)$(SCENICDIR)
	@echo Creating Run Script
	mkdir -p $(DIST_DIR)/$(ARCHIVE)$(PREFIX)/bin
	@echo "#!/bin/bash\nNODE_ENV=production node $(DESTDIR)$(SCENICDIR)/server.js \$$@" > $(DIST_DIR)/$(ARCHIVE)$(PREFIX)/bin/scenic
	chmod +x $(DIST_DIR)/$(ARCHIVE)$(PREFIX)/bin/scenic
	@echo Installing Launcher
	mkdir -p $(DIST_DIR)/$(ARCHIVE)$(PREFIX)/share/applications
	install -D scenic-launcher.desktop $(DIST_DIR)/$(ARCHIVE)$(PREFIX)/share/applications/
	@echo Creating Debian metadata
	mkdir -p $(DIST_DIR)/$(ARCHIVE)/DEBIAN
	@echo "Package: scenic" > $(DIST_DIR)/$(ARCHIVE)/DEBIAN/control
	@echo "Version: $(VERSION)" >> $(DIST_DIR)/$(ARCHIVE)/DEBIAN/control
	@echo "Section: misc" >> $(DIST_DIR)/$(ARCHIVE)/DEBIAN/control
	@echo "Priority: optional" >> $(DIST_DIR)/$(ARCHIVE)/DEBIAN/control
	@echo "Architecture: all" >> $(DIST_DIR)/$(ARCHIVE)/DEBIAN/control
	@echo "Depends: libshmdata, switcher, nodejs-legacy, npm" >> $(DIST_DIR)/$(ARCHIVE)/DEBIAN/control
	@echo "Maintainer: Metalab <metalab@sat.qc.ca>" >> $(DIST_DIR)/$(ARCHIVE)/DEBIAN/control
	@echo "Description: Scenic" >> $(DIST_DIR)/$(ARCHIVE)/DEBIAN/control
	@echo Creating post install script
	@echo "cd $(DESTDIR)$(SCENICDIR) && npm update" >> $(DIST_DIR)/$(ARCHIVE)/DEBIAN/postinst
	chmod 755 $(DIST_DIR)/$(ARCHIVE)/DEBIAN/postinst
	dpkg-deb --build $(DIST_DIR)/$(ARCHIVE)
	#@cd $(DIST_DIR) && tar czf $(ARCHIVE).tar.gz $(ARCHIVE)
	@echo $(ARCHIVE).orig.tar.gz is done!

test:
	npm test
	#$(NODE_MODULES_BIN)/mocha --recursive server/test

i18n:
	#@echo Extracting switcher strings
	#node tools/extract_switcher_i18n.js -o .tmp/switcher_strings.js
	#@echo Parsing switcher strings
	#$(I18NEXT) .tmp -o $(LOCALES) -l fr --fileFilter 'switcher_strings.js' -f '$$.t,$$.i18n.t,i18n.t,data-i18n' -r -n switcher -k :: -s :::
	#@echo Removing temporary strings file
	#rm .tmp/switcher_strings.js
	@echo Parsing server strings
	$(I18NEXT) server -o $(LOCALES) -l fr --directoryFilter '!test' --fileFilter '*.js,*.html' -f '$$.t,$$.i18n.t,i18n.t,data-i18n' -r -n server -k :: -s :::
	@echo Parsing client strings
	$(I18NEXT) client -o $(LOCALES) -l fr --directoryFilter '!.sass-cache, !assets, !css, !scss, !test' --fileFilter '*.js,*.html' -f '$$.t,$$.i18n.t,i18n.t,data-i18n' -r -n client -k :: -s :::
