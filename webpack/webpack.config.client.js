var webpack           = require( 'webpack' );
var ExtractTextPlugin = require( "extract-text-webpack-plugin" );
var path              = require( 'path' );
var fs                = require( 'fs' );

var isDev    = process.env.NODE_ENV !== 'production';
var rootPath = path.dirname( __dirname );

module.exports = {
    devtool:     'source-map',
    entry:       [
        path.join( rootPath, 'client/src/main.js' )
    ],
    output:      {
        path:       path.join( rootPath, 'build' ),
        filename:   'main.js',
        publicPath: '/'
    },
    recordsPath: path.join( rootPath, '.webpack/_records' ),
    resolve:     {
        modulesDirectories: [
            'client/src',
            'node_modules'
        ],
        alias:              {
            i18n:       'i18next-client/i18next.amd.withJQuery',
            marionette: 'backbone.marionette'
        }
    },
    module:      {
        loaders: [
            // Shims
            { test: /jquery\.ui\.touch-punch/, loader: 'imports?jQuery=jquery' },
            { test: /backbone\.mutators/, loader: 'imports?underscore&backbone' },
            { test: /underscore/, loader: 'exports?window._,_' },
            // Babel
            {
                test:    /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader:  'babel'
            },
            // File Formats
            { test: /\.html/, loader: 'ejs' },
            { test: /\.scss$/, loader: ExtractTextPlugin.extract( 'style', 'css?sourceMap!resolve-url?sourceMap!sass?sourceMap' ) },
            { test: /\.css$/, loader: ExtractTextPlugin.extract( 'style', 'css?sourceMap' ) },
            { test: /\.png$/, loader: 'url-loader?limit=4&context=public&name=[path][name].[ext]' },
            { test: /\.jpg$/, loader: 'file-loader?context=public&name=[path][name].[ext]' },
            { test: /\.(woff|woff2|eot|ttf|svg)$/, loader: 'file-loader?context=public&name=[path][name].[ext]' },
            { test: /\.md$/, loader: "html!markdown?gfm=true" }

        ]
    },
    plugins:     [
        new require( 'progress-bar-webpack-plugin' )(),
        new ExtractTextPlugin( "style.css", { allChunks: true, disable: isDev } ),
        //
        new webpack.ProvidePlugin({
            _: "underscore" // Required by the ejs-loader
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.NoErrorsPlugin(),
        new webpack.DefinePlugin( {
            __CLIENT__:             true,
            __SERVER__:             false,
            'process.env.NODE_ENV': JSON.stringify( isDev ? 'development' : 'production' )
        } )
    ]
}
;