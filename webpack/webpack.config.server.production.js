var webpack = require( 'webpack' );
var path    = require( 'path' );

var rootPath = path.dirname( __dirname );

var config = require( './webpack.config.server' );

config.output.path = path.join(rootPath, 'build');
config.plugins = config.plugins.concat(
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin()
);

module.exports = config;