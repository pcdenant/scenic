var webpack           = require( 'webpack' );
var path              = require( 'path' );

var rootPath = path.dirname( __dirname );

var config = require( './webpack.config.client' );

config.output.path = path.join( rootPath, 'build/public' );
config.plugins     = config.plugins.concat(
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin()
);

module.exports = config;