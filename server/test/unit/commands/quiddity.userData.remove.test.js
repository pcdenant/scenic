var _         = require( 'underscore' );
var chai      = require( 'chai' );
var sinon     = require( 'sinon' );
var sinonChai = require( 'sinon-chai' );
var should    = chai.should();
var expect    = chai.expect;
chai.use( sinonChai );

var quiddities = require( '../../fixtures/quiddities' );

describe( 'Remove User Data Command', function () {

    var client;
    var command;
    var cb;

    beforeEach( function () {
        command = require( '../../../src/net/commands/quiddity.userData.remove' );

        client  = {
            switcherController: {
                quiddityManager: {
                    removeUserData: sinon.stub()
                }
            }
        };
        command = command.execute.bind( client );
        cb      = sinon.stub();
    } );

    afterEach( function () {
        cb.should.have.been.calledOnce;
    } );

    it( 'should return nothing when successful', function () {
        client.switcherController.quiddityManager.removeUserData.returns( true );
        command( 'quidd', 'path', cb );
        client.switcherController.quiddityManager.removeUserData.should.have.been.calledOnce;
        client.switcherController.quiddityManager.removeUserData.should.have.been.calledWithExactly( 'quidd', 'path' );
        cb.should.have.been.calledWithExactly();
    } );

    it( 'should return an error when manager throws', function () {
        client.switcherController.quiddityManager.removeUserData.throws();
        command( 'quidd', 'path', cb );
        client.switcherController.quiddityManager.removeUserData.should.have.been.calledOnce;
        client.switcherController.quiddityManager.removeUserData.should.have.been.calledWithExactly( 'quidd', 'path' );
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when failing to set userData', function () {
        client.switcherController.quiddityManager.removeUserData.returns( false );
        command( 'quidd', 'path', cb );
        client.switcherController.quiddityManager.removeUserData.should.have.been.calledOnce;
        client.switcherController.quiddityManager.removeUserData.should.have.been.calledWithExactly( 'quidd', 'path' );
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when quiddity parameter is empty', function () {
        command( '', 'path', cb );
        client.switcherController.quiddityManager.removeUserData.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when quiddity parameter is null', function () {
        command( null, 'path', cb );
        client.switcherController.quiddityManager.removeUserData.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when quiddity parameter is a number', function () {
        command( 666, 'path', cb );
        client.switcherController.quiddityManager.removeUserData.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when quiddity parameter is not a string', function () {
        command( ['not a string'], 'path', cb );
        client.switcherController.quiddityManager.removeUserData.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when path is a number', function () {
        command( 'quidd', 666, cb );
        client.switcherController.quiddityManager.removeUserData.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when path is not a string', function () {
        command( 'quidd', ['not a string'], cb );
        client.switcherController.quiddityManager.removeUserData.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );
} );