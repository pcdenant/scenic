var async      = require( 'async' );
var chai       = require( "chai" );
var sinon      = require( "sinon" );
var sinonChai  = require( "sinon-chai" );
var should     = chai.should();
chai.use( sinonChai );

var SwitcherAddon = require( 'switcher' );

describe( 'Switcher Addon', function () {

    var log;
    var switcher;

    beforeEach( function () {
        log      = sinon.stub();
        switcher = new SwitcherAddon.Switcher( 'test', log );
    } );

    afterEach( function () {
        switcher.close();
    } );

    describe( 'Initialization', function () {

        it( 'should have initialized correctly', function () {
            switcher.has_quiddity( 'internalLogger' ).should.be.true;
            var logger = switcher.get_quiddity_description( 'internalLogger' );
            should.exist( logger );
            logger.class.should.equal( 'logger' );
        } )

    } );

    describe( 'Callbacks', function () {

        describe( 'Logger', function () {

            it( 'should receive logging info', function () {
                // Logs are sent at creation so nothing to do here, just check if we received something
                log.should.have.been.called;
            } );

        } );

        describe( 'Properties', function () {

            it( 'should receive changed property callbacks', function ( done ) {
                var dummy = switcher.create( 'dummy' );
                should.exist(dummy);
                switcher.subscribe_to_property( dummy, 'bool_' );
                switcher.register_prop_callback( function ( quiddity, property, value ) {
                    quiddity.should.equal( dummy );
                    property.should.equal( 'bool_' );
                    value.should.be.true;
                    done();
                } );
                switcher.set_property_value( dummy, 'bool_', String( true ) );
            } );

        } );

        describe( 'Signals', function () {
            this.timeout(15000);
            it( 'should receive quiddity creation callbacks', function ( done ) {
                var dummy = switcher.create( 'dummy' );
                switcher.register_signal_callback( function ( quiddity, signal, value ) {
                    signal.should.equal( 'on-quiddity-created' );
                    value.should.eql( [dummy] );
                    done();
                } );
            } );

            it( 'should receive quiddity removal callbacks', function ( done ) {
                var created = false;
                var dummy   = switcher.create( 'dummy' );
                switcher.remove( dummy );
                switcher.register_signal_callback( function ( quiddity, signal, value ) {
                    if ( !created ) {
                        signal.should.equal( 'on-quiddity-created' );
                        value.should.eql( [dummy] );
                        created = true;
                    } else {
                        signal.should.equal( 'on-quiddity-removed' );
                        value.should.eql( [dummy] );
                        done();
                    }
                } );
            } );

            it( 'should receive tree grafted callbacks', function ( done ) {
                // setTimeout(done, 5000);
                var created = false;
                var signalQuid    = switcher.create( 'signal' );

                switcher.subscribe_to_signal( signalQuid, 'on-tree-grafted' );
                switcher.invoke( signalQuid, 'emit-signal', [] );

                switcher.register_signal_callback( function ( quiddity, signal, value ) {
                    if ( !created ) {
                        created = true; // Already tested
                    } else {
                        quiddity.should.equal( signalQuid );
                        signal.should.equal( 'on-tree-grafted' );
                        done();
                    }
                } );

            } );

            it( 'should receive tree pruned callbacks', function ( done ) {
                // setTimeout(done, 5000);
                var created = false;
                var signalQuid    = switcher.create( 'signal' );

                switcher.subscribe_to_signal( signalQuid, 'on-tree-pruned' );
                switcher.invoke( signalQuid, 'emit-signal', [] );
                switcher.invoke( signalQuid, 'remove', [] );

                switcher.register_signal_callback( function ( quiddity, signal, value ) {
                    if ( !created ) {
                        created = true; // Already tested
                    } else {
                        quiddity.should.equal( signalQuid );
                        signal.should.equal( 'on-tree-pruned' );
                        done();
                    }
                } );
            } );

        } );

    } );
} );