var chai      = require( "chai" );
var sinon     = require( "sinon" );
var sinonChai = require( "sinon-chai" );
var should    = chai.should();
chai.use( sinonChai );

var Switcher = function () {
    this.register_prop_callback             = sinon.stub();
    this.register_signal_callback           = sinon.stub();
    this.subscribe_to_signal                = sinon.stub();
    this.subscribe_to_property              = sinon.stub();
    this.create                             = sinon.stub();
    this.remove                             = sinon.stub();
    this.set_property_value                 = sinon.stub();
    this.invoke                             = sinon.stub();
    this.load_defaults                      = sinon.stub();
    this.reset_command_history              = sinon.stub();
    this.set_user_data                      = sinon.stub();
    this.get_user_data                      = sinon.stub();
    this.get_nickname                       = sinon.stub();
    this.set_nickname                       = sinon.stub();
    this.load_history_from_scratch          = sinon.stub();
    this.load_history_from_current_state    = sinon.stub();
    this.close                              = sinon.stub();
    this.save_history                       = sinon.stub();
    this.reset_history                      = sinon.stub();
    this.set_user_data_branch               = sinon.stub();
    this.remove_user_data                   = sinon.stub();
    this.get_quiddity_description           = sinon.stub();
    this.get_property_value                 = sinon.stub();
    this.get_info                           = sinon.stub();
    this.get_classes_doc                    = sinon.stub();
    this.get_quiddities_description         = sinon.stub();
    this.get_methods_description            = sinon.stub();
    this.get_method_description             = sinon.stub();
    this.has_quiddity                       = sinon.stub();
};

module.exports = { Switcher: Switcher };