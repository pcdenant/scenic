'use strict';

const _         = require( 'underscore' );
const log       = require( '../lib/logger' );
const fs        = require('fs');

var LocalStrategy   = require('passport-local').Strategy;
var bcrypt          = require('bcrypt');

const authDBPath    = process.env.HOME + "/.scenic/users.json";

module.exports = passport => {

    // used to serialize the user for the session
    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser((id, done) => {
        var exists   = fs.existsSync( authDBPath );
        if ( exists ) {
            try {
                var authDB = JSON.parse( fs.readFileSync( authDBPath ) );
                if ( authDB.isEnabled ) {
                    if( _.findWhere(authDB.users , {id:id}) ){
                        return done(null, id)
                    } else {
                        return done({});
                    }
                }
            } catch ( e ) {
                log.warn( 'Error reading users file', e);
            }
        }
    });

    // callback with username and password from signup 
    passport.use('local-signup', new LocalStrategy({
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true
    },
    (req, username, password, done) => {

        var exists   = fs.existsSync( authDBPath );
        if ( exists ) {
            try {
                // get the user if he exists
                var authDB = JSON.parse( fs.readFileSync( authDBPath ) );
                var user = _.find(authDB.users , user => {
                    return user.username == username && bcrypt.compareSync(password, user.password);
                });

                if ( user ) {
                    done(null, false, { message: 'User already exists.'});
                } else {
                    // otherwise create a new one
                    var newUser = {username: username, password: bcrypt.hashSync(password, bcrypt.genSaltSync(8), null), id:guid()};
                    authDB.users.push(newUser);

                    fs.writeFile(authDBPath, JSON.stringify(authDB), err => {
                        if(err) {
                            return log.warn( 'Error reading users file', err);
                        }
                        done(null, newUser);
                    }); 
                }
            } catch ( e ) {
                log.warn( 'Error reading users file', e);
            }
        } else {
            // no file, create the json from scratch
            var newUser = {username: username, password: bcrypt.hashSync(password, bcrypt.genSaltSync(8), null), id:guid()};
            var authDB = { "isEnabled": true, "users": [newUser]};

            fs.writeFile(authDBPath, JSON.stringify(authDB), err => {
                if(err) {
                    return log.warn( 'Error reading users file', err);
                }
                done(null, newUser);           
            });          
        }
    }));

    // callback with username and password from login form
    passport.use('local-login', new LocalStrategy({
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true
    },
    (req, username, password, done) => {
        var exists   = fs.existsSync( authDBPath );
        if ( exists ) {
            try {
                var authDB = JSON.parse( fs.readFileSync( authDBPath ) );
                var users = _.where(authDB.users , {username: username});

                if ( users.length > 0 ) {
                    var user = _.find(users , user => { 
                        return bcrypt.compareSync(password, user.password);
                    });

                    if ( user ) {
                        req.login(user, function(err) {
                            if (err) return next(err);
                            return done(null, user);
                        });
                    } else {
                        done(null, false, { message: 'Wrong password'});
                    }
                } else {    
                    return done(null, false, { message: 'No user found.'});
                }
            } catch ( e ) {
                return done(null, false, { message: 'No user found.'});
            }
        }
    }));

};
// generate a guide
function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}