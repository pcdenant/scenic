'use strict';

const _       = require( 'underscore' );
const path    = require( 'path' );
const i18next = require( 'i18next' );
const express = require( 'express' );
const fs      = require( 'fs' );
const pp      = require( 'preprocess' );
const log     = require( '../lib/logger' );

const config     = require( '../settings/config' );
const authDBPath = process.env.HOME + "/.scenic/users.json";
/**
 * Express routes for the application
 * We use different routes in development and production since in production a lot of the resources are built and
 * minified in a single directory.
 *
 * @module server/controller/routes
 */
module.exports = ( app, passport, config, serverPath ) => {
    app.use( '/', express.static( './public' ) );
    app.set( 'views', './public/views' );

    /**
     * POST Login
     * Process the login
     */
    app.post( '/login', passport.authenticate( 'local-login' ), ( req, res ) => {
        log.info( 'login' );
        res.status( 200 ).json( { login: true } );
    } );

    /**
     * GET enabled
     * process reset the password ( only if the request is authenticated)
     */
    app.get( '/enable', ( req, res ) => {
        var exists = fs.existsSync( authDBPath );
        if ( exists ) {
            try {
                var authDB = JSON.parse( fs.readFileSync( authDBPath ) );

                if ( !authDB.isEnabled ) {
                    return res.status( 200 ).json( { authenticate: false, nbUser: authDB.users.length } );
                } else {
                    return res.status( 200 ).json( { authenticate: true, nbUser: authDB.users.length } );
                }
            } catch ( e ) {
                return res.status( 500 ).json( { error: "Database error" } );
            }
        } else {
            return res.status( 200 ).json( { authenticate: false, nbUser: 0 } );
        }
    } );

    /**
     * GET needlogin
     * Detect if we need to login
     */
    app.get( '/needlogin', ( req, res ) => {
        var exists = fs.existsSync( authDBPath );
        if ( exists ) {
            try {
                var authDB = JSON.parse( fs.readFileSync( authDBPath ) );

                if ( req.isAuthenticated()
                     || !authDB.isEnabled
                     || authDB.users.length == 0
                     || req.connection.remoteAddress == "::1"
                     || req.connection.remoteAddress.replace( /^.*:/, '' ) == config.host ) {

                    return res.status( 200 ).json( { authenticate: false } );
                } else {
                    return res.status( 200 ).json( { authenticate: true } );
                }
            } catch ( e ) {
                return res.status( 500 ).json( { error: "Database error" } );
            }
        } else {
            return res.status( 200 ).json( { authenticate: false } );
        }
    } );

    /**
     * POST enable
     * enable or disable the Authentication
     */
    app.post( '/enable', ( req, res ) => {
        var exists = fs.existsSync( authDBPath );
        if ( exists ) {
            try {
                var authDB = JSON.parse( fs.readFileSync( authDBPath ) );
                if ( req.body.enable != authDB.isEnabled ) {
                    authDB.isEnabled = req.body.enable;
                    fs.writeFile( authDBPath, JSON.stringify( authDB ), err => {
                        if ( err ) {
                            return res.status( 500 ).json( { error: "Database error" } );
                        }
                        return res.status( 200 ).json( { enable: req.body.enable } );
                    } );
                }
            } catch ( e ) {
                return res.status( 500 ).json( { error: "Database error" } );
            }
        } else {
            var authDB = { "isEnabled": req.body.enable, "users": [] };
            fs.writeFile( authDBPath, JSON.stringify( authDB ), err => {
                if ( err ) {
                    log.warn( 'authenticate enable Database error', err )
                    return res.status( 500 ).json( { error: "Database error" } );
                    ;
                }
                return res.status( 200 ).json( { enable: req.body.enable } );
            } );
        }
    } );

    /**
     * GET reset
     * Reset the password ( only if the request is authenticated)
     */
    app.get( '/reset', ( req, res ) => {
        if ( req.isAuthenticated()
             || req.connection.remoteAddress == "::1"
             || req.connection.remoteAddress.replace( /^.*:/, '' ) == config.host ) {

            req.logout();
            var exists = fs.existsSync( authDBPath );
            if ( exists ) {
                try {
                    var authDB   = JSON.parse( fs.readFileSync( authDBPath ) );
                    authDB.users = [];
                    fs.writeFile( authDBPath, JSON.stringify( authDB ), err => {
                        if ( err ) {
                            return res.status( 500 ).json( { error: "Database error" } );
                        }
                        return res.status( 200 ).json( { reset: true } );
                    } );
                } catch ( e ) {
                    return res.status( 500 ).json( { error: "Database error" } );
                }
            }
        }
    } );

    /**
     * POST Signup
     * process the signup form
     */
    app.post( '/signup', passport.authenticate( 'local-signup' ), ( req, res ) => {
        res.status( 200 ).json( { signup: true } );
    } );

    /**
     * Protected root route
     *
     */
    app.get( '/', isLoggedIn, ( req, res ) => {
        const indexFile = require( '../templates/index.html' );
        res.send( pp.preprocess( indexFile ) );
    } );

    /**
     * Logout ( not used for now )
     *
     */
    app.get( '/logout', ( req, res ) => {
        req.logout();
        res.redirect( '/' );
    } );

    // route to make sure a user is logged in
    function isLoggedIn( req, res, next ) {
        // if the request came from the localhost no Authentication
        if ( req.connection.remoteAddress == "::1"
             || req.connection.remoteAddress.replace( /^.*:/, '' ) == config.host ) {

            return next();
        } else {
            var exists = fs.existsSync( authDBPath );
            if ( exists ) {
                try {
                    var authDB = JSON.parse( fs.readFileSync( authDBPath ) );

                    // if user is authenticated in the session
                    // or the Authentication is not enabled
                    // or there is no users in the json file
                    if ( req.isAuthenticated() || !authDB.isEnabled || authDB.users.length == 0 ) {
                        return next();
                    }

                    res.send( pp.preprocess( require( '../templates/index.html' ) ) );
                } catch ( e ) {
                    res.send( pp.preprocess( require( '../templates/index.html' ) ) );
                }
            } else {
                return next();
            }
        }
    }
};

