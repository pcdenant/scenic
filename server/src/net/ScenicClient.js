"use strict";

const _    = require( 'underscore' );
const i18n = require( 'i18next' );
const log  = require( '../lib/logger' );

//const requireDir = require( 'require-dir' );
//const commands   = requireDir( 'commands', { recurse: true } );
const reqCmd   = require.context( './commands', true, /^\.\// );
const commands = reqCmd.keys()
                       .filter( key => key.indexOf( '.js' ) == -1 )
                       .map( key => ({
                           key:     key.replace( './', '' ),
                           command: reqCmd( key )
                       }) );

/**
 * Master Socket Id
 */
var masterSocketId;

/**
 * Refresh timeout
 */
var refreshTimeout;

/**
 * Scenic Client
 */
class ScenicClient {

    /**
     * Create a Scenic Client Instance
     *
     * @param {SwitcherController} switcherController
     * @param {Object} config Scenic configuration object
     * @param {Object} socket Client socket
     */
    constructor( switcherController, config, socket ) {
        this.switcherController = switcherController;
        this.config             = config;
        this.socket             = socket;

        this.subscriptions        = {};
        this.previewSubscriptions = {};

        this.bindCommands( commands );
        this.switcherController.addClient( this );
    }

    /**
     * Remove this client instance
     */
    destroy() {
        log.debug( 'Destroying client instance' );

        const sub = this.subscriptions[this.config.timelapse.preview.quiddName];
        if ( sub ) {
            this.switcherController.quiddityManager.disconnectPreview( sub.shmdata );
        }
        this.switcherController.removeClient( this );
        this.switcherController.ircManager.deleteClient();
        this.socket.removeAllListeners();
        this.switcherController = null;
        this.config             = null;
        this.socket             = null;
        this.subscriptions      = null;
    };

    /**
     * Binds a list of command modules to the socket instance
     *
     * @param {Object} commands - List of command modules
     */
    bindCommands( commands ) {
        // Bind commands to client socket
        commands.forEach( cmd => {
            var fn;
            if ( cmd.command.execute && _.isFunction( cmd.command.execute ) ) {
                fn = cmd.command.execute;
            } else if ( _.isFunction( cmd.command ) ) {
                fn = cmd.command;
            }
            if ( fn ) {
                this.socket.on( cmd.command.name ? cmd.command.name : cmd.key, fn.bind( this ) );
            }
        } );
    };

    /**
     * Get the master socket id
     *
     * @getter
     * @static ish
     * @returns {string} - The master socket id, if any
     */
    getMasterSocketId() {
        return masterSocketId;
    };

    /**
     * Set the master socket id
     *
     * @setter
     * @static ish
     * @param {string} socketId - The master socket id
     */
    setMasterSocketId( socketId ) {
        masterSocketId = socketId;
    };

    /**
     * Set the static refresh timeout
     *
     * @param timeout
     */
    setRefreshTimeout( timeout ) {
        refreshTimeout = timeout;
    };

    /**
     * Get the static refresh timeout
     *
     * @return timeout
     */
    getRefreshTimeout() {
        return refreshTimeout;
    };

}

module.exports = ScenicClient;