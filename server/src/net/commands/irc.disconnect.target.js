"use strict";

const _    = require( 'underscore' );
const i18n = require( 'i18next' );

/**
 * @module server/net/commands/irc/disconnect/target
 */
module.exports = {

    /**
     * Irc create command
     *
     * @param {string}  username
     */

    execute: function ( target ) {
        if ( _.isEmpty( target ) ) {
            return cb( i18n.t( 'Missing target parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( target ) ) {
            return cb( i18n.t( 'Invalid target (__target__)', {
                lng:  this.lang
            } ) );
        }
        this.switcherController.ircManager.disconnectClient( target );
    }
};