"use strict";

const i18n = require( 'i18next' );

/**
 * @module server/net/commands/file/list
 */
module.exports = {

    /**
     * Get file list command
     *
     * @param {Function} cb Callback
     */
    execute: function ( name, cb ) {

        this.switcherController.getFile( name,  ( error, data ) => {
            if ( error ) {
                cb( i18n.t( 'An error occurred while retrieving the file (__error__)', {
                    lng: this.lang,
                    error: error.toString()
                } ) );
            } else {
                cb( null, data );
            }
        } );
    }
};