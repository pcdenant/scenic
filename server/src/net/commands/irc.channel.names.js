"use strict";

const _    = require( 'underscore' );
const i18n = require( 'i18next' );

/**
 * @module server/net/commands/irc/channel/names
 */
module.exports = {

    /**
     * Irc create command
     *
     * @param {string}  username
     */

    execute: function ( channelName ) {
        if ( _.isEmpty( channelName ) ) {
            return cb( i18n.t( 'Missing channelName parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( channelName ) ) {
            return cb( i18n.t( 'Invalid name (__channelName__)', {
                lng:  this.lang
            } ) );
        }

        this.switcherController.ircManager.getNicknames( channelName );
    }
};