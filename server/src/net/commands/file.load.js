"use strict";

const _ = require('underscore');
const i18n = require( 'i18next' );

/**
 * @module server/net/commands/file/load
 */
module.exports = {

    /**
     * Load save file command
     *
     * @param {string} file Name of the file to load
     * @param {Function} cb Callback
     */
    execute: function ( file, cb ) {
        if ( _.isEmpty( file ) ) {
            return cb( i18n.t( 'Missing file name parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( file ) ) {
            return cb( i18n.t( 'Invalid file name (__file__)', {
                lng: this.lang,
                file: file
            } ) );
        }

        var self = this;
        this.switcherController.loadFile( file, function( error, success ) {

            if ( error ) {
                return cb( i18n.t('An error occurred while loading file __file__ (__error__)', {
                    lng: self.lang,
                    file: file,
                    error: error.toString()
                }));
            }

            if ( !success ) {
                return cb( i18n.t('Could not load file __file__', {
                    lng: self.lang,
                    file: file
                }));
            }

            cb();
        } );
    }
};