"use strict";

/**
 * @module server/net/commands/preview/thumbnails/subscribe
 */
module.exports = {

    /**
     * Subscribe to the preview thumbnails
     */
    execute: function() {
        this.previewSubscriptions[this.config.timelapse.thumbnail.quiddName] = true;
    }
};