"use strict";

const i18n = require( 'i18next' );

/**
 * @module server/net/commands/sip/contact/block
 */
module.exports = {

    /**
     * Block all contacts except ones tha are in a call
     *
     * @param {Function} cb Callback
     */
    execute: function ( cb ) {
        try {
            var contacts = this.switcherController.sipManager.blockAllContact();
        } catch ( e ) {
            return cb( i18n.t( 'An error occurred while blocking contacts (__error__)', {
                lng:   this.lang,
                error: e.toString()
            } ) );
        }
        cb( null, contacts ? contacts : [] );
    }
};