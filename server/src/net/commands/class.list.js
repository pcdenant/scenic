"use strict";

const i18n = require('i18next');

/**
 * @module server/net/commands/class/list
 */
module.exports = {
    /**
     * Get quiddity classes list
     *
     * @param {Function} cb Callback
     */
    execute: function( cb ) {
        try {
            var classes = this.switcherController.quiddityManager.getQuiddityClasses();
        } catch ( e ) {
            return cb( i18n.t('An error occurred while getting quiddity classes (__error__)', {
                lng: this.lang,
                error: e.toString()
            } ) );
        }
        cb( null, classes );
    }
};