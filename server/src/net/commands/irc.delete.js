"use strict";

const _    = require( 'underscore' );
const i18n = require( 'i18next' );

/**
 * @module server/net/commands/irc/delete
 */
module.exports = {

    /**
     * Irc create command
     *
     * @param {string}  username
     */

    execute: function (  ) {
        this.switcherController.ircManager.deleteClient();
    }
};