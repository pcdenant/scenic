"use strict";

const i18n = require( 'i18next' );

/**
 * @module server/net/commands/sip/contact/noBother
 */
module.exports = {

    /**
     * Detect if the self contact is in a not bother mode.
     *
     * @param {Function} cb Callback
     */
    execute: function ( cb ) {
        try {
            var result = this.switcherController.sipManager.isInNotBotherMode();
        } catch ( e ) {
            return cb( i18n.t( 'An error occurred (__error__)', {
                lng:   this.lang,
                error: e.toString()
            } ) );
        }
        cb( null, result );
    }
};