"use strict";

const _ = require('underscore');
const i18n = require( 'i18next' );

/**
 * @module server/net/commands/file/reset
 */
module.exports = {

    /**
     * Reset save file command
     *
     * @param {Function} cb Callback
     */
    execute: function ( cb ) {
        var self = this;
        this.switcherController.reset( function( error, success ) {

            if ( error ) {
                return cb( i18n.t('An error occurred while resetting session (__error__)', {
                    lng: self.lang,
                    error: error.toString()
                }));
            }

            if ( !success ) {
                return cb( i18n.t('Could not reset session', {
                    lng: self.lang
                }));
            }

            cb();
        } );
    }
};