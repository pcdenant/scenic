"use strict";

/**
 * @module server/net/commands/preview/subscribe
 */
module.exports = {

    /**
     * Subscribe to the preview of a shmdate
     * @param {string} shmdata
     */
    execute: function(shmdata) {
        this.switcherController.quiddityManager.connectPreview(shmdata);
        
        // Subscribe to the shmdata name without the "/tmp/"
        this.previewSubscriptions[this.config.timelapse.preview.quiddName] = {
            shmdata: shmdata.substring( shmdata.lastIndexOf( '/' ) + 1 )
        };
    }
};