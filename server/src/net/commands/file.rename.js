"use strict";

const _    = require( 'underscore' );
const i18n = require( 'i18next' );
const log  = require( '../../lib/logger' );

/**
 * @module server/net/commands/file/rename
 */
module.exports = {

    /**
     * Rename file command
     *
     * @param {string} Name of the file to rename
     * @param {string} New name
     * @param {Function} cb Callback
     */
    execute: function ( oldFile, newFile, cb ) {

        if ( _.isEmpty( oldFile ) ) {
            return cb( i18n.t( 'Missing file name parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( oldFile ) ) {
            return cb( i18n.t( 'Invalid file name (__oldFile__)', {
                lng:  this.lang,
                file: oldFile
            } ) );
        }

        if ( _.isEmpty( newFile ) ) {
            return cb( i18n.t( 'Missing new file name parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( newFile ) ) {
            return cb( i18n.t( 'Invalid new file name (__newFile__)', {
                lng:  this.lang,
                file: oldFile
            } ) );
        }

        var self = this;
        this.switcherController.renameFile(  oldFile, newFile, function( error, success ) {
            if ( error ) {
                return cb( i18n.t('An error occurred while renaiming file __oldFile__ (__error__)', {
                    lng: self.lang,
                    file: oldFile,
                    error: error.toString()
                }));
            }

            if ( !success ) {
                return cb( i18n.t('Could not rename file __oldFile__', {
                    lng: self.lang,
                    file: oldFile
                }));
            }

            cb();
        } );
    }
};