"use strict";

const _    = require( 'underscore' );
const i18n = require( 'i18next' );

/**
 * @module server/net/commands/quiddity/nickname/get
 */
module.exports = {

    /**
     * Get nickname quiddity
     *
     * @param {string} quiddityId Quiddity for which we want to retrieve the nickname
     * @param {Function} cb Callback
     */
    execute: function ( quiddityId, cb ) {
        if ( _.isEmpty( quiddityId ) ) {
            return cb( i18n.t( 'Missing quiddity id parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( quiddityId ) ) {
            return cb( i18n.t( 'Invalid quiddity id (__quiddity__)', {
                lng: this.lang,
                quiddity: quiddityId
            } ) );
        }

        try {
            var nickname = this.switcherController.quiddityManager.getNickname( quiddityId );
        } catch ( e ) {
            return cb( i18n.t( 'An error occurred while getting the nickname on quiddity __quiddity__ (__error__)', {
                lng: this.lang,
                quiddity: quiddityId,
                error:    e.toString()
            } ) );
        }
        cb( null, nickname );
    }
};