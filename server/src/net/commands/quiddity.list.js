"use strict";

const i18n = require('i18next');

/**
 * @module server/net/commands/quiddity/list
 */
module.exports = {

    /**
     * List quiddities
     * 
     * @param {Function} cb Callback
     */
    execute: function( cb ) {
        try {
            var quiddities = this.switcherController.quiddityManager.getQuiddities();
        } catch ( e ) {
            return cb( i18n.t('An error occurred while getting quiddities (__error__)', {
                lng: this.lang,
                error: e.toString()
            } ) );
        }
        cb( null, quiddities );
    }
};