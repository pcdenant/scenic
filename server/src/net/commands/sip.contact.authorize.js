"use strict";

const _    = require( 'underscore' );
const i18n = require( 'i18next' );
const log  = require( '../../lib/logger' );

/**
 * @module server/net/commands/sip/contact/update
 */
module.exports = {

    /**
     * Authorize contact command
     *
     * @param {string} uri - URI of the contacts
     * @param {Boolean} authorized
     * @param {Function} cb Callback
     */
    execute: function ( uri, authorized, cb ) {
        if ( _.isEmpty( uri ) ) {
            return cb( i18n.t( 'Missing uri parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( uri ) ) {
            return cb( i18n.t( 'Invalid uri (__uri__)', {
                lng: this.lang,
                uri: uri
            } ) );
        }
        if ( !_.isBoolean( authorized ) ) {
            return cb( i18n.t( 'Missing  parameter', {
                lng: this.lang
            } ) );
        }

        try {
            var blocked = this.switcherController.sipManager.authorizeContact( uri, authorized );
        } catch ( e ) {
            return cb( i18n.t( 'An error occurred while blocking contact __uri__ (__error__)', {
                lng:   this.lang,
                uri:   uri,
                error: e.toString()
            } ) );
        }

        if ( !blocked ) {
            return cb( i18n.t( 'Could not block contact __uri__', {
                lng:  this.lang,
                uri:  uri
            } ) )
        }

        cb( null, blocked );
    }
};