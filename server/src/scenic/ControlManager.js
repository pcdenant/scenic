'use strict';

const _           = require( 'underscore' );
const log         = require( '../lib/logger' );
const BaseManager = require( './BaseManager' );

/**
 * Manages switcher operations related to the property mapping feature.
 * It is used to map properties from one quiddity to properties of another quiddity.
 * @extends BaseManager
 */
class ControlManager extends BaseManager {

    /**
     * Create a mapping between a source property from the source quiddity 
     * to the destination property of the destination quiddity
     *
     * @param {string} sourceQuiddity
     * @param {string} sourceProperty
     * @param {string} destinationQuiddity
     * @param {string} destinationProperty
     * @returns {boolean}
     */
    addMapping( sourceQuiddity, sourceProperty, destinationQuiddity, destinationProperty ) {
        log.info( 'Adding mapping', sourceQuiddity, sourceProperty, 'to', destinationQuiddity, destinationProperty );

        var sourcePropertyDescription = this.switcherController.quiddityManager.getPropertyDescription( sourceQuiddity, sourceProperty );
        if ( !sourcePropertyDescription ) {
            log.warn( 'Source property not found' );
            return false;
        }

        var destinationPropertyDescription = this.switcherController.quiddityManager.getPropertyDescription( destinationQuiddity, destinationProperty );
        if ( !destinationPropertyDescription ) {
            log.warn( 'Destination property not found' );
            return false;
        }

        var mapper = this.switcherController.quiddityManager.create( 'property-mapper' );
        if ( !mapper ) {
            log.warn( 'Could not create mapper' );
            return false;
        }

        // Utility function to remove the mapper
        var self = this;

        function removeMapper() {
            log.info( 'Removing mapper' );
            var removed = self.switcherController.quiddityManager.remove( mapper.id );
            if ( !removed ) {
                log.error( 'Could not remove mapper after mapping failed' );
            }
        }

        var sourceSet = this.switcherController.quiddityManager.invokeMethod( mapper.id, 'set-source-property', [sourceQuiddity, sourceProperty] );
        if ( !sourceSet ) {
            log.warn( 'Could not map source property' );
            removeMapper();
            return false;
        }

        var destinationSet = this.switcherController.quiddityManager.invokeMethod( mapper.id, 'set-sink-property', [destinationQuiddity, destinationProperty] );
        if ( !destinationSet ) {
            log.warn( 'Could not map destination property' );
            removeMapper();
            return false;
        }

        // If we are here everything went smoothly
        return true;
    };

    /**
     * Removes all mappings for a quiddity (source and destination).
     * 
     * @param {string} quiddityId
     */
    removeMappingsByQuiddity( quiddityId ) {
        log.info( 'Removing mappers by quiddity', quiddityId );

        var quiddities = this.switcherController.quiddityManager.getQuiddities( { tree: true } );
        if ( !quiddities ) {
            return false;
        }

        var mappers = quiddities.filter( quiddity => (
            quiddity.class == 'property-mapper'
            && quiddity.tree
            && (
                ( quiddity.tree.source && quiddity.tree.source.quiddity == quiddityId )
                || ( quiddity.tree.sink && quiddity.tree.sink.quiddity == quiddityId )
            )
        ) );

        var allRemoved = true;
        mappers.forEach( mapper => {
            var removed = this.switcherController.quiddityManager.remove( mapper.id );
            if ( !removed ) {
                allRemoved = false;
            }
        } );

        return allRemoved;
    };

    /**
     * Remove all mapping for a destination property
     *
     * @param {string} destinationQuiddity
     * @param {string} destinationProperty
     */
    removeMappingsByDestination( destinationQuiddity, destinationProperty ) {
        log.info( 'Removing mappers by destination', destinationQuiddity, destinationProperty );
        var quiddities = this.switcherController.quiddityManager.getQuiddities();
        if ( !quiddities ) {
            return false;
        }

        var mappers = quiddities.filter( quiddity => (
            quiddity.class == 'property-mapper' &&
            quiddity.tree &&
            quiddity.tree.sink &&
            quiddity.tree.sink.quiddity == destinationQuiddity &&
            quiddity.tree.sink.property == destinationProperty
        ) );

        var allRemoved = true;
        mappers.forEach( mapper => {
            var removed = this.switcherController.quiddityManager.remove( mapper.id );
            if ( !removed ) {
                allRemoved = false;
            }
        } );

        return allRemoved;
    };

}


module.exports = ControlManager;