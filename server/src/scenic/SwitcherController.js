"use strict";

const fs              = require( 'fs' );
const path            = require( 'path' );
const _               = require( 'underscore' );
const async           = require( 'async' );
const switcher        = require( 'switcher' );
const SipManager      = require( './SipManager' );
const IRCManager      = require( './IRCManager' );
const QuiddityManager = require( './QuiddityManager' );
const ControlManager  = require( './ControlManager' );
const Timelapse       = require( './quiddities/Timelapse' );
const SIP             = require( './quiddities/SIP' );
const UserQuiddity    = require( './quiddities/UserQuiddity' );
const log             = require( '../lib/logger' );
const checkPort       = require( '../utils/check-port' );
const cleanFileName   = require( '../utils/cleanFileName' );

/**
 * Switcher Controller
 */
class SwitcherController {

    /**
     * Make a Switcher Controller instance
     *
     * @param {Object} config Scenic configuration
     * @param {Object} io Socket.io
     */
    constructor( config, io ) {
        this.config = config;
        this.io     = io;

        // Namespace switcher with the scenic port from the config (if any, we silently ignore it otherwise) so that
        // multiple instances running on the same machine don't create shmdatas with conflicting names
        const name    = 'scenic' + ( ( config.scenic && config.scenic.port ) ? config.scenic.port : '' );
        //this.switcher = new switcher.Switcher( name, log.switcher );
        this.switcher = new switcher.Switcher( name, this._callbackLogger.bind( this ) );

        this.quiddityManager = new QuiddityManager( this );
        this.sipManager      = new SipManager( this );
        this.controlManager  = new ControlManager( this );
        this.ircManager      = new IRCManager( this );

        /**
         * Connected clients
         * @type {ScenicClient[]}
         */
        this.clients = [];

        /**
         * Current file path
         * @type {string}
         */
        this.currentFile = null;
    }

    /**
     * Initialize
     * Sets up the default Scenic environment
     *
     * @param {Function} callback Function to call when initialization is finished
     */
    initialize( callback ) {
        log.debug( "Initializing Switcher Controller..." );

        // Create save file directory
        if ( !fs.existsSync( this.config.savePath ) ) {
            try {
                fs.mkdirSync( this.config.savePath );
            } catch ( err ) {
                console.error( "Could not create directory: " + this.config.savePath + " Error: " + err.toString() );
                return process.exit( 1 );
            }
        }

        // Switcher Callbacks
        this.switcher.register_prop_callback( this._onSwitcherProperty.bind( this ) );
        this.switcher.register_signal_callback( this._onSwitcherSignal.bind( this ) );

        /**
         * DEFAULTS
         */

        log.debug( 'Setting up system...' );

        async.series( [

            callback => {
                // Initialize managers
                this.quiddityManager.initialize();
                this.sipManager.initialize();
                this.controlManager.initialize();
                callback();
            },

            // Default stuff
            callback => {
                // Defaults Configuration
                if ( this.config.defaultsFile ) {
                    if ( fs.existsSync( this.config.defaultsFile ) ) {
                        log.info( `Loading defaults from ${this.config.defaultsFile}` );
                        const loaded = this.switcher.load_defaults( this.config.defaultsFile );
                        if ( !loaded ) {
                            // empty config file
                            log.error( `Could not load defaults file.` );
                        }
                    } else {
                        log.warn( `Defaults file ${this.config.defaultsFile} does not exist` );
                    }
                }

                // SOAP Control
                if ( this.config.soap.enabled ) {
                    log.debug( 'Creating SOAP Control Server...' );
                    this.switcher.create( 'SOAPcontrolServer', this.config.soap.quiddName );
                }

                // Create the default quiddities necessary to use switcher
                log.debug( 'Creating RTP Session...' );
                this.switcher.create( 'rtpsession', this.config.rtp.quiddName );

                // Create quiddity systemUsage to get information about the CPU usage
                // System usage is a private quiddity so we manually subscribe to it events
                log.debug( 'Creating System Usage...' );
                this.switcher.create( 'systemusage', this.config.systemUsage.quiddName );
                this.switcher.set_property_value( this.config.systemUsage.quiddName, 'period', String( this.config.systemUsage.period ) );

                log.debug( 'Creating thumbnail timelapse...' );
                const thumbnail = new Timelapse(
                    this,
                    this.config.timelapse.thumbnail.quiddName,
                    this.config.timelapse.thumbnail,
                    {
                        path: this.config.timelapse.path
                    }
                );

                this.quiddityManager.managedQuiddities[thumbnail.id] = thumbnail;
                this.quiddityManager.protectedQuiddities.push( thumbnail.id );

                log.debug( 'Creating preview timelapse...' );
                const preview = new Timelapse(
                    this,
                    this.config.timelapse.preview.quiddName,
                    this.config.timelapse.preview,
                    {
                        path: this.config.timelapse.path
                    }
                );

                this.quiddityManager.managedQuiddities[preview.id] = preview;
                this.quiddityManager.protectedQuiddities.push( preview.id );

                log.debug( 'Creating SIP quiddity...');
                const sip = new SIP(
                    this,
                    this.config.sip.quiddName,
                    this.config.sip
                );
                
                this.quiddityManager.managedQuiddities[sip.id] = sip;

                log.debug( 'Creating user tree quiddity...');

                const userQuiddity = new UserQuiddity(
                    this,
                    this.config.userTree.quiddName,
                    this.config.userTree.userData
                );

                this.quiddityManager.managedQuiddities[userQuiddity.id] = userQuiddity;

                // Reset command history, nothing we do here should go in the save file
                this.switcher.reset_command_history();
                callback();
            },


            // SOAP Port
            callback => {
                if ( this.config.soap.enabled ) {
                    log.debug( 'Setting SOAP port...' );
                    // Validate SOAP port and this.configure quiddity, otherwise fail
                    checkPort( 'SOAP', this.config.soap, error => {
                        if ( error ) {
                            log.error( error );
                            callback( error );
                        }
                        if ( this.config.soap.port == null ) {
                            log.error( "SOAP Port Required." );
                            callback( "SOAP Port Required." );
                        }
                        this.switcher.invoke( this.config.soap.quiddName, 'set_port', [this.config.soap.port] );

                        // Reset command history, again, we don't want to save that call
                        this.switcher.reset_command_history();

                        callback();
                    } );
                } else {
                    callback();
                }
            },

            // Load file
            callback => {
                if ( this.config.loadFile ) {
                    log.info( 'Loading save file ' + this.config.loadFile );
                    this._loadFile( this.config.loadFile, ( err, result ) => {
                        if ( err || !result ) {
                            log.error( `Could not load file ${this.config.loadFile}`, err );
                            return callback( err );
                        }
                        log.info( 'Save file loaded.' );
                        callback();
                    } );
                } else {
                    callback();
                }
            }

        ], error => {
            if ( error ) {
                log.error( error );
                return process.exit( 1 );
            } else {
                callback();
            }
        } );
    };

    /**
     * Add a client
     *
     * @param {ScenicClient} client
     */
    addClient( client ) {
        this.clients.push( client );
    };

    /**
     * Remove a client
     *
     * @param {ScenicClient} client
     */
    removeClient( client ) {
        this.clients.splice( this.clients.indexOf( client ), 1 );
    };

    //   ██████╗ █████╗ ██╗     ██╗     ██████╗  █████╗  ██████╗██╗  ██╗███████╗
    //  ██╔════╝██╔══██╗██║     ██║     ██╔══██╗██╔══██╗██╔════╝██║ ██╔╝██╔════╝
    //  ██║     ███████║██║     ██║     ██████╔╝███████║██║     █████╔╝ ███████╗
    //  ██║     ██╔══██║██║     ██║     ██╔══██╗██╔══██║██║     ██╔═██╗ ╚════██║
    //  ╚██████╗██║  ██║███████╗███████╗██████╔╝██║  ██║╚██████╗██║  ██╗███████║
    //   ╚═════╝╚═╝  ╚═╝╚══════╝╚══════╝╚═════╝ ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝

    /**
     * Internal callback to catch g_message from switcher
     *
     * @param {string} message - switcher message
     * @private
     */
    _callbackLogger( message ) { 
        if( message.indexOf('switcher-message:') != -1 ){
            this.io.emit( 'log.message', message.split( 'switcher-message: ' )[1] );
        }
        log.switcher( message );
    };
    /**
     * Switcher Property Callback
     *
     * @param {string} quiddityId - Id of the quiddity which a property have changed
     * @param {string} property - Name of the changed property
     * @param {string} value - Value of the changed property
     * @private
     */
    _onSwitcherProperty( quiddityId, property, value ) {
        this.quiddityManager.onSwitcherProperty( quiddityId, property, value );
        this.sipManager.onSwitcherProperty( quiddityId, property, value );
        this.controlManager.onSwitcherProperty( quiddityId, property, value );
    };

    /**
     * Switcher Signal Callback
     *
     * @param {string} quiddityId - Id of the quiddity which sent a signal
     * @param {string} signal - Signal name
     * @param {string} value - Signal value
     * @private
     */
    _onSwitcherSignal( quiddityId, signal, value ) {
        this.quiddityManager.onSwitcherSignal( quiddityId, signal, value );
        this.sipManager.onSwitcherSignal( quiddityId, signal, value );
        this.controlManager.onSwitcherSignal( quiddityId, signal, value );
    };

    //  ██╗     ██╗███████╗███████╗ ██████╗██╗   ██╗ ██████╗██╗     ███████╗
    //  ██║     ██║██╔════╝██╔════╝██╔════╝╚██╗ ██╔╝██╔════╝██║     ██╔════╝
    //  ██║     ██║█████╗  █████╗  ██║      ╚████╔╝ ██║     ██║     █████╗
    //  ██║     ██║██╔══╝  ██╔══╝  ██║       ╚██╔╝  ██║     ██║     ██╔══╝
    //  ███████╗██║██║     ███████╗╚██████╗   ██║   ╚██████╗███████╗███████╗
    //  ╚══════╝╚═╝╚═╝     ╚══════╝ ╚═════╝   ╚═╝    ╚═════╝╚══════╝╚══════╝

    /**
     * Close the server
     * Sends a shutdown message to every client
     */
    close() {
        log.info( "Closing Scenic server..." );
        if ( this.io ) {
            this.io.emit( 'shutdown' );
        }
        this.quiddityManager.close();
        this.switcher.close();
    };

    //  ██████╗  ██████╗  ██████╗██╗   ██╗███╗   ███╗███████╗███╗   ██╗████████╗███████╗
    //  ██╔══██╗██╔═══██╗██╔════╝██║   ██║████╗ ████║██╔════╝████╗  ██║╚══██╔══╝██╔════╝
    //  ██║  ██║██║   ██║██║     ██║   ██║██╔████╔██║█████╗  ██╔██╗ ██║   ██║   ███████╗
    //  ██║  ██║██║   ██║██║     ██║   ██║██║╚██╔╝██║██╔══╝  ██║╚██╗██║   ██║   ╚════██║
    //  ██████╔╝╚██████╔╝╚██████╗╚██████╔╝██║ ╚═╝ ██║███████╗██║ ╚████║   ██║   ███████║
    //  ╚═════╝  ╚═════╝  ╚═════╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝

    reset() {
        log.info( 'Resetting scenic file' );
        // Start by disconnecting previews we have
        this.quiddityManager.disconnectAllPreviews();
        this.sipManager.hangUpAllCalls();
        // Need to reset the userData of the user quiddity for the matrix when reset a file
        this.quiddityManager.resetUserQuiddity();
        
        this.switcher.reset_history();

        // Reconnect all thumbnails from sip quiddity
        this.quiddityManager.reconnectSIPThumbnails();

        this.io.emit( 'file.reset' );
    }

    /**
     * Get scenic files list
     *
     * TODO: Use promises instead of plain callback
     *
     * @param {Function} cb Callback
     */
    getFileList( cb ) {
        var savePath = this.config.savePath;
        log.info( 'Getting scenic file list', savePath );
        try {
            fs.readdir( savePath, ( error, files ) => {
                if ( error ) {
                    log.error( error );
                    return cb( error );
                }
                files = files
                    .filter( file => path.extname( file ) == '.json' )
                    .map( file => {
                        const filePath = savePath + file;
                        const stat     = fs.statSync( filePath );
                        const name     = file.replace( path.extname( file ), '' );
                        return {
                            id:      name,
                            name:    name,
                            path:    filePath,
                            current: filePath == this.currentFile,
                            date:    new Date( stat.mtime )
                        };
                    } );
                cb( null, files );
            } );
        } catch ( e ) {
            log.error( e );
            return cb( e.toString() );
        }
    };

    /**
     * Load scenic file
     *
     * @param {string} name - File name in the save files directory
     * @param {Function} cb - Callback to call once the operation is complete
     */
    loadFile( name, cb ) {
        var fileName = cleanFileName( name );
        var filePath = this.config.savePath + fileName + '.json';
        this.io.emit( 'file.loading', fileName );
        this._loadFile( filePath, ( err, result ) => {
            if ( err || !result ) {
                this.io.emit( 'file.load.error', fileName );
            } else {
                this.io.emit( 'file.loaded', fileName );
            }
            cb( err, result );
        } );
    };

    /**
     * get the content of a scenic file
     *
     * @param {string} name - File name in the save files directory
     * @param {Function} cb - Callback to call once the operation is complete
     */
    getFile( name, cb ) {
        log.info('getFile', name )

        this.getFileList( ( error , files) => {
            if ( error ) {
                log.error( error );
                return cb( error );
            }
            
            if ( _.findWhere(files, {name: name} ) ) {
                try {
                    var filePath = this.config.savePath +  name + '.json';
                    var data = fs.readFileSync( filePath );
                    var parsedData = JSON.parse( data );

                    cb(null, parsedData);
                } catch ( e ) {
                    log.error(e);
                    return cb( e.toString() );
                }
            } else {
                this.io.emit( 'file.get.error', name );
            }
        });
    };

    /**
     * Rename a scenic file
     *
     * @param {string} oldName - Old file name in the save files directory
     * @param {string} newName - New file name in the save files directory
     * @param {Function} cb - Callback to call once the operation is complete
     */
    renameFile( oldName, newName, cb ) {
        var fileName = cleanFileName( oldName );
        var newfileName = cleanFileName( newName );
        var oldFilePath = this.config.savePath + fileName + '.json';
        var newFilePath = this.config.savePath + newfileName + '.json';

        var self = this;
        try {
            fs.rename( oldFilePath, newFilePath, function ( error ) {
                if ( error ) {
                    log.error( error );
                    return cb( error );
                }
                log.info( 'Scenic file renamed', newFilePath );
                self.io.emit( 'file.renamed', { "filename": oldName, "rename": newName } );
                cb(null, true);
            } );
        } catch ( e ) {
            log.error( e );
            return cb( e.toString() );
        }
    };

    /**
     * Internal scenic file loading
     *
     * This method uses defer as the blocking load_history_from_scratch prevented the "file.loading" message
     * being sent to the clients before actually loading the file.
     *
     * @param {string} filePath - Complete path to the file to load
     * @param {Function} cb - Callback to call once the operation is complete
     * @private
     */
    _loadFile( filePath, cb ) {
        log.info( 'Loading scenic file', filePath );
        _.defer( _.bind( function () {
            // Start by disconnecting all previews we have
            this.quiddityManager.disconnectAllPreviews();
            // Need to reset the userData of the user quiddity for the matrix when reset a file
            this.quiddityManager.resetUserQuiddity();

            this.switcher.reset_history();

            try {
                var loaded = this.switcher.load_history_from_current_state( filePath );
            } catch ( e ) {
                log.error( e.toString() );
                return cb( e, false );
            }
            if ( !loaded ) {
                log.warn( 'Could not load scenic file', filePath );
                this.currentFile = null;
            } else {
                this.currentFile = filePath;
            }
            cb( null, loaded );
        }, this ) );
    };

    /**
     * Save scenic file
     *
     * @param {string} name - File name
     */
    saveFile( name ) {
        var fileName = cleanFileName( name );
        var filePath = this.config.savePath + fileName + '.json';
        log.info( 'Saving scenic file', filePath );
        var saved = this.switcher.save_history( filePath );
        if ( !saved ) {
            log.warn( 'Could not save scenic file', filePath );
            return null;

        } else {
            log.info( 'Scenic file saved', filePath );
            this.currentFile = filePath;
            var file         = {
                id:      fileName,
                name:    fileName,
                path:    filePath,
                current: true,
                date:    new Date( fs.statSync( filePath ).mtime )
            };
            this.io.emit( 'file.saved', file );
            return file;
        }
    };

    /**
     * Import a scenic file
     *
     * @param {string} name - File name
     * @param {string} content - File content
     */
    importFile( name, content ) {
        var filePath = this.config.savePath + name;
        var exists   = fs.existsSync( filePath );

        if ( exists ) {
            log.warn( 'file already exists', filePath );
            this.io.emit( 'file.import.error', name ); 
        } else {
            fs.writeFile(filePath, content, err => {
                if( err ) {
                    log.warn( 'Error import file', err);
                }
                // remove the .json
                var filename = name.slice( 0, name.length - 5);
                var file         = {
                    id:      filename,
                    name:    filename,
                    path:    filePath,
                    current: true,
                    date:    new Date( fs.statSync( filePath ).mtime )
                };
                this.io.emit( 'file.imported', file );        
            });          
        }
    };

    /**
     * Save scenic file as...
     *
     * @param {string} name - File name
     */
    saveFileAs( name ) {
        return this.saveFile( name );
    }

    /**
     * Remove scenic file
     *
     * TODO: Use promises instead of plain callback
     *
     * @param {string} name - File name to delete
     * @param {Function} cb - Callback
     */
    deleteFile( name, cb ) {
        var self     = this;
        var fileName = cleanFileName( name );
        var filePath = this.config.savePath + fileName + '.json';
        log.info( 'Removing scenic file', filePath );
        try {
            fs.unlink( filePath, function ( error ) {
                if ( error ) {
                    log.error( error );
                    return cb( error );
                }
                log.info( 'Scenic file deleted', filePath );
                self.io.emit( 'file.deleted', fileName );
                cb();
            } );
        } catch ( e ) {
            log.error( e );
            return cb( e.toString() );
        }
    };

}

module.exports = SwitcherController;
