"use strict";

/**
 * This is the base for all manager classes.
 * Manager classes are used to separate various concerns related to switcher.
 */
class BaseManager {

    /**
     * Constructs a new manager
     * @param {SwitcherController} switcherController
     */
    constructor(switcherController) {
        this.switcherController = switcherController;
        this.config             = this.switcherController.config;
        this.switcher           = this.switcherController.switcher;
        this.io                 = this.switcherController.io;
    }

    /**
     * Initialize the manager
     *
     * @abstract
     */
    initialize() {
        //noop
    }

    /**
     * Switcher Property Callback
     * This will be called by SwitcherController when a property changes.
     *
     * @abstract
     * @param {string} quiddityId - Id of the quiddity which a property have changed
     * @param {string} property - Name of the changed property
     * @param {string} value - Value of the changed property
     */
    onSwitcherProperty( quiddityId, property, value ) {
        //noop
    };

    /**
     * Switcher Signal Callback
     * This will be called by SwitcherController when a signal is sent from switcher
     *
     * @abstract
     * @param {string} quiddityId - Id of the quiddity which sent a signal
     * @param {string} signal - Signal name
     * @param {string} value - Signal value
     */
    onSwitcherSignal( quiddityId, signal, value ) {
        //noop
    };
}

module.exports = BaseManager;