"use strict";

const _        = require( "underscore" );
const log      = require( '../../lib/logger' );
const Quiddity = require( "./Quiddity" );

/**
 * SIP Quiddity
 */
class SIP extends Quiddity {

    /**
     * Create a new SIP
     *
     * @param {SwitcherController} switcherController
     * @param {string} id
     * @param {Object} [initProperties]
     * @param {Object} [config]
     */
    constructor( switcherController, id, initProperties, config ) {
        super( switcherController, "sip", id, initProperties, config );
        this.managedProperties['sip-registration'] = true;

        this.didSetup = false;

        // Handle cases were the quiddity starts already logged in (using switcher defaults json file)
        if ( this.switcherController.switcher.get_property_value( id, 'sip-registration' ) ) {
            this._setupContacts();
        } else if ( initProperties.commandLine ) {
            this.switcherController.switcher.set_property_value( id, 'port', String( initProperties.port ) );
        }
    }

    /**
     * Property Changed
     *
     * @param {string} property
     * @param {string} value
     */
    onProperty( property, value ) {
        switch ( property ) {
            case 'sip-registration':
                if ( value && !this.didSetup ) {
                    this._setupContacts();
                } else if ( !value ) {
                    this.didSetup = false;
                }
                break;
        }
    }

    _setupContacts() {
        // Add self to the contact list
        const uri = this.switcherController.sipManager.getSelf();

        this.switcherController.sipManager.addContact( uri, uri.split( '@' )[0], true, true );
        
        // Add user's contacts
        if ( this.switcherController.sipManager.contacts && this.switcherController.sipManager.contacts[uri] ) {
            _.each( this.switcherController.sipManager.contacts[uri], function ( contactInfo, contactURI ) {
                // Do not add self from here, it already has been done earlier
                if ( contactURI != uri ) {
                    this.switcherController.sipManager.addContact( contactURI, contactInfo.name, contactInfo.authorized, true );
                }
            }, this );
        }

        // Save contacts (async, but we don't care about when it finishes)
        this.switcherController.sipManager.saveContacts();

        this.didSetup = true;
    }

}

module.exports = SIP;