"use strict";

const _ = require('underscore');

/**
 * Quiddity Base Class
 */
class Quiddity {

    /**
     * Create a new quiddity
     *
     * @param {SwitcherController} switcherController
     * @param {String} quiddityClass
     * @param {string} id
     * @param {Object} [initProperties]
     * @param {Object} [config]
     */
    constructor( switcherController, quiddityClass, id, initProperties, config ) {
        this.switcherController = switcherController;
        this.id = id;
        this.initProperties = initProperties || {};
        this.config = _.defaults( config || {}, { create: true });
        
        this.managedProperties = {}; 
        
        if ( this.config.create ) {
            //TODO: Creation status in quiddity, or error reporting on creation
            //TODO: "real id" when creating, reuse what is returned by create
            this.switcherController.switcher.create( quiddityClass, id );
        }
    }

    /**
     * Property Changed
     *
     * @param {string} property
     * @param {string} value
     */
    onProperty(property, value) {
        
    }

}

module.exports = Quiddity;