'use strict';

const _ = require( 'underscore' );
const log = require( '../lib/logger' );
const BaseManager = require( './BaseManager' );
const NameExistsError = require( '../exceptions/NameExistsError' );

/**
 * Manages switcher operations related to the quiddities and their lifecycle.
 * @extends BaseManager
 */
class QuiddityManager extends BaseManager {

    /**
     * @inheritdoc
     */
    constructor( switcherController ) {
        super( switcherController );

        /**
         * Map of quiddities and their creator's socket id
         * Used to tell clients if they are the creator and thus should display the edit panel, for example
         * @type {Object.<string,string>}
         */
        this.quidditySocketMap = {};

        /**
         * Private Quiddity Classes
         * These class of quiddities cannot be created by the end-user
         *
         * @type {string[]}
         */
        this.privateQuiddityClasses = [
            'dico',
            'create_remove_spy',
            'logger',
            'runtime',
            'SOAPcontrolServer'
        ].concat( switcherController.config.privateQuiddities );

        /**
         * Protected Quiddities (actual instances, by id)
         * They are handled by the back-end but never sent to the client
         * @type {Quiddity[]}
         */
        this.protectedQuiddities = [];

        /**
         * Read-Only Quiddities
         * Quiddities that are shared with the client but that can't be manually removed
         * @type {Array<Object<string, Object<string,string>>>}
         */
        this.readOnlyQuiddityClasses = [
            'sip'
        ];

        /**
         * Managed quiddity instances
         * @type {Object<string,Quiddity>}
         */
        this.managedQuiddities = {};

        /**
         * Allowed shmdata types
         * @type {string[]}
         */
        this.shmdataTypes = ['reader', 'writer'];

        /**
         * Changed properties
         * A cache of all the latest changed properties that is periodically checked and sent to the client.
         * This prevents sending too many changes and overloading the network. The client only needs to know the most
         * recent value, not all the intermediate changes.
         * @type {object[]}
         */
        this.changedProperties = {};

        /**
         * Connected preview shmdata counter
         * Count the number of clients connected (viewing) the preview of each shmdata in order to keep track of
         * which shmdata to connect/disconnect.
         * @type {Object<string, int>}
         */
        this.connectedPreviews = {};
    }

    /**
     * @inheritdoc
     */
    initialize() {
        super.initialize();

        // Watch changed properties at approx. 30Hz
        this.intervalPublishChanged = setInterval( _.bind( this.publishChangedProperties, this ), 1000 / 30 );
    };

    close() {
        clearInterval(this.intervalPublishChanged);
    };

    /**
     * Connect a shmdata to the preview quiddity.
     * This keeps a counter of the connected clients, and only connect if there is no client currently
     * previewing the specified shmdata
     * @param {string} shmdata
     */
    connectPreview( shmdata ) {
        log.debug( `Connecting preview for ${shmdata}` );
        if ( this.connectedPreviews[shmdata] ) {
            this.connectedPreviews[shmdata]++;
        } else {
            log.debug( `\tIt is the first connection, connecting for real.` );
            this.connectedPreviews[shmdata] = 1;
            this.invokeMethod( this.config.timelapse.preview.quiddName, 'connect', [shmdata] );
        }
    };

    /**
     * Disconnects the shmdata from the preview quiddity.
     * It uses a counter to only disconnect when no client remains connected to the preview.
     * @param {string} shmdata
     */
    disconnectPreview( shmdata ) {
        log.debug( `Disconnecting preview for ${shmdata}` );
        if ( this.connectedPreviews[shmdata] ) {
            this.connectedPreviews[shmdata]--;
            if ( this.connectedPreviews[shmdata] == 0 ) {
                log.debug( `\tIt was the last connected client, disconnecting for real.` );
                this.invokeMethod( this.config.timelapse.preview.quiddName, 'disconnect', [shmdata] );
            }
        }
    };

    _disconnectAutoPreviews( path ) {
        log.debug( `Disconnecting automatic previews for ${path}` );
        var previewReaders = this.getTreeInfo( this.config.timelapse.preview.quiddName, '.shmdata.reader' );
        if ( previewReaders && previewReaders[path] ) {
            log.verbose( `\tDisconnecting preview for ${path}` );
            this.invokeMethod( this.config.timelapse.preview.quiddName, 'disconnect', [path] );
            delete this.connectedPreviews[path];
        }
        var thumbnailReaders = this.getTreeInfo( this.config.timelapse.thumbnail.quiddName, '.shmdata.reader' );
        if ( thumbnailReaders && thumbnailReaders[path] ) {
            log.verbose( `\tDisconnecting thumbnail for ${path}` );
            this.invokeMethod( this.config.timelapse.thumbnail.quiddName, 'disconnect', [path] );
        }
    };

    disconnectAllPreviews() {
        log.debug( `Disconnecting all previews` );
        var previewReaders = this.getTreeInfo( this.config.timelapse.preview.quiddName, '.shmdata.reader' );
        _.forEach( previewReaders, ( reader, path ) => {
            log.verbose( `\tDisconnecting preview for ${path}` );
            this.invokeMethod( this.config.timelapse.preview.quiddName, 'disconnect', [path] );
            delete this.connectedPreviews[path];
        } );
        var thumbnailReaders = this.getTreeInfo( this.config.timelapse.thumbnail.quiddName, '.shmdata.reader' );
        _.forEach( thumbnailReaders, ( reader, path ) => {
            log.verbose( `\tDisconnecting thumbnail for ${path}` );
            this.invokeMethod( this.config.timelapse.thumbnail.quiddName, 'disconnect', [path] );
        } );
    };

    reconnectSIPThumbnails() {
        var hasSipQuiddity = this.exists( this.config.sip.quiddName );

        try {
            var writers = this.getTreeInfo( this.config.sip.quiddName, '.shmdata.writer' );
        } catch ( e ) {
            log.error( e );
        }
        if ( !writers ) {
            return null;
        }

        _.forEach( writers, ( writer, path ) => {
            if ( writer.category == 'video') {
                this.invokeMethod( this.config.timelapse.thumbnail.quiddName, 'connect', [path] );
            }
        } );
    };

    resetUserQuiddity() {
        var hasUserQuiddity = this.exists( this.config.userTree.quiddName );
        if ( hasUserQuiddity ) {
            this.setUserDataBranch( this.config.userTree.quiddName , this.config.userTree.userData.path, JSON.stringify( this.config.userTree.userData.value )) ;
        }
    };

    //  ██████╗  █████╗ ██████╗ ███████╗███████╗██████╗ ███████╗
    //  ██╔══██╗██╔══██╗██╔══██╗██╔════╝██╔════╝██╔══██╗██╔════╝
    //  ██████╔╝███████║██████╔╝███████╗█████╗  ██████╔╝███████╗
    //  ██╔═══╝ ██╔══██║██╔══██╗╚════██║██╔══╝  ██╔══██╗╚════██║
    //  ██║     ██║  ██║██║  ██║███████║███████╗██║  ██║███████║
    //  ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝╚══════╝

    /**
     * Parse a shmdata into a format more manageable by the front end
     *
     * @param shmdata
     * @returns {*}
     */
    _parseShmdata( shmdata ) {
        if ( shmdata.byte_rate ) {
            shmdata.byte_rate = parseInt( shmdata.byte_rate );
        }
        return shmdata;
    };

    /**
     * Parse a property into a format more manageable by the front end
     *
     * EDIT: Properties were refactored, this only puts the value inside the property info for now
     * TODO: Get rid of this method
     *
     * @param property
     * @returns {*}
     */
    _parseProperty( quiddityId, property ) {
        const value = this.getPropertyValue( quiddityId, property.id );

        // Value
        switch ( property.type ) {
            case 'string':
                // Try JSON first, switcher doesn't tell us if it's JSON or not
                try {
                    var v = JSON.parse( value );
                } catch ( e ) {
                    // Don't care
                }
                property.value = v || value;
                break;

            default:
                if ( 'undefined' != typeof value ) {
                    property.value = value;
                } else if ( 'undefined' != typeof property.default ) {
                    property.value = property.default;
                }
        }

        // General
        property.parent = _.isEmpty( property.parent ) ? null : property.parent;

        return property;
    };

    /**
     * Parse a method into a format more manageable by the front end
     *
     * @param method
     * @return {*}
     */
    _parseMethod( method ) {
        // General
        method.parent = _.isEmpty( method.parent ) ? null : method.parent;
        method.returnType = method['return type'];
        method.returnDescription = method['return description'];

        delete method['default value'];
        delete method['return type'];
        delete method['return description'];

        // Arguments
        if ( method['arguments'] ) {
            method.args = method['arguments'];
            delete method['arguments'];
            _.each( method.args, function ( argument ) {
                argument.id = argument.name;
                argument.name = argument['long name'];

                delete argument['long name'];
            }, this );
        }

        return method;
    };

    //  ██╗███╗   ██╗████████╗███████╗██████╗ ███╗   ██╗ █████╗ ██╗     ███████╗
    //  ██║████╗  ██║╚══██╔══╝██╔════╝██╔══██╗████╗  ██║██╔══██╗██║     ██╔════╝
    //  ██║██╔██╗ ██║   ██║   █████╗  ██████╔╝██╔██╗ ██║███████║██║     ███████╗
    //  ██║██║╚██╗██║   ██║   ██╔══╝  ██╔══██╗██║╚██╗██║██╔══██║██║     ╚════██║
    //  ██║██║ ╚████║   ██║   ███████╗██║  ██║██║ ╚████║██║  ██║███████╗███████║
    //  ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝  ╚═╝╚══════╝╚══════╝

    /**
     * Fill quiddity with its properties, methods and tree
     *
     * @param {Quiddity} quiddity
     * @param {Object} [config]
     * @returns {Quiddity}
     * @private
     */
    _fillQuiddity( quiddity, config ) {

        // Set read-only flag
        quiddity.readOnly = _.contains( this.readOnlyQuiddityClasses, quiddity.class );
        quiddity.protected = _.contains( this.protectedQuiddities, quiddity.id );

        // Get all properties
        if ( !config || config.properties ) {
            try {
                // Fill properties in the quiddity object
                quiddity.properties = this.getProperties( quiddity.id );
            } catch ( e ) {
                log.error( 'Error getting properties for quiddity', quiddity.id, e );
            }
        }

        // Get all methods
        if ( !config || config.methods ) {
            try {
                // Fill methods in the quiddity object
                quiddity.methods = this.getMethods( quiddity.id );
            } catch ( e ) {
                log.error( 'Error getting methods for quiddity', quiddity.id, e );
            }
        }

        // Get tree
        if ( !config || config.tree ) {
            try {
                // Fill tree in the quiddity object
                quiddity.tree = this.getTreeInfo( quiddity.id, '.' );
            } catch ( e ) {
                log.error( 'Error getting tree for quiddity', quiddity.id, e );
            }
        }

        // Get user data
        if ( !config || config.userData ) {
            try {
                // Fill userData in the quiddity object
                quiddity.userData = this.getUserData( quiddity.id, '.' );
            } catch ( e ) {
                log.error( 'Error getting userData for quiddity', quiddity.id, e );
            }
        }

        return quiddity;
    };

    /**
     * Subscribe to quiddity
     *
     * @private
     * @param {Object} quiddity
     */
    _subscribeToQuiddity( quiddity ) {
        log.debug( `Subscribing to quiddity ${quiddity.id}` );

        // Methods
        this.switcher.subscribe_to_signal( quiddity.id, 'on-method-added' );
        this.switcher.subscribe_to_signal( quiddity.id, 'on-method-removed' );

        // Tree
        this.switcher.subscribe_to_signal( quiddity.id, 'on-tree-grafted' );
        this.switcher.subscribe_to_signal( quiddity.id, 'on-tree-pruned' );

        // User Data
        this.switcher.subscribe_to_signal( quiddity.id, 'on-user-data-grafted' );
        this.switcher.subscribe_to_signal( quiddity.id, 'on-user-data-pruned' );

        this.switcher.subscribe_to_signal( quiddity.id, 'on-nicknamed' );

        // Fill with properties, methods and tree
        this._fillQuiddity( quiddity );

        // Property Values
        if ( quiddity.properties ) {
            quiddity.properties.forEach( property => {
                log.debug( 'Subscribing to property ' + property.label + ' (' + property.id + ') of ' + quiddity.id );
                this.switcher.subscribe_to_property( quiddity.id, property.id );
            } );
        }
    };

    /**
     * Subscribe to quiddities
     * 
     */
    subscribeToQuiddities( skippedQuiddities = [] ) {
        log.info( 'Subscribing to quiddities' );

        var result = this.switcher.get_quiddities_description();
        if ( result && result.error ) {
            throw new Error( result.error );
        } else if ( result && ( !result.quiddities || !_.isArray( result.quiddities ) ) ) {
            throw new Error( "Could not get quiddities while trying to subscribe to all" );
        }

        result.quiddities
            .filter( quiddity => this.managedQuiddities[quiddity.id] == null && quiddity.id != 'internal_logger' )
            .filter( quiddity => skippedQuiddities.indexOf( quiddity.id ) == -1 )
            .forEach( quiddity => {
                    this._subscribeToQuiddity( quiddity );
                    // need to connect timelapse thumb to their shmdata 
                    try {
                        var writers = this.getTreeInfo( quiddity.id, '.shmdata.writer' );
                    } catch ( e ) {
                        log.error( e );
                    }

                    _.forEach( writers, ( writer, path ) => {
                        if ( writer.category == 'video') {
                            this.invokeMethod( this.config.timelapse.thumbnail.quiddName, 'connect', [path] );
                        }
                    } );
                }
             );
    };

    /**
     * On Quiddity Added Handler
     *
     * @param quiddityId
     * @private
     */
    _onCreated( quiddityId ) {
        log.info( 'Quiddity created:', quiddityId );

        // Quiddity was created, get information on its type
        try {

            var quiddity = this.switcher.get_quiddity_description( quiddityId );
        } catch ( e ) {
            return log.error( 'Error while retrieving quiddity information', quiddityId, e );
        }
        if ( !quiddity ) {
            return log.error( 'Failed to get information for quiddity', quiddityId );
        } else if ( quiddity.error ) {
            return log.error( 'Failed to get information for quiddity', quiddityId, quiddity.error );
        }

        log.debug( quiddity );

        this._subscribeToQuiddity( quiddity );

        // SOAP Control Client
        if ( quiddity.class == 'SOAPcontrolClient' ) {
            log.debug( 'SOAP Control Client, subscribing to on-connection-tried' );
            this.switcher.subscribe_to_signal( quiddityId, 'on-connection-tried' );
        }

        this.io.emit( 'quiddity.created', quiddity, this.quidditySocketMap[quiddity.id] );
    };

    /**
     * On Quiddity Removed Handler
     *
     * @param quiddityId
     * @private
     */
    _onRemoved( quiddityId ) {
        log.info( 'Quiddity removed:', quiddityId );

        // Remove related stuff
        //TODO: Remove connections?
        try {
            this.switcherController.controlManager.removeMappingsByQuiddity( quiddityId );
        } catch ( e ) {
            log.error( 'Error while removing quiddity mappings', quiddityId, e );
        }

        // Broadcast to clients
        this.io.emit( 'quiddity.removed', quiddityId );
    };

    /**
     * Nickname updated Handler
     *
     * @param quiddityId
     * @param value
     * @private
     */
    _onNicknamed( quiddityId, value ) {
        if ( "undefined" != typeof value ) {
            this.io.emit( 'quiddity.nickname.updated', quiddityId, value );
        }
    }

    /**
     * Tree Grafted Handler
     *
     * @param quiddityId
     * @param path
     * @private
     */
    _onTreeGrafted( quiddityId, path ) {
        var graftedPath = path.split( '.' );

        // Remove the first empty value, because paths sometimes start with a dot
        if ( _.isEmpty( graftedPath[0] ) ) {
            graftedPath.shift();
        }

        try {
            var value = this.getTreeInfo( quiddityId, path );
        } catch ( e ) {
            return log.error( e );
        }

        let broadcastTreeUpdate = true;

        switch ( graftedPath[0] ) {
            case 'shmdata':
                this._onTreeGraftedShmdata( quiddityId, path, graftedPath, value );
                broadcastTreeUpdate = false;
                break;

            case 'property':
                this._onTreeGraftedProperty( quiddityId, path, graftedPath, value );
                broadcastTreeUpdate = false;
                break;

            default:
                //noop
                break;
        }


        if ( "undefined" != typeof value || value != null ) {
            var start = path.substr( 0, 1 ) == '.' ? 1 : 0;
            var end = path.substr( path.length - 1, 1 ) == '.' ? path.length - 1 : path.length;
            path = path.substring( start, end );

            if ( broadcastTreeUpdate ) {
                // Unhandled tree update, broadcast
                this.io.emit( 'quiddity.tree.updated', quiddityId, path, value );
            } else {
                // Tree data was handled, do not broadcast
                // but send to the clients that subscribed to the quiddity
                this.switcherController.clients.forEach( client => {
                    if ( client.subscriptions[quiddityId] ) {
                        client.socket.emit( 'quiddity.tree.updated', quiddityId, path, value );
                    }
                } );
            }
            // if focused, send it only to the host client 
            if( path == 'focused' ) {
                this.switcherController.clients.forEach( client => {
                    if ( this.config.host == client.config.host ) {
                        client.socket.emit( 'quiddity.focused', quiddityId, path, value );
                    }
                } );
            }         
        }
    };

    /**
     * Handles shmdata tree grafts
     *
     * @param {string} quiddityId
     * @param {string} path
     * @param {string[]} pathElements
     * @param {*} value
     * @private
     */
    _onTreeGraftedShmdata( quiddityId, path, pathElements, value ) {
        if ( pathElements.length < 3 ) {
            return;
        }

        var graftedShmdataType = pathElements[1];
        var graftedShmdataPath = pathElements[2];
        if ( !_.isEmpty( graftedShmdataPath ) && _.contains( this.shmdataTypes, graftedShmdataType ) ) {

            //TODO: Merge into a more unified tree management
            var key = pathElements[pathElements.length - 1];
            if ( key == 'stat' ) {
                // This is a special case done to send less data over the network
                // It should eventually be merged into a better tree management
                if ( value ) {
                    this.io.emit( 'shmdata.update.stat', quiddityId, graftedShmdataType + '.' + graftedShmdataPath, value );
                }
            } else {
                try {
                    var shmdataInfo = this.getTreeInfo( quiddityId, '.shmdata.' + graftedShmdataType + '.' + graftedShmdataPath );
                } catch ( e ) {
                    log.error( e );
                }

                if ( !shmdataInfo || !_.isObject( shmdataInfo ) || shmdataInfo.error ) {
                    log.error( shmdataInfo ? shmdataInfo.error : 'Could not get shmdata writer info' );

                } else {
                    this._parseShmdata( shmdataInfo );
                    // Made-up id for backbone
                    shmdataInfo.id = graftedShmdataType + '.' + graftedShmdataPath;
                    shmdataInfo.path = graftedShmdataPath;
                    shmdataInfo.type = graftedShmdataType;

                    this.io.emit( 'shmdata.update', quiddityId, shmdataInfo );

                    // If this is a video shmdata, connect it to the thumbnail quiddity if not already connected
                    if ( graftedShmdataType == 'writer' && shmdataInfo.category == 'video' ) {
                        var thumbnailReaders = this.getTreeInfo( this.config.timelapse.thumbnail.quiddName, '.shmdata.reader' );
                        if ( !thumbnailReaders || !thumbnailReaders[shmdataInfo.path] ) {
                            this.invokeMethod( this.config.timelapse.thumbnail.quiddName, 'connect', [shmdataInfo.path] );
                        }
                    }

                }
            }
        }
    }

    /**
     * Handles property tree grafts
     *
     * @param {string} quiddityId
     * @param {string} path
     * @param {string[]} pathElements
     * @param {*} value
     * @private
     */
    _onTreeGraftedProperty( quiddityId, path, pathElements, value ) {
        const property = pathElements[1];
        if ( pathElements.length == 2 ) {
            // Whole property grafted
            log.debug( 'Property added', quiddityId, property );
            this.switcher.subscribe_to_property( quiddityId, property );
            var propertyDescription = this.getPropertyDescription( quiddityId, property );
            if ( propertyDescription ) {
                this.io.emit( 'quiddity.property.added', quiddityId, propertyDescription );
            } else {
                log.warn( 'Could not get property description', quiddityId, property );
            }
        } else {
            // Attribute grafted
            const attribute = pathElements[2];
            log.debug( 'Property attribute grafted', quiddityId, property, attribute, value );
            this.io.emit( 'quiddity.property.attribute.grafted', quiddityId, property, attribute, value );
        }
    }

    /**
     * Tree Pruned Handler
     *
     * @param quiddityId
     * @param path
     * @private
     */
    _onTreePruned( quiddityId, path ) {
        var prunedPath = path.split( '.' );

        // Remove the first empty value, because paths sometimes start with a dot
        if ( _.isEmpty( prunedPath[0] ) ) {
            prunedPath.shift();
        }

        let broadcastTreeUpdate = true;

        switch ( prunedPath[0] ) {
            case 'shmdata':
                this._onTreePrunedShmdata( quiddityId, path, prunedPath );
                broadcastTreeUpdate = false;
                break;

            case 'property':
                this._onTreePrunedProperty( quiddityId, path, prunedPath );
                broadcastTreeUpdate = false;
                break;

            default:
                //noop
                break;
        }

        var start = path.substr( 0, 1 ) == '.' ? 1 : 0;
        var end = path.substr( path.length - 1, 1 ) == '.' ? path.length - 1 : path.length;
        path = path.substring( start, end );

        if ( broadcastTreeUpdate ) {
            // Unhandled tree update, broadcast
            this.io.emit( 'quiddity.tree.updated', quiddityId, path, null );
        } else {
            // Tree data was handled, do not broadcast
            // but send to the clients that subscribed to the quiddity
            this.switcherController.clients.forEach( client => {
                if ( client.subscriptions[quiddityId] ) {
                    client.socket.emit( 'quiddity.tree.updated', quiddityId, path, null );
                }
            } );
        }
    }

    /**
     * Handle shmdata tree prunes
     *
     * @param {string} quiddityId
     * @param {string} path
     * @param {string[]} pathElements
     * @private
     */
    _onTreePrunedShmdata( quiddityId, path, pathElements ) {
        if ( pathElements.length < 2 ) {
            return;
        }

        var prunedShmdataType = pathElements[1];
        if ( _.contains( this.shmdataTypes, prunedShmdataType ) ) {
            var prunedShmdataPath = ( pathElements.length >= 3 ) ? pathElements[2] : null;
            if ( prunedShmdataPath ) {

                // If the pruned shmdata is a writer, remove the auto preview, if any
                if ( prunedShmdataType == 'writer' ) {
                    this._disconnectAutoPreviews( prunedShmdataPath );
                }

                // A path was provided, remove only that shmdata
                this.io.emit( 'shmdata.remove', quiddityId, {
                    type: prunedShmdataType,
                    path: prunedShmdataPath
                } );
            } else {
                // No path was provided, remove all shmdata of the specified type
                this.io.emit( 'shmdata.remove', quiddityId, {
                    type: prunedShmdataType
                } );
            }
        }
    };

    /**
     * Handle property tree prunes
     *
     * @param {string} quiddityId
     * @param {string} path
     * @param {string[]} pathElements
     * @private
     */
    _onTreePrunedProperty( quiddityId, path, pathElements ) {
        const property = pathElements[1];

        if ( pathElements.length == 2 ) {
            // Whole property pruned
            log.debug( 'Property removed', quiddityId, property );
            this.io.emit( 'quiddity.property.removed', quiddityId, property );
        } else {
            // Attribute pruned
            const attribute = pathElements[2];

            log.debug( 'Property attribute pruned', quiddityId, property, attribute );
            this.io.emit( 'quiddity.property.attribute.pruned', quiddityId, property, attribute );
        }
    }

    /**
     * User Data Grafted Handler
     *
     * @param quiddityId
     * @param path
     * @private
     */
    _onUserDataGrafted( quiddityId, path ) {
        var graftedPath = path.split( '.' );

        // Remove the first empty value, because paths sometimes start with a dot
        if ( _.isEmpty( graftedPath[0] ) ) {
            graftedPath.shift();
        }

        try {
            var value = this.getUserData( quiddityId, path );
        } catch ( e ) {
            return log.error( e );
        }
        if ( "undefined" != typeof value ) {
            var start = path.substr( 0, 1 ) == '.' ? 1 : 0;
            var end = path.substr( path.length - 1, 1 ) == '.' ? path.length - 1 : path.length;
            path = path.substring( start, end );
            this.io.emit( 'quiddity.userData.updated', quiddityId, path, value );
        }
    };

    /**
     * User Data Pruned Handler
     *
     * @param quiddityId
     * @param path
     * @private
     */
    _onUserDataPruned( quiddityId, path ) {
        var prunedPath = path.split( '.' );

        // Remove the first empty value, because paths sometimes start with a dot
        if ( _.isEmpty( prunedPath[0] ) ) {
            prunedPath.shift();
        }

        var start = path.substr( 0, 1 ) == '.' ? 1 : 0;
        var end = path.substr( path.length - 1, 1 ) == '.' ? path.length - 1 : path.length;
        path = path.substring( start, end );
        this.io.emit( 'quiddity.userData.updated', quiddityId, path, null );
    }

    /**
     * Method Added Handler
     *
     * @param quiddityId
     * @param method
     * @private
     */
    _onMethodAdded( quiddityId, method ) {
        log.debug( 'Method added', quiddityId, method );
        var methodDescription = this.getMethodDescription( quiddityId, method );
        if ( methodDescription ) {
            this.io.emit( 'quiddity.method.added', quiddityId, methodDescription );
        } else {
            log.warn( 'Could not get method description', quiddityId, method );
        }
    };

    /**
     * Method Removed Handler
     *
     * @param quiddityId
     * @param method
     * @private
     */
    _onMethodRemoved( quiddityId, method ) {
        log.debug( 'Method removed', quiddityId, method );
        this.io.emit( 'quiddity.method.removed', quiddityId, method );
    };

    //  ███████╗██╗   ██╗██████╗ ███████╗ ██████╗██████╗ ██╗██████╗ ████████╗██╗ ██████╗ ███╗   ██╗███████╗
    //  ██╔════╝██║   ██║██╔══██╗██╔════╝██╔════╝██╔══██╗██║██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝
    //  ███████╗██║   ██║██████╔╝███████╗██║     ██████╔╝██║██████╔╝   ██║   ██║██║   ██║██╔██╗ ██║███████╗
    //  ╚════██║██║   ██║██╔══██╗╚════██║██║     ██╔══██╗██║██╔═══╝    ██║   ██║██║   ██║██║╚██╗██║╚════██║
    //  ███████║╚██████╔╝██████╔╝███████║╚██████╗██║  ██║██║██║        ██║   ██║╚██████╔╝██║ ╚████║███████║
    //  ╚══════╝ ╚═════╝ ╚═════╝ ╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝╚═╝        ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝

    /**
     * @override
     */
    onSwitcherProperty( quiddityId, property, value ) {
        super.onSwitcherProperty( quiddityId, property, value );

        // Special case for manager quiddities
        const managedQuiddity = this.managedQuiddities[quiddityId];
        if ( managedQuiddity && managedQuiddity.managedProperties[property] ) {
            // Verbose, as the timelapse quiddity, for example, is flooding the console with messages otherwise 
            log.verbose( 'Property (managed):', quiddityId + '.' + property + '=' + value );
            managedQuiddity.onProperty( property, value );
        } else {
            log.debug( `Property: ${quiddityId}.${property}=(${typeof value})${value}` );
        }

        // Caches the changed properties and their last values
        // so that the scheduled publish method can iterate over them

        // Create if non-existent
        if ( this.changedProperties[quiddityId] == null ) {
            this.changedProperties[quiddityId] = {};
        }

        // Update to last received value
        this.changedProperties[quiddityId][property] = value;
    };

    /**
     * Publishes the changed properties
     * This is done at a fixed rate to prevent large number of changes of the same property
     * overflowing the client with updates
     */
    publishChangedProperties() {
        _.each( this.changedProperties, ( properties, quiddityId ) => {
            var quiddity = this.switcher.get_quiddity_description( quiddityId );
            if ( quiddity ) {
                if ( _.contains( this.privateQuiddityClasses, quiddity.class ) || _.contains( this.protectedQuiddities, quiddity.id ) ) {
                    // Private, use client subscriptions
                    this.switcherController.clients.forEach( client => {
                        if ( client.subscriptions[quiddity.id] ) {
                            _.each( properties, ( value, property ) => {
                                client.socket.emit( 'propertyChanged', quiddityId, property, value );
                            } );
                        }
                    } );
                } else {
                    // Not private, broadcast to everyone
                    _.each( properties, ( value, property ) => {
                        this.io.emit( 'propertyChanged', quiddityId, property, value );
                    } );
                }
            } else {
                log.error( `Error getting quiddity ${quiddityId} while publishing changed properties.` );
            }
        } );
        this.changedProperties = {};
    };

    /**
     * @override
     */
    onSwitcherSignal( quiddityId, signal, value ) {
        super.onSwitcherSignal( quiddityId, signal, value );
        // We exclude system usage and shmdata from debug because it dispatches every second
        if ( quiddityId != this.config.systemUsage.quiddName && value[0].indexOf( '.shmdata' ) != 0 ) {
            log.debug( 'Signal:', quiddityId + '.' + signal + '=' + value );
        } else {
            log.verbose( 'Signal:', quiddityId + '.' + signal + '=' + value );
        }

        switch ( signal ) {
            case 'on-nicknamed':
                this._onNicknamed( quiddityId, value[0] );
                break;
            case 'on-tree-grafted':
                this._onTreeGrafted( quiddityId, value[0] );
                break;

            case  'on-tree-pruned':
                this._onTreePruned( quiddityId, value[0] );
                break;

            case 'on-user-data-grafted':
                this._onUserDataGrafted( quiddityId, value[0] );
                break;

            case 'on-user-data-pruned':
                this._onUserDataPruned( quiddityId, value[0] );
                break;

            case 'on-quiddity-created':
                this._onCreated( value[0] );
                break;

            case 'on-quiddity-removed':
                this._onRemoved( value[0] );
                break;

            case 'on-method-added' :
                this._onMethodAdded( quiddityId, value[0] );
                break;

            case 'on-method-removed':
                this._onMethodRemoved( quiddityId, value[0] );
                break;

            default:
                // We could send other signals to the client but we'll stay silent for now
                //this.io.emit( 'quiddity.signal', quiddityId, signal, value[0] );
                break;
        }
    };

    //  ███╗   ███╗███████╗████████╗██╗  ██╗ ██████╗ ██████╗ ███████╗
    //  ████╗ ████║██╔════╝╚══██╔══╝██║  ██║██╔═══██╗██╔══██╗██╔════╝
    //  ██╔████╔██║█████╗     ██║   ███████║██║   ██║██║  ██║███████╗
    //  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║   ██║██║  ██║╚════██║
    //  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║╚██████╔╝██████╔╝███████║
    //  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝

    /**
     * Verify if a quiddity exists
     *
     * @param {string} quiddityId - Id of the quiddity
     * @returns {Boolean} If the quiddity exists or not
     */
    exists( quiddityId ) {
        return this.switcher.has_quiddity( quiddityId );
    };

    /**
     * Create Quiddity
     *
     * @param {string} className The class of the quiddity
     * @param {string} [quiddityName] The name (id) of the quiddity
     * @param {Object} [initProperties] Properties to initialize with
     * @param {string} [socketId] Id Socket (socket.io) of the user ask to create the quiddity
     * @throws {NameExistsError}
     */
    create( className, quiddityName, initProperties, socketId ) {
        log.info( 'Creating quiddity ' + className + ' named ' + quiddityName );

        if ( quiddityName && this.switcher.has_quiddity( quiddityName ) ) {
            throw new NameExistsError();
        }

        // Create the quiddity
        var result = this.switcher.create( className );

        var quiddityDescription = null;
        if ( result ) {
            var quiddityId = result;

            if ( initProperties ) {
                Object.keys( initProperties ).forEach( key => {
                    this.setPropertyValue( quiddityId, key, initProperties[key] );
                } );
            }

            if ( quiddityName ) {
                this.setNickname( quiddityId, quiddityName );
            } else {
                this.setNickname( quiddityId, quiddityId );
            }

            if ( socketId ) {
                // Keep a history of who created what
                //TODO Move that into the client
                this.quidditySocketMap[quiddityId] = socketId;
            }
            quiddityDescription = this.getQuiddityDescription( quiddityId );
            // Set read-only flag
            // This is usually done in _fillQuiddity but it was breaking the unit tests so I took no chance and put it here
            //TODO: When eventually refactoring, favor a better solution
            quiddityDescription.readOnly = _.contains( this.readOnlyQuiddityClasses, quiddityDescription.class );
            quiddityDescription.protected = _.contains( this.protected, quiddityDescription.id );
        }

        return quiddityDescription;
    };

    /**
     * Remove quiddity
     * Removes the quiddity
     *
     * @param {string} quiddityId The name (id) of the quiddity
     * @returns {Boolean} Success
     */
    remove( quiddityId ) {
        log.info( 'Removing quiddity ' + quiddityId );

        var self = this;

        var quiddity = this.getQuiddityDescription( quiddityId );
        if ( !quiddity ) {
            log.warn( 'Quiddity ' + quiddityId + ' not found while trying to remove.' );
            return false;
        }

        if ( _.contains( this.readOnlyQuiddityClasses, quiddity.class ) ) {
            log.warn( 'Quiddity ' + quiddityId + ' is read-only.' );
            return false;
        }

        // Disconnect auto previews
        var writers = this.getTreeInfo( quiddityId, '.shmdata.writer' );
        _.forEach( writers, function ( writer, path ) {
            if ( writer.category == "video" ) {
                self._disconnectAutoPreviews( path );
            }
        } );

        var result = this.switcher.remove( quiddityId );
        if ( result ) {
            log.debug( 'Quiddity ' + quiddityId + ' removed.' );
        } else {
            log.warn( 'Failed to remove quiddity ' + quiddityId );
            return false
        }

        return result;
    };

    /**
     * Get quiddity classes
     *
     * @returns {Array} List of classes and their descriptions
     */
    getQuiddityClasses() {
        log.info( 'Getting quiddity classes' );

        var result = this.switcher.get_classes_doc();
        if ( result && result.error ) {
            throw new Error( result.error );
        }

        var classes = [];
        if ( result && result.classes && _.isArray( result.classes ) ) {
            classes = result.classes;
            classes.forEach( quiddityClass => quiddityClass.readOnly = _.contains( this.readOnlyQuiddityClasses, quiddityClass.class ) );
        }

        return classes;
    };

    /**
     * Get all the quiddities, without their properties, methods or tree
     *
     * @returns {Array} List of quiddities
     */
    getAllQuiddities( ) {
        log.debug( `Getting all quiddities` );
        var result = this.switcher.get_quiddities_description();

        if ( result && result.error ) {
            throw new Error( result.error );
        } else if ( result && ( !result.quiddities || !_.isArray( result.quiddities ) ) ) {
            throw new Error( "Could not get quiddities" );
        }

        return result.quiddities;
    };

    /**
     * Get Quiddities with their properties, methods and tree
     *
     * @param {Object} config
     * @returns {Array} List of quiddities
     */
    getQuiddities( config ) {
        log.info( 'Getting quiddities' );

        var result = this.switcher.get_quiddities_description();
        if ( result && result.error ) {
            throw new Error( result.error );
        }

        var quiddities = [];
        if ( result && result.quiddities && _.isArray( result.quiddities ) ) {
            quiddities = result.quiddities;
        }

        // Fill quiddities with their properties, methods and tree before sending
        _.each( quiddities, function ( quiddity ) {
            this._fillQuiddity( quiddity, config );
        }, this );

        return quiddities;
    };

    /**
     * Get quiddity description
     *
     * @param {string} quiddityId Quiddity for which we want to retrieve the description
     * @returns {Object} Object describing the quiddity
     */
    getQuiddityDescription( quiddityId ) {
        log.info( 'Getting quiddity description for quiddity ' + quiddityId );
        var result = this.switcher.get_quiddity_description( quiddityId );
        if ( result && result.error ) {
            throw new Error( result.error );
        }

        var quiddityDescription = {};
        if ( result && !_.isEmpty( result ) && _.isObject( result ) && !_.isArray( result ) ) {
            quiddityDescription = result;
        }

        return quiddityDescription;
    };

    /**
     * Get quiddity tree information
     *
     * @param {string} quiddityId Quiddity for which we want to retrieve the tree information
     * @param {string} [path] Branch/leaf path to query inside the tree
     * @returns {Object} Information contained in the tree or an empty object if nothing was found
     */
    getTreeInfo( quiddityId, path ) {
        log.verbose( 'Getting quiddity tree for: ' + quiddityId + ' ' + path );
        if ( path == null ) {
            path = '.';
        }
        var result = this.switcher.get_info( quiddityId, path );
        if ( result && result.error ) {
            throw new Error( result.error );
        }

        return result;
    };

    /**
     * Get quiddity user data
     *
     * @param {string} quiddityId Quiddity for which we want to retrieve the tree information
     * @param {string} [path] Branch/leaf path to query inside the tree
     * @returns {Object} Information contained in the tree or an empty object if nothing was found
     */
    getUserData( quiddityId, path ) {
        // log.info( 'Getting quiddity user data for: ' + quiddityId + ' ' + path );
        // log.info( 'get_user_data : ', this.switcher.get_user_data( quiddityId, '.' ));
        if ( path == null ) {
            path = '.';
        }
        var result = this.switcher.get_user_data( quiddityId, path );
        if ( result && result.error ) {
            throw new Error( result.error );
        }
        return result == null ? {} : result;
    };

    /**
     * Set quididty user data
     *
     * @param {string} quiddityId Quiddity for which we want to set the tree information
     * @param {string} path Branch/leaf path to add/modify inside the tree
     * @param {*} value Value to set the branch to
     */
    setUserData( quiddityId, path, value ) {
        log.info( 'Setting quiddity user data for: ' + quiddityId + ' ' + path, value );
        if ( path == null ) {
            path = '.';
        }
        var result = this.switcher.set_user_data( quiddityId, path, value );
        if ( result && result.error ) {
            throw new Error( result.error );
        }

        return result;
    };

    /**
     * Set quididty user data
     *
     * @param {string} quiddityId Quiddity for which we want to set the tree information
     * @param {string} path Branch/leaf path to add/modify inside the tree
     * @param {JSON}  value JSON to set the branch to
     */
    setUserDataBranch( quiddityId, path, value ) {
        log.info( 'Setting quiddity user data branch for: ' + quiddityId + ' ' + path, value );
        if ( path == null ) {
            path = '.';
        }
        var result = this.switcher.set_user_data_branch( quiddityId, path, value );
        if ( result && result.error ) {
            throw new Error( result.error );
        }

        return result;
    };

    /**
     * Remove quididty user data
     *
     * @param {string} quiddityId Quiddity for which we want to remove some tree information
     * @param {string} path Branch/leaf path to remove inside the tree
     */
    removeUserData( quiddityId, path ) {
        log.info( 'Remove quiddity user data for: ' + quiddityId + ' ' + path );
        if ( path == null ) {
            path = '.';
        }
        var result = this.switcher.remove_user_data( quiddityId, path );
        if ( result && result.error ) {
            throw new Error( result.error );
        }

        return result;
    };

    /**
     * Get quiddity nickname
     *
     * @param {string} quiddityId Quiddity for which we want to retrieve the nickname
     * @returns {string} nickname
     */
    getNickname( quiddityId ) {
        var result = this.switcher.get_nickname( quiddityId );
        if ( result && result.error ) {
            throw new Error( result.error );
        }
        return result;
    };

    /**
     * Set quididty nickname
     *
     * @param {string} quiddityId Quiddity for which we want to set the nickname
     * @param {string} value of the nickname
     */
    setNickname( quiddityId, value ) {
        log.info( 'Setting quiddity nickname for: ' + quiddityId + ' ', value );

        var result = this.switcher.set_nickname( quiddityId, String( value ));
        if ( result && result.error ) {
            throw new Error( result.error );
        }

        return result;
    };


    /**
     * Get properties
     *
     * @param {string} quiddityId Quiddity for which we want to retrieve the properties
     * @returns {Array} Array of properties for the passed quiddity
     */
    getProperties( quiddityId ) {
        log.info( 'Getting properties for quiddity: ' + quiddityId );

        var properties = this.getTreeInfo( quiddityId, 'property' );
        if ( properties && _.isArray( properties ) ) {
            properties.forEach( this._parseProperty.bind( this, quiddityId ) );
            return properties;
        } else {
            return [];
        }
    };

    /**
     * Get property description
     *
     * @param {string} quiddityId Quiddity for which we want to retrieve the property description
     * @param {string} property Property for which we want the description
     * @returns {Object} Object describing the property
     */
    getPropertyDescription( quiddityId, property ) {
        log.info( 'Getting property description for quiddity ' + quiddityId + ' property ' + property );

        var propertyDescription = this.getTreeInfo( quiddityId, `property.${property}` );
        if ( propertyDescription && !_.isEmpty( propertyDescription ) && _.isObject( propertyDescription ) && !_.isArray( propertyDescription ) ) {
            this._parseProperty( quiddityId, propertyDescription );
            return propertyDescription;
        } else {
            return null;
        }
    };

    /**
     * Get property value
     *
     * @param {string} quiddityId Quiddity for which we want to retrieve the property value
     * @param {string} property Property for which we want the value
     * @returns {*} Property value
     */
    getPropertyValue( quiddityId, property ) {
        log.info( 'Getting property value for quiddity ' + quiddityId + ' property ' + property );

        var result = this.switcher.get_property_value( quiddityId, property );
        // Safeguard against old switcher behavior
        // TODO: Check with Nico if it is still required
        if ( result && result.error ) {
            throw new Error( result.error );
        }

        return result;
    };

    /**
     * Set property value
     *
     * @param {string} quiddityId Quiddity for which we want to retrieve the property description
     * @param {string} property Property for which we want the description
     * @param {*} value Value to set the property to
     * @returns {Boolean} Returns true if the operation was successful
     */
    setPropertyValue( quiddityId, property, value ) {
        log.info( 'Setting property ' + property + ' of ' + quiddityId + ' to ' + value );

        var result = this.switcher.set_property_value( quiddityId, property, String( value ) );
        // Safeguard against old switcher behavior
        // TODO: Check with Nico if it is still required
        if ( result && result.error ) {
            throw new Error( result.error );
        }

        return result;
    };

    /**
     * Get Methods
     *
     * @param {string} quiddityId Quiddity for which we want to retrieve the methods
     * @returns {Array} Methods for the quiddity
     */
    getMethods( quiddityId ) {
        log.info( 'Getting methods for quiddity ' + quiddityId );

        var result = this.switcher.get_methods_description( quiddityId );
        if ( result && result.error ) {
            throw new Error( result.error );
        }

        var methods = [];
        if ( result && result.methods && _.isArray( result.methods ) ) {
            methods = result.methods;

            // Parse methods
            _.each( methods, this._parseMethod, this );
        }

        return methods;
    };

    /**
     * Get method description
     *
     * @param {string} quiddityId Quiddity for which we want to retrieve the method description
     * @param {string} method Method for which we want the description
     * @returns {*}
     */
    getMethodDescription( quiddityId, method ) {
        log.info( 'Getting method description for quiddity ' + quiddityId + ' method ' + method );

        var result = this.switcher.get_method_description( quiddityId, method );
        if ( result && result.error ) {
            throw new Error( result.error );
        }

        var methodDescription = {};
        if ( result && !_.isEmpty( result ) && _.isObject( result ) && !_.isArray( result ) ) {
            methodDescription = result;

            // Parse method
            this._parseMethod( result );
        }

        return methodDescription;
    };

    /**
     * Invoke method on quiddity
     *
     * @param quiddityId
     * @param method
     * @param parameters
     * @returns {*}
     */
    invokeMethod( quiddityId, method, parameters ) {
        log.info( 'Invoking method ' + method + ' of ' + quiddityId + ' with', parameters );

        if ( _.isEmpty( parameters ) ) {
            parameters = [];
        } else if ( !_.isArray( parameters ) ) {
            parameters = [parameters];
        }

        //TODO: Return values are unclear, does an empty string means an error occurred?
        var result = this.switcher.invoke( quiddityId, method, parameters );

        return result;
    };

}

module.exports = QuiddityManager;