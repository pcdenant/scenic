"use strict";

/**
 * Invalid Host Error
 */
class InvalidHostError extends Error {
    /**
     * Create an Invalid Host Error instance
     * @param {string} [message] Custom message for the error
     */
    constructor( message, id ) {
        super( message, id )
        this.name    = 'InvalidHostError';
        this.message = message || 'Invalid Host';
    }
}

module.exports = InvalidHostError;