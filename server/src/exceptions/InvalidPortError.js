"use strict";

/**
 * Invalid Port Error
 */
class InvalidPortError extends Error {
    /**
     * Create an Invalid Port Error instance
     * @param {string} [message] Custom message for the error
     */
    constructor( message, id ) {
        super( message, id );
        this.name    = 'InvalidPortError';
        this.message = message || 'Invalid Port';
    }
}

module.exports = InvalidPortError;