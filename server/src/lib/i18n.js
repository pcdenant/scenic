"use strict";

/**
 * Internationalization
 * @module server/lib/i18n
 */

const _    = require( 'underscore' );
const i18n = require( 'i18next' );
const log  = require( './logger' );

module.exports = {

    /**
     * Initialize i18n
     * 
     * @param callback
     */
    initialize: function ( callback ) {
        log.info( "Initializing translations..." );

        i18n.init( {
            ns:           {
                namespaces: ['switcher', 'server'],
                defaultNs:  'server'
            },
            resGetPath:   'locales/__lng__/__ns__.json',
            resSetPath:   'locales/__lng__/__ns__.json',
            fallbackLng:  false,
            preload:      ['en', 'fr'],
            keyseparator: "::",
            nsseparator:  ':::',
            debug:        false
        }, function () {
            callback();
        } );
    }
};