"use strict";

/**
 * Scenic IO
 * @module server/lib/scenic-io
 */

const log          = require( './logger' );
const ScenicClient = require( '../net/ScenicClient' );

module.exports = {

    /**
     * Initialize Scenic's Socket.IO
     *     
     * @param {Object} config Scenic configuration object
     * @param {Object} io Socket.IO instance
     * @param {SwitcherController} switcherController
     */
    initialize: function ( config, io, switcherController ) {
        log.debug( "Initializing Scenic Io..." );

        /**
         * On Connection simply create a scenic client for now
         * TODO: Manage list of clients somehow
         */
        io.on( 'connection', function ( socket ) {
            var client = new ScenicClient( switcherController,  config, socket );
        } );
    }
};