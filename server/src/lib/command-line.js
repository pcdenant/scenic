"use strict";

/**
 * Command Line Parser
 * @module server/lib/command-line
 */

const _        = require( 'underscore' );
const optimist = require( 'optimist' );
const rpad     = require( 'underscore.string/rpad' );

/**
 * Parses the command line arguments
 * @param {Object} config Scenic configuration
 */
exports.parse = function ( config ) {

    var argv = optimist.argv;

    /**
     * Help
     */
    if ( argv.h || argv.help ) {
        var message = "Scenic " + config.version + "\n\n";
        message += "Command helper for Scenic\n";
        message += "----------------------------------------------------------\n";
        message += rpad( '-v, --version', 25 ) + "Print Scenic version\n";
        message += rpad( '-c, --config', 25 ) + 'Override with the specified json config file\n';
        message += rpad( '-d, --defaults', 25 ) + 'Load the json config file for default quiddity configuration\n';
        message += rpad( '-f, --file', 25 ) + 'Load a Scenic file (ex : -f my_save.scenic)\n';
        message += rpad( '-n, -nogui', 25 ) + "Launch Scenic without app interface\n";
        message += rpad( '-l, --log', 25 ) + "Set the log level (default: info) [switcher|debug]\n";
        message += rpad( '-g, --guiport', 25 ) + "GUI port for Scenic (default is " + config.scenic.ports.min + ")\n";
        message += rpad( '-s, --soapport', 25 ) + "Enables SOAP and optionally set a port (default is " + config.soap.ports.min + ")\n";
        message += rpad( '-i, --identification', 25 ) + "Identification name (default is " + config.nameComputer + ")\n";
        message += rpad( '-r, --rtpsession', 25 ) + "RTP session name (default is " + config.rtp.quiddName + ")\n";
        message += rpad( '--sip', 25 ) + "SIP configuration (ex : --sip user=Username port=" + config.sip.port + " server=" + config.sip.server + ")\n";
        console.log( message );
        process.exit();
    }

    /**
     * SIP
     */
    if ( argv.sip ) {
        config.sip.commandLine = true;
        argv._.push( argv.sip );
        _.each( argv._, function ( param ) {
            var paramSplit = param.split( "=" );
            if ( _.contains( ['user', 'port', 'server'], paramSplit[0] ) ) {
                config.sip[paramSplit[0]] = paramSplit[1];
            }
        } );
    }

    /**
     * Version
     */
    if ( argv.v || argv.version ) {
        var version = (argv.v ? argv.v : argv.version);
        console.log( "Scenic version " + config.version );
        process.exit();
    }

    /**
     * Config
     */
    if ( argv.c || argv.config ) {
        config.configFile = (argv.c ? argv.c : argv.config);
    }

    /**
     * Defaults
     */
    if ( argv.d || argv.defaults ) {
        config.defaultsFile = (argv.d ? argv.d : argv.defaults);
    }

    /**
     * Log level
     */
    if ( argv.l || argv.log ) {
        config.logLevel = (argv.l ? argv.l : argv.log);
    }

    /**
     * Load file
     */
    if ( argv.f || argv.file ) {
        config.loadFile = (argv.f ? argv.f : argv.file);
    }

    /**
     * GUI port
     */
    if ( argv.g || argv.guiport ) {
        var port           = (argv.g ? argv.g : argv.guiport);
        config.scenic.port = port;
    }

    /**
     * SOAP port
     */
    if ( argv.s || argv.soapport ) {
        var port = parseInt( argv.s ? argv.s : argv.soapport );
        config.soap.enabled = true;
        if ( !isNaN( port ) ) {
            config.soap.port = port;
        }
    }

    /**
     * Identification
     */
    if ( argv.i || argv.identification ) {
        var identification  = (argv.i ? argv.i : argv.identification);
        config.nameComputer = identification;
    }

    /**
     * RTP session
     */
    if ( argv.r || argv.rtpsession ) {
        var rtpsession       = (argv.r ? argv.r : argv.rtpsession);
        config.rtp.quiddName = rtpsession;
    }

    /**
     * Headless
     */
    var message = null;
    if ( argv.n || argv.nogui ) {
        config.standalone = true;
    }
};
