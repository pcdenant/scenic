"use strict";

/**
 * Logger
 * @module server/lib/logger
 */

const winston = require( 'winston' );
const colors  = require( 'colors/safe' );
const config  = require( '../settings/config' );

const customLevels = {
    levels: {
        verbose:  5,
        switcher: 4,
        debug:    3,
        info:     2,
        warn:     1,
        error:    0
    },
    colors: {
        verbose:  'gray',
        switcher: 'magenta',
        debug:    'gray',
        info:     'blue',
        warn:     'yellow',
        error:    'red'
    }
};

const fs = require( 'fs' );

/* check if folder logs exist */
if ( !fs.existsSync( config.logPath ) ) {
    // create folder
    fs.mkdir( config.logPath, e => {
        if ( e ) {
            return console.log( e );
        }
    } )
}

const log = new (winston.Logger)( {
    levels:      customLevels.levels,
    exitOnError: false,
    transports:  [
        new (winston.transports.Console)( {
            colorize:                        true,
            prettyPrint:                     true,
            level:                           config.logLevel,
            timestamp:                       true,
            handleExceptions:                true,
            humanReadableUnhandledException: true
        } ),

        new (winston.transports.File)( {
            name:          'activity',
            filename:      config.logPath + 'scenic.log',
            colorize:      false,
            level:         config.logLevel,
            tailable:      true,
            zippedArchive: true,
            maxsize:       1024 * 1024 * 10, // 10MB
            maxFiles:      3
        } ),

        new (winston.transports.File)( {
            name:                            'error',
            filename:                        config.logPath + 'scenic-error.log',
            colorize:                        false,
            level:                           'error',
            tailable:                        true,
            zippedArchive:                   true,
            maxsize:                         1024 * 1024 * 10, // 10MB
            maxFiles:                        3,
            handleExceptions:                true,
            humanReadableUnhandledException: true
        } )
    ],
    filters:     [
        function ( level, msg, meta ) {
            if ( level == 'error' ) {
                msg = traceCaller( 5 ) + ": " + msg;
            } else if ( level == 'switcher' ) {
                var prefix = msg.split( ':' )[0];
                switch ( prefix.split( '-' ).pop() ) {
                    case 'error':
                    case 'critical':
                        msg = colors.red( msg );
                        break;
                    case 'warn':
                    case 'warning':
                        // g_error is always fatal (SIGTRAP), so we changed it to g_warn and added "ERROR: "
                        // in the message to catch it here
                        if ( msg.indexOf('ERROR: ') != -1 ) {
                            msg = colors.red( msg );
                        } else {
                            msg = colors.yellow( msg );
                        }
                        break;
                    case 'info':
                        msg = colors.blue( msg );
                        break;
                    case 'message':
                    case 'notice':
                    case 'log':
                        //noop
                        break;
                    case 'debug':
                        msg = colors.gray( msg );
                        break;
                }
            }
            return { msg: msg, meta: meta, level: level };
        }
    ]
} );

winston.addColors( customLevels.colors );

/**
 * examines the call stack and returns a string indicating
 * the file and line number of the n'th previous ancestor call.
 * this works in chrome, and should work in nodejs as well.
 *
 * @param {int} [n=1] the number of calls to trace up the
 *   stack from the current call.  `n=0` gives you your current file/line.
 *  `n=1` gives the file/line that called you.
 */

function traceCaller( n ) {
    if ( isNaN( n ) || n < 0 ) {
        n = 1;
    }
    n += 1;
    var s = (new Error()).stack,
        a = s.indexOf( '\n', 5 );
    while ( n-- ) {
        a = s.indexOf( '\n', a + 1 );
        if ( a < 0 ) {
            a = s.lastIndexOf( '\n', s.length );
            break;
        }
    }
    var b = s.indexOf( '\n', a + 1 );
    if ( b < 0 ) {
        b = s.length;
    }
    a = Math.max( s.lastIndexOf( ' ', b ), s.lastIndexOf( '/', b ) );
    b = s.lastIndexOf( ':', b );
    s = s.substring( a + 1, b );
    return s;
}

module.exports = log;
