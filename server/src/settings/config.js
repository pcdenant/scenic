"use strict";

/**
 * @module server/settings/config
 */
const pkg          = require( '../../../package.json' );
const NetworkUtils = require( '../utils/NetworkUtils' );
var os             = require( 'os' );
const homePath     = process.env.HOME + "/.scenic";

/**
 * Scenic Configuration
 */
const config = {
    version:    pkg.version,
    debug:      false,
    lang:       'en',
    host:       NetworkUtils.getLocalIp(),
    interfaces: NetworkUtils.getInterfaces(),

    // Scenic Configuration
    scenic: {
        ports: {
            min: 8000,
            max: 8099
        },
        port:  null //Automatic
    },

    // SOAP Configuration
    soap: {
        enabled:             false,
        ports:               {
            min: 27182,
            max: 27199
        },
        port:                null, //Automatic
        quiddName:           'soap',
        controlClientPrefix: 'soapControlClient-'
    },

    // SIP Configuration
    sip: {
        port:       5060,
        user:       '',
        server:     "sip.scenic.sat.qc.ca",
        quiddName:  'SIP',
        commandLine: false,
        turnServer:  null,
        stunServer:  null,
        sameLogin:   false
    },

    rtp: {
        quiddName: 'defaultRtp'
    },

    userTree: {
        quiddName: 'userTree',
        userData: {
            path: 'scenes',
            value:  {
                        defaultScene: {
                            id: 'defaultScene',
                            properties: {
                                name : "Default scene",
                                select: true,
                                active: false
                            },
                            connections: {}
                        }
            }
        }
    },
    httpSdpDec: {
        refreshTimeout: 2000
    },

    // System Usage
    systemUsage: {
        period:    1.0, // In seconds
        quiddName: 'systemUsage'
    },

    // Timelapse
    timelapse: {
        path:      '/tmp/scenic',
        thumbnail: {
            quiddName: 'timelapseThumb',
            type:      'thumbnail',
            width:     64,
            height:    0,//Math.round((16/9)*64),
            quality:   70,
            framerate: 6
        },
        preview:   {
            quiddName: 'timelapsePreview',
            type:      'preview',
            width:     480,
            height:    0,
            quality:   75,
            framerate: 12
        }
    },

    locale: {
        supported: ['en', 'fr']
    },

    // Overridable by a config.json in ~/.scenic or passed to command line

    privateQuiddities:          [],
    disabledPages:              [],
    disableRTP:                 false,
    disableChat:                false,
    disableDestinationsMenu:    false,
    disableSourcesMenu:         false,
    fullscreenMode:             false,
    defaultPanelPage:           'matrix',
    customMenus:                [],
    logLevel:                   "warn",
    standalone:                 false,
    passSet:                    null,
    loadFile:                   false,
    propertiesPage:             false,
    nameComputer:               os.hostname(),
    homePath:                   homePath,
    defaultsFile:               homePath + "/switcher.json",
    configFile:                 homePath + "/config.json",
    savePath:                   homePath + "/save_files/",
    contactsPath:               homePath + "/contacts.json",
    logPath:                    homePath + '/logs/'
};

module.exports = config;
