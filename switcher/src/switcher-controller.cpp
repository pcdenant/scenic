/*
 * This file is part of switcher-nodejs.
 *
 * switcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * switcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with switcher.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./switcher-controller.hpp"
#include <iostream>
#include <node.h>
#include <switcher/information-tree-json.hpp>
#include <switcher/file-utils.hpp>
#include <utility>
#include <uv.h>

using namespace std;
using namespace v8;
using namespace node;
using namespace switcher;

Persistent<Function> SwitcherController::constructor;

SwitcherController::SwitcherController(const std::string &name,
                                       Local<Function> logger_callback)
    : quiddityManager(switcher::Switcher::make_switcher<SwitcherControllerLog>(name, this)) {
  Isolate *isolate = Isolate::GetCurrent();

  // keep the log callback
  user_log_cb.Reset(isolate, logger_callback);

  // mutex
  uv_mutex_init(&switcher_log_mutex);
  uv_mutex_init(&switcher_prop_mutex);
  uv_mutex_init(&switcher_sig_mutex);

  // async
  switcher_log_async.data = this;
  uv_async_init(uv_default_loop(), &switcher_log_async, NotifyLog);

  switcher_prop_async.data = this;
  uv_async_init(uv_default_loop(), &switcher_prop_async, NotifyProp);

  switcher_sig_async.data = this;
  uv_async_init(uv_default_loop(), &switcher_sig_async, NotifySignal);

  quiddityManager->register_creation_cb(
      [this](const std::string& quid_name) {
        signal_cb(quid_name, "on-quiddity-created", std::vector<std::string>({quid_name}), this);
      });
  quiddityManager->register_removal_cb(
      [this](const std::string& quid_name) {
        signal_cb(quid_name, "on-quiddity-removed", std::vector<std::string>({quid_name}), this);
      });

// load plugins
#ifdef USR_PLUGINS
  std::string usr_plugin_dir(USR_PLUGINS);
  quiddityManager->scan_directory_for_plugins(usr_plugin_dir.c_str());
#endif

#ifdef USR_LOCAL_PLUGINS
  std::string usr_local_plugin_dir(USR_LOCAL_PLUGINS);
  quiddityManager->scan_directory_for_plugins(usr_local_plugin_dir);
#endif

  // do not play with previous config when saving
  quiddityManager->reset_state(false);
};

SwitcherController::~SwitcherController(){};

void SwitcherController::Init(Local<Object> exports) {
  Isolate *isolate = exports->GetIsolate();

  // Prepare constructor template
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
  tpl->SetClassName(String::NewFromUtf8(isolate, "Switcher"));
  tpl->InstanceTemplate()->SetInternalFieldCount(34);

  // lifecycle - 1
  NODE_SET_PROTOTYPE_METHOD(tpl, "close", SwitcherClose);

  // nickname - 2
  NODE_SET_PROTOTYPE_METHOD(tpl, "set_nickname", SetNickname);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_nickname", GetNickname);

  // history - 5
  NODE_SET_PROTOTYPE_METHOD(tpl, "reset_command_history", ResetCommandHistory);
  NODE_SET_PROTOTYPE_METHOD(tpl, "save_history", SaveHistory);
  NODE_SET_PROTOTYPE_METHOD(tpl, "load_history_from_current_state",
                            LoadHistoryFromCurrentState);
  NODE_SET_PROTOTYPE_METHOD(tpl, "load_history_from_scratch",
                            LoadHistoryFromScratch);
  NODE_SET_PROTOTYPE_METHOD(tpl, "reset_history", ResetHistory);

  // life manager - 13
  NODE_SET_PROTOTYPE_METHOD(tpl, "load_defaults", LoadDefaults);
  NODE_SET_PROTOTYPE_METHOD(tpl, "create", Create);
  NODE_SET_PROTOTYPE_METHOD(tpl, "remove", Remove);
  NODE_SET_PROTOTYPE_METHOD(tpl, "has_quiddity", HasQuiddity);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_classes_doc", GetClassesDoc);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_class_doc", GetClassDoc);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_quiddity_description",
                            GetQuiddityDescription);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_quiddities_description",
                            GetQuidditiesDescription);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_info", GetInfo);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_user_data", GetUserData);
  NODE_SET_PROTOTYPE_METHOD(tpl, "set_user_data", SetUserData);
  NODE_SET_PROTOTYPE_METHOD(tpl, "set_user_data_branch", SetUserDataBranch);
  NODE_SET_PROTOTYPE_METHOD(tpl, "remove_user_data", RemoveUserData);

  // properties - 2
  NODE_SET_PROTOTYPE_METHOD(tpl, "set_property_value", SetProperty);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_property_value", GetProperty);

  // methods - 5
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_methods_description",
                            GetMethodsDescription);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_method_description",
                            GetMethodDescription);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_methods_description_by_class",
                            GetMethodsDescriptionByClass);
  NODE_SET_PROTOTYPE_METHOD(tpl, "get_method_description_by_class",
                            GetMethodDescriptionByClass);
  NODE_SET_PROTOTYPE_METHOD(tpl, "invoke", Invoke);

  // property subscription - 4
  NODE_SET_PROTOTYPE_METHOD(tpl, "register_prop_callback",
                            RegisterPropCallback);
  NODE_SET_PROTOTYPE_METHOD(tpl, "subscribe_to_property", SubscribeToProperty);
  NODE_SET_PROTOTYPE_METHOD(tpl, "unsubscribe_from_property",
                            UnsubscribeFromProperty);

  // signal subscription - 2
  NODE_SET_PROTOTYPE_METHOD(tpl, "register_signal_callback",
                            RegisterSignalCallback);
  NODE_SET_PROTOTYPE_METHOD(tpl, "subscribe_to_signal", SubscribeToSignal);

  // constructor
  constructor.Reset(isolate, tpl->GetFunction());
  exports->Set(String::NewFromUtf8(isolate, "Switcher"), tpl->GetFunction());
}

void SwitcherController::release() {
  is_closing_ = true;

  quiddityManager.reset();

  if (!uv_is_closing((uv_handle_t *)&switcher_log_async)) {
    uv_close((uv_handle_t *)&switcher_log_async, nullptr);
  }
  if (!uv_is_closing((uv_handle_t *)&switcher_prop_async)) {
    uv_close((uv_handle_t *)&switcher_prop_async, nullptr);
  }
  if (!uv_is_closing((uv_handle_t *)&switcher_sig_async)) {
    uv_close((uv_handle_t *)&switcher_sig_async, nullptr);
  }

  uv_mutex_destroy(&switcher_sig_mutex);
  uv_mutex_destroy(&switcher_prop_mutex);
  uv_mutex_destroy(&switcher_log_mutex);

  user_log_cb.Reset();
  user_prop_cb.Reset();
  user_signal_cb.Reset();
}

void SwitcherController::New(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);

  if (args.IsConstructCall()) {
    // Invoked as constructor: `new SwitcherController(...)`
    if (args.Length() < 2 || !args[0]->IsString() || !args[1]->IsFunction()) {
      isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
          isolate,
          "Wrong arguments. Switcher requires a name and logger callback.")));
      return;
    }

    // Construction
    String::Utf8Value name((args.Length() >= 1 && !args[0]->IsString())
                               ? String::NewFromUtf8(isolate, "nodeserver")
                               : args[0]->ToString());
    SwitcherController *obj = new SwitcherController(
        std::string(*name), Local<Function>::Cast(args[1]));
    obj->Wrap(args.This());
    return args.GetReturnValue().Set(args.This());

  } else {
    // Invoked as plain function `SwitcherController(...)`, turn into construct
    // call.
    const int argc = 1;
    Local<Value> argv[argc] = {args[0]};
    Local<Function> cons = Local<Function>::New(isolate, constructor);
    return args.GetReturnValue().Set(cons->NewInstance(argc, argv));
  }
}

Handle<Value> SwitcherController::parseJson(Handle<Value> jsonString) {
  Isolate *isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);
  Handle<Object> global = isolate->GetCurrentContext()->Global();
  Handle<Object> JSON =
      global->Get(String::NewFromUtf8(isolate, "JSON"))->ToObject();
  Handle<Function> JSON_parse =
      Handle<Function>::Cast(JSON->Get(String::NewFromUtf8(isolate, "parse")));
  return JSON_parse->Call(JSON, 1, &jsonString);
}

void SwitcherController::NotifyLog(uv_async_s *async) {
  Isolate *isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);
  SwitcherController *obj = static_cast<SwitcherController *>(async->data);
  bool cleanup = false;

  if (!obj->user_log_cb.IsEmpty()) {
    Local<Function> cb = Local<Function>::New(isolate, obj->user_log_cb);
    if (cb->IsCallable()) {
      TryCatch try_catch;
      uv_mutex_lock(&obj->switcher_log_mutex);
      // Performing a copy in order to avoid deadlock from log handlers
      // having to call the addon themselves
      auto log_list = obj->switcher_log_list;
      obj->switcher_log_list.clear();
      uv_mutex_unlock(&obj->switcher_log_mutex);

      for (auto &it : log_list) {
        Local<Value> argv[] = {String::NewFromUtf8(isolate, it.c_str())};
        cb->Call(isolate->GetCurrentContext()->Global(), 1, argv);
      }

      if (try_catch.HasCaught()) {
        FatalException(isolate, try_catch);
      }
    } else {
      cleanup = true;
    }
  } else {
    cleanup = true;
  }

  if (cleanup) {
    uv_mutex_lock(&obj->switcher_log_mutex);
    obj->switcher_log_list.clear();
    uv_mutex_unlock(&obj->switcher_log_mutex);
  }
}

void SwitcherController::NotifyProp(uv_async_s *async) {
  Isolate *isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);
  SwitcherController *obj = static_cast<SwitcherController *>(async->data);

  bool cleanup = false;

  if (!obj->user_prop_cb.IsEmpty()) {
    Local<Function> cb = Local<Function>::New(isolate, obj->user_prop_cb);
    if (cb->IsCallable()) {
      TryCatch try_catch;
      uv_mutex_lock(&obj->switcher_prop_mutex);
      // Performing a copy in order to avoid deadlock from prop handlers
      // having to call the addon themselves
      auto prop_list = obj->switcher_prop_list;
      obj->switcher_prop_list.clear();
      uv_mutex_unlock(&obj->switcher_prop_mutex);

      for (auto &it : prop_list) {
        Local<Value> argv[3];
        argv[0] = {String::NewFromUtf8(isolate, it.quid_.c_str())};
        argv[1] = {String::NewFromUtf8(isolate, it.prop_.c_str())};
        // argv[2] = {String::NewFromUtf8(isolate, it.val_.c_str())};

        // Try parsing as JSON, if it fails just return the string as-is
        Local<String> str_val = String::NewFromUtf8(isolate, it.val_.c_str());
        TryCatch val_try_catch;
        Handle<Value> json = parseJson(str_val);
        if (!val_try_catch.HasCaught()) {
          argv[2] = {json};
        } else {
          argv[2] = {str_val};
        }

        cb->Call(isolate->GetCurrentContext()->Global(), 3, argv);
      }

      if (try_catch.HasCaught()) {
        FatalException(isolate, try_catch);
      }
    } else {
      cleanup = true;
    }
  } else {
    cleanup = true;
  }

  if (cleanup) {
    uv_mutex_lock(&obj->switcher_prop_mutex);
    obj->switcher_prop_list.clear();
    uv_mutex_unlock(&obj->switcher_prop_mutex);
  }
}

void SwitcherController::signal_cb(const std::string &quiddity_name,
                                   const std::string &signal_name,
                                   const std::vector<std::string> &params,
                                   void *user_data) {
  SwitcherController *obj = static_cast<SwitcherController *>(user_data);
  uv_mutex_lock(&obj->switcher_sig_mutex);
  obj->switcher_sig_list.push_back(
       SigUpdate(quiddity_name, signal_name, params));
  uv_mutex_unlock(&obj->switcher_sig_mutex);
  uv_async_send(&obj->switcher_sig_async);
}

void SwitcherController::NotifySignal(uv_async_s *async) {
  Isolate *isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);
  SwitcherController *obj = static_cast<SwitcherController *>(async->data);

  bool cleanup = false;

  if (!obj->user_signal_cb.IsEmpty()) {
    Local<Function> cb = Local<Function>::New(isolate, obj->user_signal_cb);

    if (!obj->user_signal_cb.IsEmpty() && cb->IsCallable()) {
      TryCatch try_catch;

      uv_mutex_lock(&obj->switcher_sig_mutex);
      // Performing a copy in order to avoid deadlock from signal handlers
      // having to call the addon themselves
      // For example, on-quiddity-removed has to also remove associated
      // quiddities
      auto sig_list = obj->switcher_sig_list;
      obj->switcher_sig_list.clear();
      uv_mutex_unlock(&obj->switcher_sig_mutex);

      for (auto &it : sig_list) {
        Local<Value> argv[3];
        Local<Array> array = Array::New(isolate, it.val_.size());
        for (auto &item : it.val_) {
          array->Set(0, String::NewFromUtf8(isolate, item.c_str()));
        }
        argv[0] = {String::NewFromUtf8(isolate, it.quid_.c_str())};
        argv[1] = {String::NewFromUtf8(isolate, it.sig_.c_str())};
        argv[2] = {array};

        cb->Call(isolate->GetCurrentContext()->Global(), 3, argv);
      }

      if (try_catch.HasCaught()) {
        FatalException(isolate, try_catch);
      }
    } else {
      cleanup = true;
    }
  } else {
    cleanup = true;
  }

  if (cleanup) {
    uv_mutex_lock(&obj->switcher_sig_mutex);
    obj->switcher_sig_list.clear();
    uv_mutex_unlock(&obj->switcher_sig_mutex);
  }
}

//  ██╗     ██╗███████╗███████╗ ██████╗██╗   ██╗ ██████╗██╗     ███████╗
//  ██║     ██║██╔════╝██╔════╝██╔════╝╚██╗ ██╔╝██╔════╝██║     ██╔════╝
//  ██║     ██║█████╗  █████╗  ██║      ╚████╔╝ ██║     ██║     █████╗
//  ██║     ██║██╔══╝  ██╔══╝  ██║       ╚██╔╝  ██║     ██║     ██╔══╝
//  ███████╗██║██║     ███████╗╚██████╗   ██║   ╚██████╗███████╗███████╗
//  ╚══════╝╚═╝╚═╝     ╚══════╝ ╚═════╝   ╚═╝    ╚═════╝╚══════╝╚══════╝

void SwitcherController::SwitcherClose(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());
  obj->release();
  return args.GetReturnValue().Set(Boolean::New(isolate, true));
}

//  ██╗  ██╗██╗███████╗████████╗ ██████╗ ██████╗ ██╗   ██╗
//  ██║  ██║██║██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗╚██╗ ██╔╝
//  ███████║██║███████╗   ██║   ██║   ██║██████╔╝ ╚████╔╝
//  ██╔══██║██║╚════██║   ██║   ██║   ██║██╔══██╗  ╚██╔╝
//  ██║  ██║██║███████║   ██║   ╚██████╔╝██║  ██║   ██║
//  ╚═╝  ╚═╝╚═╝╚══════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝   ╚═╝

void SwitcherController::ResetCommandHistory(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  obj->quiddityManager->reset_state(false);
}

void SwitcherController::SaveHistory(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  String::Utf8Value file_path(args[0]->ToString());
  if(FileUtils::save(JSONSerializer::serialize(obj->quiddityManager->get_state().get()), std::string(*file_path))) {
    return args.GetReturnValue().Set(Boolean::New(isolate, true));
  }
  return args.GetReturnValue().Set(Boolean::New(isolate, false));
}

void SwitcherController::ResetHistory(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  obj->signal_muted_ = true;
  obj->quiddityManager->reset_state(true);
  obj->signal_muted_ = false;
}

void SwitcherController::LoadHistoryFromCurrentState(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  String::Utf8Value file_path(args[0]->ToString());
  obj->signal_muted_ = true;
  if (!obj->quiddityManager->load_state(
          JSONSerializer::deserialize(
              FileUtils::get_content(std::string(*file_path))))) {
    obj->signal_muted_ = false;
    return args.GetReturnValue().Set(Boolean::New(isolate, false));
  }
  obj->signal_muted_ = false;

  return args.GetReturnValue().Set(Boolean::New(isolate, true));
}

void SwitcherController::LoadHistoryFromScratch(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  String::Utf8Value file_path(args[0]->ToString());

  obj->signal_muted_ = true;
  if (!obj->quiddityManager->load_state(
          JSONSerializer::deserialize(
              FileUtils::get_content(std::string(*file_path))))) {
  obj->signal_muted_ = false;
    return args.GetReturnValue().Set(Boolean::New(isolate, false));
  }
  obj->signal_muted_ = false;

  obj->quiddityManager->reset_state(true);

  return args.GetReturnValue().Set(Boolean::New(isolate, true));
}

//   ██████╗ ██╗   ██╗██╗██████╗ ██████╗ ██╗████████╗██╗███████╗███████╗
//  ██╔═══██╗██║   ██║██║██╔══██╗██╔══██╗██║╚══██╔══╝██║██╔════╝██╔════╝
//  ██║   ██║██║   ██║██║██║  ██║██║  ██║██║   ██║   ██║█████╗  ███████╗
//  ██║▄▄ ██║██║   ██║██║██║  ██║██║  ██║██║   ██║   ██║██╔══╝  ╚════██║
//  ╚██████╔╝╚██████╔╝██║██████╔╝██████╔╝██║   ██║   ██║███████╗███████║
//   ╚══▀▀═╝  ╚═════╝ ╚═╝╚═════╝ ╚═════╝ ╚═╝   ╚═╝   ╚═╝╚══════╝╚══════╝

void SwitcherController::LoadDefaults(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher load_defaults: Wrong first arguments type")));
    return;
  }

  String::Utf8Value file_path(args[0]->ToString());

  if (obj->quiddityManager->load_configuration_file(std::string(*file_path))) {
    return args.GetReturnValue().Set(Boolean::New(isolate, true));
  }
  return args.GetReturnValue().Set(Boolean::New(isolate, false));
}

void SwitcherController::Remove(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher remove: Wrong first arguments type")));
    return;
  }

  String::Utf8Value first_arg(args[0]->ToString());

  if (obj->quiddityManager->remove(std::string(*first_arg))) {
    return args.GetReturnValue().Set(Boolean::New(isolate, true));
  }
  return args.GetReturnValue().Set(Boolean::New(isolate, false));
}

void SwitcherController::HasQuiddity(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher has_quiddity: Wrong first arguments type")));
    return;
  }

  String::Utf8Value quiddity_name(args[0]->ToString());

  if (obj->quiddityManager->has_quiddity(std::string(*quiddity_name))) {
    return args.GetReturnValue().Set(Boolean::New(isolate, true));
  }
  return args.GetReturnValue().Set(Boolean::New(isolate, false));
}

void SwitcherController::Create(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1 && args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "switcher create: Wrong first arg type")));
    return;
  }

  String::Utf8Value first_arg(args[0]->ToString());

  if (args.Length() == 2) {
    if (!args[1]->IsString()) {
      isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
          isolate, "switcher create: Wrong second arg type")));
      return;
    }

    String::Utf8Value second_arg(args[1]->ToString());

    return args.GetReturnValue().Set(String::NewFromUtf8(
        isolate, obj->quiddityManager
                     ->create(std::string(*first_arg), std::string(*second_arg))
                     .c_str()));
  } else {
    return args.GetReturnValue().Set(String::NewFromUtf8(
        isolate,
        obj->quiddityManager->create(std::string(*first_arg)).c_str()));
  }
}

void SwitcherController::GetInfo(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher get_info: Wrong first arg type")));
    return;
  }

  String::Utf8Value first_arg(args[0]->ToString());

  if (!args[1]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher get_info: Wrong second arg type")));
    return;
  }

  String::Utf8Value second_arg(args[1]->ToString());
  Handle<String> res = String::NewFromUtf8(
      isolate, obj->quiddityManager
                   ->use_tree<MPtr(&InfoTree::serialize_json)>(
                       std::string(*first_arg), std::string(*second_arg))
                   .c_str());

  return args.GetReturnValue().Set(parseJson(res));
}

void SwitcherController::GetUserData(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher get_user_data: Wrong first arg type")));
    return;
  }

  String::Utf8Value first_arg(args[0]->ToString());

  if (!args[1]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher get_user_data: Wrong second arg type")));
    return;
  }

  String::Utf8Value second_arg(args[1]->ToString());
  Handle<String> res = String::NewFromUtf8(
      isolate, obj->quiddityManager
                   ->user_data<MPtr(&InfoTree::serialize_json)>(
                       std::string(*first_arg), std::string(*second_arg))
                   .c_str());

  return args.GetReturnValue().Set(parseJson(res));
}

void SwitcherController::SetUserData(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 3) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }

  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher set_user_data: Wrong quiddity id arg type")));
    return;
  }

  String::Utf8Value quiddity_id(args[0]->ToString());

  if (!args[1]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher set_user_data: Wrong path arg type")));
    return;
  }

  String::Utf8Value path(args[1]->ToString());

  bool res = false;
  InfoTree::ptr tree;

  if (args[2]->IsString()) {
    tree = InfoTree::make(std::string(*String::Utf8Value(args[2]->ToString())));
  } else if (args[2]->IsBoolean()) {
    tree = InfoTree::make(args[2]->ToBoolean()->Value());
  } else if (args[2]->IsInt32()) {
    tree = InfoTree::make(args[2]->ToInt32()->Value());
  } else if (args[2]->IsUint32()) {
    tree = InfoTree::make(args[2]->ToUint32()->Value());
  } else if (args[2]->IsNumber()) {
    tree = InfoTree::make(args[2]->ToNumber()->Value());
  }

  if (tree) {
    res = obj->quiddityManager->user_data<MPtr(&InfoTree::graft)>(
        std::string(*quiddity_id), std::string(*path), tree);
  }

  return args.GetReturnValue().Set(Boolean::New(isolate, res));
}

void SwitcherController::SetUserDataBranch(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 3) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }

  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher set_user_data: Wrong quiddity id arg type")));
    return;
  }

  String::Utf8Value quiddity_id(args[0]->ToString());

  if (!args[1]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher set_user_data: Wrong path arg type")));
    return;
  }

  String::Utf8Value path(args[1]->ToString());

  bool res = false;
  InfoTree::ptr tree;

  if (args[2]->IsString()) {
    tree = JSONSerializer::deserialize(std::string(*String::Utf8Value(args[2]->ToString())));
  }

  if (tree) {
    res = obj->quiddityManager->user_data<MPtr(&InfoTree::graft)>(
        std::string(*quiddity_id), std::string(*path), tree);
  } else {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher set_user_data: cannot deserialize json")));
    return;
  }

  return args.GetReturnValue().Set(Boolean::New(isolate, res));
}

void SwitcherController::RemoveUserData(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }

  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher remove_user_data: Wrong quiddity id arg type")));
    return;
  }

  String::Utf8Value quiddity_id(args[0]->ToString());

  if (!args[1]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher remove_user_data: Wrong path arg type")));
    return;
  }

  String::Utf8Value path(args[1]->ToString());

  bool res = false;
  InfoTree::ptr tree;

  res = static_cast<bool>(obj->quiddityManager->user_data<MPtr(&InfoTree::prune)>(
      std::string(*quiddity_id), std::string(*path)));


  return args.GetReturnValue().Set(Boolean::New(isolate, res));
}

void SwitcherController::GetClassesDoc(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  Handle<String> res = String::NewFromUtf8(
      isolate, obj->quiddityManager->get_classes_doc().c_str());
  return args.GetReturnValue().Set(parseJson(res));
}

void SwitcherController::GetClassDoc(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  String::Utf8Value class_name(args[0]->ToString());

  Handle<String> res = String::NewFromUtf8(
      isolate,
      obj->quiddityManager->get_class_doc(std::string(*class_name)).c_str());
  return args.GetReturnValue().Set(parseJson(res));
}

void SwitcherController::GetQuiddityDescription(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  String::Utf8Value quiddity_name(args[0]->ToString());

  Handle<String> res = String::NewFromUtf8(
      isolate, obj->quiddityManager
                   ->get_quiddity_description(std::string(*quiddity_name))
                   .c_str());
  return args.GetReturnValue().Set(parseJson(res));
}

void SwitcherController::GetQuidditiesDescription(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());
  Handle<String> res = String::NewFromUtf8(
      isolate, obj->quiddityManager->get_quiddities_description().c_str());
  return args.GetReturnValue().Set(parseJson(res));
}

//  ██████╗ ██████╗  ██████╗ ██████╗ ███████╗██████╗
//  ████████╗██╗███████╗███████╗
//  ██╔══██╗██╔══██╗██╔═══██╗██╔══██╗██╔════╝██╔══██╗╚══██╔══╝██║██╔════╝██╔════╝
//  ██████╔╝██████╔╝██║   ██║██████╔╝█████╗  ██████╔╝   ██║   ██║█████╗
//  ███████╗
//  ██╔═══╝ ██╔══██╗██║   ██║██╔═══╝ ██╔══╝  ██╔══██╗   ██║   ██║██╔══╝
//  ╚════██║
//  ██║     ██║  ██║╚██████╔╝██║     ███████╗██║  ██║   ██║
//  ██║███████╗███████║
//  ╚═╝     ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚══════╝╚═╝  ╚═╝   ╚═╝
//  ╚═╝╚══════╝╚══════╝

void SwitcherController::SetProperty(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 3) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString() || !args[1]->IsString() || !args[2]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  String::Utf8Value element_name(args[0]->ToString());
  String::Utf8Value property_name(args[1]->ToString());
  String::Utf8Value property_val(args[2]->ToString());

  Handle<Boolean> res = Boolean::New(
      isolate, obj->quiddityManager->use_prop<MPtr(&PContainer::set_str_str)>(
                   std::string(*element_name), std::string(*property_name),
                   std::string(*property_val)));

  return args.GetReturnValue().Set(res);
}

void SwitcherController::GetProperty(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString() || !args[1]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  String::Utf8Value element_name(args[0]->ToString());
  String::Utf8Value property_name(args[1]->ToString());

  Handle<String> res = String::NewFromUtf8(
      isolate, obj->quiddityManager
                   ->use_prop<MPtr(&PContainer::get_str_str)>(
                       std::string(*element_name), std::string(*property_name))
                   .c_str());

  // Try parsing as JSON, if it fails just return the string as-is
  TryCatch try_catch;
  Handle<Value> json = parseJson(res);
  if (!try_catch.HasCaught()) {
    return args.GetReturnValue().Set(json);
  } else {
    return args.GetReturnValue().Set(res);
  }
}

//  ███╗   ███╗███████╗████████╗██╗  ██╗ ██████╗ ██████╗ ███████╗
//  ████╗ ████║██╔════╝╚══██╔══╝██║  ██║██╔═══██╗██╔══██╗██╔════╝
//  ██╔████╔██║█████╗     ██║   ███████║██║   ██║██║  ██║███████╗
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║   ██║██║  ██║╚════██║
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║╚██████╔╝██████╔╝███████║
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝

void SwitcherController::Invoke(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() < 3) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString() || !args[1]->IsString() || !args[2]->IsArray()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  String::Utf8Value element_name(args[0]->ToString());
  String::Utf8Value method_name(args[1]->ToString());
  Local<Object> obj_arguments = args[2]->ToObject();
  Local<Array> arguments = obj_arguments->GetPropertyNames();

  std::vector<std::string> vector_arg;
  for (unsigned int i = 0; i < arguments->Length(); i++) {
    String::Utf8Value val(obj_arguments->Get(i)->ToString());
    vector_arg.push_back(std::string(*val));
  }

  std::string *return_value = nullptr;
  obj->quiddityManager->invoke(std::string(*element_name),
                               std::string(*method_name), &return_value,
                               vector_arg);
  if (nullptr != return_value) {
    Handle<String> res = String::NewFromUtf8(isolate, (*return_value).c_str());
    delete return_value; // FIXME this should not be necessary
    return args.GetReturnValue().Set(parseJson(res));
  }

  return;
}

void SwitcherController::GetMethodsDescription(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  String::Utf8Value element_name(args[0]->ToString());

  Handle<String> res = String::NewFromUtf8(
      isolate,
      obj->quiddityManager->get_methods_description(std::string(*element_name))
          .c_str());
  return args.GetReturnValue().Set(parseJson(res));
}

void SwitcherController::GetMethodDescription(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString() || !args[1]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  String::Utf8Value element_name(args[0]->ToString());
  String::Utf8Value method_name(args[1]->ToString());

  Handle<String> res = String::NewFromUtf8(
      isolate, obj->quiddityManager
                   ->get_method_description(std::string(*element_name),
                                            std::string(*method_name))
                   .c_str());

  return args.GetReturnValue().Set(parseJson(res));
}

void SwitcherController::GetMethodsDescriptionByClass(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  String::Utf8Value class_name(args[0]->ToString());

  Handle<String> res = String::NewFromUtf8(
      isolate, obj->quiddityManager
                   ->get_methods_description_by_class(std::string(*class_name))
                   .c_str());
  return args.GetReturnValue().Set(parseJson(res));
}

void SwitcherController::GetMethodDescriptionByClass(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString() || !args[1]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  String::Utf8Value class_name(args[0]->ToString());
  String::Utf8Value method_name(args[1]->ToString());

  Handle<String> res = String::NewFromUtf8(
      isolate, obj->quiddityManager
                   ->get_method_description_by_class(std::string(*class_name),
                                                     std::string(*method_name))
                   .c_str());
  return args.GetReturnValue().Set(parseJson(res));
}

//  ██████╗ ██████╗  ██████╗ ██████╗ ███████╗██████╗ ████████╗██╗   ██╗
//  ██╔══██╗██╔══██╗██╔═══██╗██╔══██╗██╔════╝██╔══██╗╚══██╔══╝╚██╗ ██╔╝
//  ██████╔╝██████╔╝██║   ██║██████╔╝█████╗  ██████╔╝   ██║    ╚████╔╝
//  ██╔═══╝ ██╔══██╗██║   ██║██╔═══╝ ██╔══╝  ██╔══██╗   ██║     ╚██╔╝
//  ██║     ██║  ██║╚██████╔╝██║     ███████╗██║  ██║   ██║      ██║
//  ╚═╝     ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚══════╝╚═╝  ╚═╝   ╚═╝      ╚═╝

void SwitcherController::RegisterPropCallback(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  obj->user_prop_cb.Reset(isolate, Local<Function>::Cast(args[0]));

  return;
}

void SwitcherController::SubscribeToProperty(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString() || !args[1]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  String::Utf8Value element_name(args[0]->ToString());
  String::Utf8Value property_name(args[1]->ToString());

  auto man = obj->quiddityManager.get();
  auto qname = std::string(*element_name);
  auto pname = std::string(*property_name);
  auto prop_id = man->use_prop<MPtr(&PContainer::get_id)>(qname, pname);
  auto reg_id = man->use_prop<MPtr(&PContainer::subscribe)>(
      qname, prop_id, [obj, man, qname, pname, prop_id]() {
        if (!obj->is_closing_) {
          uv_mutex_lock(&obj->switcher_prop_mutex);
          obj->switcher_prop_list.push_back(PropUpdate(
              qname, pname,
              man->use_prop<MPtr(&PContainer::get_str)>(qname, prop_id)));
          uv_mutex_unlock(&obj->switcher_prop_mutex);
          uv_async_send(&obj->switcher_prop_async);
        }
      });
  if (0 == reg_id) {
    Handle<Boolean> res = Boolean::New(isolate, false);
    return args.GetReturnValue().Set(res);
  }
  obj->prop_regs_[std::make_pair(qname, pname)] = reg_id;
  Handle<Boolean> res = Boolean::New(isolate, true);
  return args.GetReturnValue().Set(res);
}

void SwitcherController::UnsubscribeFromProperty(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString() || !args[1]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  String::Utf8Value element_name(args[0]->ToString());
  String::Utf8Value property_name(args[1]->ToString());

  auto qname = std::string(*element_name);
  auto pname = std::string(*property_name);
  auto it = obj->prop_regs_.find(std::make_pair(qname, pname));
  if (obj->prop_regs_.end() == it) {
    Handle<Boolean> res = Boolean::New(isolate, false);
    return args.GetReturnValue().Set(res);
  }
  auto man = obj->quiddityManager.get();
  Handle<Boolean> res = Boolean::New(
      isolate,
      man->use_prop<MPtr(&PContainer::unsubscribe)>(
          qname, man->use_prop<MPtr(&PContainer::get_id)>(qname, pname),
          it->second));
  return args.GetReturnValue().Set(res);
}

//  ███████╗██╗ ██████╗ ███╗   ██╗ █████╗ ██╗
//  ██╔════╝██║██╔════╝ ████╗  ██║██╔══██╗██║
//  ███████╗██║██║  ███╗██╔██╗ ██║███████║██║
//  ╚════██║██║██║   ██║██║╚██╗██║██╔══██║██║
//  ███████║██║╚██████╔╝██║ ╚████║██║  ██║███████╗
//  ╚══════╝╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚═╝  ╚═╝╚══════╝

void SwitcherController::RegisterSignalCallback(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  obj->user_signal_cb.Reset(isolate, Local<Function>::Cast(args[0]));

  return;
}

void SwitcherController::SubscribeToSignal(
    const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString() || !args[1]->IsString()) {
    isolate->ThrowException(
        Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
    return;
  }

  String::Utf8Value element_name(args[0]->ToString());
  String::Utf8Value signal_name(args[1]->ToString());

  Handle<Boolean> res = Boolean::New(
      isolate,
      0 !=
          obj->quiddityManager
              ->use_sig<MPtr(&switcher::SContainer::subscribe_by_name)>(
                  std::string(*element_name), std::string(*signal_name),
                  [
                        obj, quid = std::string(*element_name),
                        sig = std::string(*signal_name)
                  ](const switcher::InfoTree::ptr &tree) {
                    if (!obj->signal_muted_)
                      obj->signal_cb(quid, sig, 
                                     std::vector<std::string>(
                                           {tree->get_value().as<std::string>()}),
                                     obj);
                  }));
  return args.GetReturnValue().Set(res);
}

void SwitcherController::SetNickname(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 2) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher set_nickname: Wrong first arg type")));
    return;
  }

  String::Utf8Value first_arg(args[0]->ToString());

  if (!args[1]->IsString()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(
        isolate, "switcher set_nickname: Wrong second arg type")));
    return;
  }

  String::Utf8Value second_arg(args[1]->ToString());

  if (obj->quiddityManager->set_nickname(std::string(*first_arg), std::string(*second_arg))) {
    return args.GetReturnValue().Set(Boolean::New(isolate, true));
  }
  return args.GetReturnValue().Set(Boolean::New(isolate, false));
}

void SwitcherController::GetNickname(const FunctionCallbackInfo<Value> &args) {
  Isolate *isolate = args.GetIsolate();
  HandleScope scope(isolate);
  SwitcherController *obj = ObjectWrap::Unwrap<SwitcherController>(args.This());

  if (args.Length() != 1) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
    return;
  }
  if (!args[0]->IsString()) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "switcher create: Wrong first arg type")));
    return;
  }

  String::Utf8Value first_arg(args[0]->ToString());

  return args.GetReturnValue().Set(String::NewFromUtf8(
      isolate,
      obj->quiddityManager->get_nickname(std::string(*first_arg)).c_str()));
}

SwitcherControllerLog::SwitcherControllerLog(SwitcherController *ctrl)
    : ctrl_(ctrl) {}

void SwitcherControllerLog::on_error(std::string &&str) {
  static std::string prefix = "switcher-error: ";
  if (!ctrl_->is_closing_) {
    uv_mutex_lock(&ctrl_->switcher_log_mutex);
    ctrl_->switcher_log_list.push_back(prefix + str);
    uv_mutex_unlock(&ctrl_->switcher_log_mutex);
    uv_async_send(&ctrl_->switcher_log_async);
  }
}

void SwitcherControllerLog::on_critical(std::string &&str) {
  static std::string prefix = "switcher-critical: ";
  if (!ctrl_->is_closing_) {
    uv_mutex_lock(&ctrl_->switcher_log_mutex);
    ctrl_->switcher_log_list.push_back(prefix + str);
    uv_mutex_unlock(&ctrl_->switcher_log_mutex);
    uv_async_send(&ctrl_->switcher_log_async);
  }
}
void SwitcherControllerLog::on_warning(std::string &&str) {
  static std::string prefix = "switcher-warning: ";
  if (!ctrl_->is_closing_) {
    uv_mutex_lock(&ctrl_->switcher_log_mutex);
    ctrl_->switcher_log_list.push_back(prefix + str);
    uv_mutex_unlock(&ctrl_->switcher_log_mutex);
    uv_async_send(&ctrl_->switcher_log_async);
  }
}
void SwitcherControllerLog::on_message(std::string &&str) {
  static std::string prefix = "switcher-message: ";
  if (!ctrl_->is_closing_) {
    uv_mutex_lock(&ctrl_->switcher_log_mutex);
    ctrl_->switcher_log_list.push_back(prefix + str);
    uv_mutex_unlock(&ctrl_->switcher_log_mutex);
    uv_async_send(&ctrl_->switcher_log_async);
  }
}
void SwitcherControllerLog::on_info(std::string &&str) {
  static std::string prefix = "switcher-info: ";
  if (!ctrl_->is_closing_) {
    uv_mutex_lock(&ctrl_->switcher_log_mutex);
    ctrl_->switcher_log_list.push_back(prefix + str);
    uv_mutex_unlock(&ctrl_->switcher_log_mutex);
    uv_async_send(&ctrl_->switcher_log_async);
  }
}
void SwitcherControllerLog::on_debug(std::string &&str) {
  static std::string prefix = "switcher-debug: ";
  if (!ctrl_->is_closing_) {
    uv_mutex_lock(&ctrl_->switcher_log_mutex);
    ctrl_->switcher_log_list.push_back(prefix + str);
    uv_mutex_unlock(&ctrl_->switcher_log_mutex);
    uv_async_send(&ctrl_->switcher_log_async);
  }
}
